import React, { Component } from "react"
import { Link } from "gatsby"
import axios from "axios"
import Layout from "../components/layout"
import SEO from "../components/seo"
import TitleBar from "../components/listings/title-bar"
import Pagination from "../components/shared/pagination"
import { GET_profile_summary, } from "../api/endpoints"
import Loader from "../components/shared/loader"
import { _suburbs } from "../mocks";
import PrivateLayout from "../components/private-layout"

class UserProfile extends Component {
  state = {
    loading: true,
    error: false,
    userProfile: {
      listings: [],
      bookings: [],
      reviews: [],
      user: {}
    }
  }


  componentDidMount() {
    this.GetProfileSummary();
  }


  extractParametersFromSearch() {
    return window.location.href.split('=')[1];
  }


  GetProfileSummary = () => {
    const userId = this.extractParametersFromSearch();
    console.log("Getting user profile for", userId)
    axios.get(`${GET_profile_summary}${userId}`)
      .then(response => {
        console.log("User Profile", response)
        this.setState({
          loading: false,
          userProfile: response.data,
        })
      }).catch(error => {
        console.log(`[ERROR] Get user profile error: `, { error })
        this.setState({ loading: false, error })
      })
  }

  render() {
    return (

      <PrivateLayout bodyCss="" displayHeader={true} >
        <Loader showLoader={this.state.loading} />
        <SEO title={`User Profile | ${this.state.userProfile.user.firstName}`} />
        {/* <TitleBar title={`User Profile | ${this.state.userProfile.user.firstName}`} description="All rentals in your area" /> */}

        <div id="titlebar" class="gradient">
          <div class="container">
            <div class="row">
              <div class="col-md-12">

                <div class="user-profile-titlebar">
                  <div class="user-profile-avatar"><img src={this.state.userProfile.user.profilePicUrl} alt="" /></div>
                  <div class="user-profile-name">
                    <h2>{`${this.state.userProfile.user.firstName} ${this.state.userProfile.user.lastName}`}</h2>
                    <div class="star-rating" data-rating={this.state.userProfile.user.rating}>
                      <div class="rating-counter"><a href="#listing-reviews">({`${this.state.userProfile.reviews === undefined ? "0" : this.state.userProfile.reviews.length} reviews`}})</a></div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row sticky-wrapper">


            <div class="col-lg-4 col-md-4 margin-top-0">

              {/* <!-- Verified Badge --> */}
              {this.state.userProfile.user.isVerified === true
                ? <div class="verified-badge with-tip" data-tip-content="Account has been verified and belongs to the person or organization represented.">
                  <i class="sl sl-icon-user-following"></i> Verified Account
                  </div>
                : ""}

              {/* <!-- Contact --> */}
              <div class="boxed-widget margin-top-30 margin-bottom-50">
                <h3>Contact</h3>
                <ul class="listing-details-sidebar">
                  <li><i class="sl sl-icon-phone"></i>{this.state.userProfile.user.phoneNumber}</li>
                  <li><i class="fa fa-envelope-o"></i> <a href="#">{this.state.userProfile.user.email}</a></li>
                </ul>

                <ul class="listing-details-sidebar social-profiles">
                  <li><a href="#" class="facebook-profile"><i class="fa fa-facebook-square"></i> {this.state.userProfile.user.facebook}</a></li>
                  <li><a href="#" class="twitter-profile"><i class="fa fa-twitter"></i> {this.state.userProfile.user.twitter}</a></li>
                  {/* <!-- <li><a href="#" class="gplus-profile"><i class="fa fa-google-plus"></i> Google Plus</a></li> --> */}
                </ul>

                {/* <!-- Reply to review popup --> */}
                <div id="small-dialog" class="zoom-anim-dialog mfp-hide">
                  <div class="small-dialog-header">
                    <h3>Send Message</h3>
                  </div>
                  <div class="message-reply margin-top-0">
                    <textarea cols="40" rows="3" placeholder={`Your message to ${this.state.userProfile.user.firstName}`}></textarea>
                    <button class="button">Send Message</button>
                  </div>
                </div>

                <a href="#small-dialog" class="send-message-to-owner button popup-with-zoom-anim"><i class="sl sl-icon-envelope-open"></i> Send Message</a>
              </div>
              {/* <!-- Contact / End--> */}

            </div>
            {/* <!-- Sidebar / End --> */}


            {/* <!-- Content */}
            {/* ================================================== --> */}
            <div class="col-lg-8 col-md-8 padding-left-30">

              <h3 class="margin-top-0 margin-bottom-40">{`${this.state.userProfile.user.firstName}`}'s Listings</h3>

              {/* <!-- Listings Container --> */}
              <div class="row">


                {this.state.userProfile.listings.length > 0 ? this.state.userProfile.listings.map(listing => (
                  <>
                    {console.log('value', listing)}

                    {/* <!-- Listing Item --> */}
                    <div class="col-lg-12 col-md-12">
                      <div class="listing-item-container list-layout">
                        {/* <a href="listings-single-page.html" class="listing-item"> */}
                        <Link
                          to={`/detail?id=${listing.id}`}
                          state={{ id: listing.id }}
                          className={"listing-item"}
                        >
                          {/* <!-- Image --> */}
                          <div class="listing-item-image">
                            <img src={listing.mainImage} alt="" />
                            <span class="tag">{listing.subCategory.name}</span>
                          </div>

                          {/* <!-- Content --> */}
                          <div class="listing-item-content">
                            <div class="listing-badge now-open">Now Open</div>

                            <div class="listing-item-inner">
                              <h3>{listing.title}</h3>
                              {/* const _suburb = (_suburbs.filter((val) => { return val.id === suburb })[0]);${listing.suburb} , ${listing.location}`}</span> */}
                              {/* <span>{`${_suburbs.filter((val) => { return val.id === listing.suburb })[0].suburb}, ${listing.location}`}</span> */}
                              <div class="star-rating" data-rating={listing.rating}>
                                <div class="rating-counter">{`(${listing.reviewCount} reviews)`}</div>
                              </div>
                            </div>

                            <span class="like-icon"></span>
                          </div>
                          {/* </a> */}
                        </Link>
                      </div>
                    </div>
                    {/* <!-- Listing Item / End --> */}
                  </>
                )) : "No results"}

                <div class="col-md-12 browse-all-user-listings">
                  <a href="#">Browse All Listings <i class="fa fa-angle-right"></i> </a>
                </div>

              </div>
              {/* <!-- Listings Container / End --> */}


              {/* <!-- Reviews --> */}
              <div id="listing-reviews" class="listing-section">
                <h3 class="margin-top-60 margin-bottom-20">Reviews</h3>

                <div class="clearfix"></div>

                {/* <!-- Reviews --> */}

                {/* <section class="comments listing-reviews">

                  <ul>
                    <li>
                      <div class="avatar"><img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=70" alt="" /></div>
                      <div class="comment-content"><div class="arrow-comment"></div>
                        <div class="comment-by">Kathy Brown <div class="comment-by-listing">on <a href="#">Burger House</a></div> <span class="date">June 2019</span>
                          <div class="star-rating" data-rating="5"></div>
                        </div>
                        <p>Morbi velit eros, sagittis in facilisis non, rhoncus et erat. Nam posuere tristique sem, eu ultricies tortor imperdiet vitae. Curabitur lacinia neque non metus</p>

                        <div class="review-images mfp-gallery-container">
                          <a href="images/review-image-01.jpg" class="mfp-gallery"><img src="images/review-image-01.jpg" alt="" /></a>
                        </div>
                      </div>
                    </li>

                    <li>
                      <div class="avatar"><img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=70" alt="" /> </div>
                      <div class="comment-content"><div class="arrow-comment"></div>
                        <div class="comment-by">John Doe <div class="comment-by-listing">on <a href="#">Tom's Restaurant</a></div> <span class="date">May 2019</span>
                          <div class="star-rating" data-rating="4"></div>
                        </div>
                        <p>Commodo est luctus eget. Proin in nunc laoreet justo volutpat blandit enim. Sem felis, ullamcorper vel aliquam non, varius eget justo. Duis quis nunc tellus sollicitudin mauris.</p>
                      </div>
                    </li>

                    <li>
                      <div class="avatar"><img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=70" alt="" /></div>
                      <div class="comment-content"><div class="arrow-comment"></div>
                        <div class="comment-by">Kathy Brown <div class="comment-by-listing">on <a href="#">Burger House</a></div> <span class="date">June 2019</span>
                          <div class="star-rating" data-rating="5"></div>
                        </div>
                        <p>Morbi velit eros, sagittis in facilisis non, rhoncus et erat. Nam posuere tristique sem, eu ultricies tortor imperdiet vitae. Curabitur lacinia neque non metus</p>

                        <div class="review-images mfp-gallery-container">
                          <a href="images/review-image-02.jpg" class="mfp-gallery"><img src="images/review-image-02.jpg" alt="" /></a>
                          <a href="images/review-image-03.jpg" class="mfp-gallery"><img src="images/review-image-03.jpg" alt="" /></a>
                        </div>
                      </div>
                    </li>

                    <li>
                      <div class="avatar"><img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=70" alt="" /> </div>
                      <div class="comment-content"><div class="arrow-comment"></div>
                        <div class="comment-by">John Doe <div class="comment-by-listing">on <a href="#">Hotel Govendor</a></div> <span class="date">May 2019</span>
                          <div class="star-rating" data-rating="5"></div>
                        </div>
                        <p>Commodo est luctus eget. Proin in nunc laoreet justo volutpat blandit enim. Sem felis, ullamcorper vel aliquam non, varius eget justo. Duis quis nunc tellus sollicitudin mauris.</p>
                      </div>

                    </li>
                  </ul>
                </section> */}

                {/* <!-- Pagination --> */}
                <div class="clearfix"></div>
                <div class="row">
                  <div class="col-md-12">
                    {/* <!-- Pagination --> */}
                    <div class="pagination-container margin-top-30">
                      <nav class="pagination">
                        <ul>
                          <li><a href="#" class="current-page">1</a></li>
                          <li><a href="#">2</a></li>
                          <li><a href="#"><i class="sl sl-icon-arrow-right"></i></a></li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
                {/* <!-- Pagination / End --> */}
              </div>


            </div>

          </div>
        </div>
      </PrivateLayout>
    )
  }

}
export default UserProfile
