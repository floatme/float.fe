import React, { Component } from "react"
import { Link } from "gatsby"
import axios from "axios"
import Layout from "../components/layout"
import SEO from "../components/seo"
import TitleBar from "../components/listings/title-bar"
import MainSearch from "../components/home/main-search"
import ListItem from "../components/listings/list-item"
import Pagination from "../components/shared/pagination"
import { mockListings } from "../mocks"
import { GET_all_listings, GET_search_listings, GET_filter_listings, GET_listings_by_category } from "../api/endpoints"
import Loader from "../components/shared/loader"
import { ListingFilterTypeEnum, ListingFilterOrderTypeEnum } from "../components/admin/dashboard/enums/ListingFilterTypeEnum"
import { SearchFilterTypes, OtherFilterTypes, LocationFilterTypes } from "../components/listings/enums/SearchFilterTypes";

import floating from "../images/svgs/undraw_floating_61u6.svg"

const useMock = false;

class ListingPage extends Component {
  state = {
    loading: true,
    error: false,
    listings: [],
    allListings: [],
    page: 1,
    search: { term: "", location: "", subCategory: "" },
    filterType: SearchFilterTypes.NONE,
    activeFilters: []

  }


  componentDidMount() {

    const { location } = this.props;
    if (location !== undefined) {
      const { state } = location;
      if (state !== null && state !== undefined) {

        console.log("INIT SEARCH ", this.props)



        //initearch from home page search
        if (state.initSearch.term !== undefined && state.initSearch.location !== undefined && state.initSearch.category !== undefined) {
          console.log("INIT SEARCH", this.props)

          this.handleSearch(state.initSearch.term, state.initSearch.location, state.initSearch.category)
        }
        //category search from home page categories
        if (state.initSearch.term === undefined && state.initSearch.location === undefined && state.initSearch.category !== undefined) {
          console.log("INIT SEARCH 2", this.props)

          this.getByCategory(state.initSearch.category)
        }
      }
    }
    // const { initSearch } = state



    if (useMock) {
      this.setState({
        loading: false,
        listings: [...this.state.listings, ...mockListings.listings],
        search: { term: "", location: "", subCategory: "" }
      })
    } else {
      if (this.props.location)
        this.GetListings()
    }

  }

  getByCategory = (category) => {
    const config = {
      headers: { 'Access-Control-Allow-Origin': '*' }
    };
    axios.get(`${GET_listings_by_category}${category}`, config)
      .then(response => {
        const filteredListings = JSON.parse(response.data.output)
        console.log("LENGTH", filteredListings.length)
        this.setState({
          loading: false,
          listings: filteredListings,
        })
        console.log("STATE AFTER", this.state)
      }).catch(error => {
        console.log(`[ERROR] Adding listing, error: `, { error })
        this.setState({ loading: false, error })
      })
  }

  updateSearchState = (term, location, subCategory) => {
    console.log("UPDATING SEARCH")
    this.setState({ loading: true, search: { term: term, location: location, subCategory: subCategory } })
    console.log("UPDATED SEARCH", this.state)
  }

  handleSearch = (term, location, subCategory) => {

    this.updateSearchState(term, location, subCategory);
    axios.get(GET_search_listings(term, location, subCategory))
      .then(response => {
        const filteredListings = response.data
        this.setState({
          loading: false,
          listings: filteredListings,
        })
      }).catch(error => {
        console.log(`[ERROR] Adding listing, error: `, { error })
        this.setState({ loading: false, error })
      })
  }

  filterListings() {
    this.setState({ loading: true })

    axios.get(GET_filter_listings(ListingFilterTypeEnum.MINPRICE, "2", ListingFilterOrderTypeEnum.NEWEST, false,
      this.state.search.term === "" ? ListingFilterOrderTypeEnum.NONE : this.state.search.term,
      this.state.search.location === "" ? ListingFilterOrderTypeEnum.NONE : this.state.search.location,
      this.state.search.subCategory === "" ? ListingFilterOrderTypeEnum.NONE : this.state.search.subCategory))
      .then(response => {
        const orderedListings = response.data
        console.log("FILTERED RESPONSE", response)
        this.setState({
          loading: false,
          listings: orderedListings,
          allListings: orderedListings
        })
      }).catch(error => {
        console.log(`[ERROR] Adding listing, error: `, { error })
        this.setState({ loading: false, error })
      })
  }

  GetListings = page => {

    this.setState({ loading: true, page: page })
    axios
      // .get(`${GET_all_listings}${page}`)
      .get(`${GET_all_listings}`)
      .then(response => {

        const newListings = response.data.data

        this.setState({
          loading: false,
          listings: [...this.state.listings, ...newListings],
          allListings: newListings
        })
      })
      .catch(error => {
        this.setState({ loading: false, error })

      })
  }

  toggleFilterControl(filterType) {
    if (this.state.filterType === filterType) {
      this.setState({ filterType: SearchFilterTypes.NONE })
    } else {
      this.setState({ filterType: filterType })
    }

  }

  filterCurrentListings(filterType, filterValue) {
    console.log("filtering current", this.state)
    switch (filterType) {
      case SearchFilterTypes.OTHER:
        switch (filterValue) {
          case OtherFilterTypes.DELIVERY: this.setState({ listings: this.state.allListings.filter(x => x.delivery === true) });
          case OtherFilterTypes.COLLECTION: this.setState({ listings: this.state.allListings.filter(x => x.collection === true) });
          // case OtherFilterTypes.VERIFIED_USER: this.setState({ listings: this.state.allListings.filter(x => x.collection === true) });
          // case OtherFilterTypes.HIGH_RATING: this.setState({ listings: this.state.allListings.filter(x => x.collection === true) });
        }
      case SearchFilterTypes.LOCATION:
        switch (filterValue) {
          // case LocationFilterTypes.FIVEKM: this.setState({ listings: this.state.allListings.filter(x => x.delivery === true) });
        }
    }
  }

  activateFilter(filterValue) {
    if (this.state.activeFilters.indexOf(filterValue) === -1) {
      this.setState({ activeFilters: [...this.state.activeFilters, filterValue] })
    } else {
      this.setState({ activeFilters: this.state.activeFilters.filter(x => x !== filterValue) });
    }
    console.log("toggle filter", filterValue)

    console.log("toggle filter state", this.state.activeFilters)

  }

  applyFilter(filterType) {
    this.state.activeFilters.map((filterValue) => this.filterCurrentListings(filterType, filterValue))
  }

  resetFilters(filterType) {
    this.setState({ activeFilters: [] });
  }

  render() {

    return (

      <Layout bodyCss="" displayHeader={true} >
        <Loader showLoader={this.state.loading} />
        <SEO title="All listings" />
        <TitleBar title="All Listings" description="All rentals in your area" />

        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <MainSearch
                css="main-search-input gray-style margin-top-0 margin-bottom-10"
                searchFn={this.handleSearch}
                changeLocation={this.changeLocation}
                redirectOnSearch={false}
                updateParent={this.updateSearchState}

              />
            </div>

            <div class="col-md-12">
              <div class="row">
                {this.state.listings.length > 0 ?


                  <div class="row margin-bottom-25 margin-top-30">
                    <div class="col-md-6">
                      <div class="layout-switcher">
                        <a href="listings-grid-full-width.html" class="grid">
                          <i class="fa fa-th"></i>
                        </a>
                        <a href="#" class="list active">
                          <i class="fa fa-align-justify"></i>
                        </a>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="fullwidth-filters">
                        <div class={this.state.filterType === SearchFilterTypes.OTHER ? `panel-dropdown active wide float-right` : `panel-dropdown wide float-right`}>
                          <a onClick={() => { this.toggleFilterControl(SearchFilterTypes.OTHER) }}>More Filters</a>
                          <div class="panel-dropdown-content checkboxes">
                            <div class="row">
                              <div class="col-md-6">
                                <input onClick={() => { this.activateFilter(OtherFilterTypes.DELIVERY) }} id="check-a" type="checkbox" name="check" />
                                <label for="check-a">Delivery</label>

                                <input onClick={() => { this.activateFilter(OtherFilterTypes.COLLECTION) }} id="check-b" type="checkbox" name="check" />
                                <label for="check-b">Collection</label>
                              </div>

                              <div class="col-md-6">
                                <input onClick={() => { this.activateFilter(OtherFilterTypes.HIGH_RATING) }} id="check-e" type="checkbox" name="check" />
                                <label for="check-e"> Highly Rated </label>

                                <input onClick={() => { this.activateFilter(OtherFilterTypes.VERIFIED_USER) }} id="check-f" type="checkbox" name="check" />
                                <label for="check-f">Verified owner</label>

                              </div>
                            </div>

                            <div class="panel-buttons">
                              <button onClick={() => { this.resetFilter(SearchFilterTypes.OTHER) }} class="panel-cancel">Cancel</button>
                              <button onClick={() => { this.applyFilter(SearchFilterTypes.OTHER) }} class="panel-apply">Apply</button>
                            </div>
                          </div>
                        </div>
                        <div class={this.state.filterType === SearchFilterTypes.LOCATION ? `panel-dropdown active wide float-right` : `panel-dropdown float-right`}>
                          <a onClick={() => { this.toggleFilterControl(SearchFilterTypes.LOCATION) }}>Distance Radius</a>
                          <div class="panel-dropdown-content">
                            <input
                              class="distance-radius"
                              type="range"
                              min="5"
                              max="50"
                              step="10"
                              value="0"
                              data-title="Radius around selected destination"
                            />
                            <div class="panel-buttons">
                              <button class="panel-cancel">Disable</button>
                              <button class="panel-apply">Apply</button>
                            </div>
                          </div>
                        </div>

                        <div class="sort-by">
                          <div class="sort-by-select">
                            <select
                              data-placeholder="Default order"
                              class="chosen-select-no-single"
                            >
                              <option>Default Order</option>
                              <option>Highest Rated</option>
                              <option>Most Reviewed</option>
                              <option>Newest Listings</option>
                              <option>Oldest Listings</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  : ""}
                {this.state.listings.length > 0 ? this.state.listings.map(value => (
                  <>
                    < ListItem
                      key={value.id}
                      id={value.id}
                      title={value.title}
                      location={value.location}
                      image={value.mainImage}
                      category={value.subCategory}
                      rating={value.rating}
                      reviewCount={value.reviewCount}
                      badgeText={value.PromoText}
                      likeButtonFn={() => { }}
                    />
                  </>
                )) :
                  <>
                    <h1> Keep floating...</h1>
                    <h3> Unfortunately we couldn't find any listings that match your search</h3>
                    <br />
                    <img src={floating} />
                    <br />
                  </>
                }
              </div>

              {/* <Pagination page={this.state.page} count="8" /> */}
            </div>
          </div>
        </div>
      </Layout >
    )
  }

}
export default ListingPage
