import React, { Component } from "react"
import { Link } from "gatsby"
import axios from "axios"
import { ModalRoutingContext } from 'gatsby-plugin-modal-routing'

import Layout from "../components/layout"
import SEO from "../components/seo"
import Slider from "../components/detail/slider"
import TitleBar from "../components/detail/title-bar"
import Nav from "../components/detail/nav"
import Overview from "../components/detail/overview"
import Pricing from "../components/detail/pricing"
import ListingMap from "../components/detail/listing-map"
import RatingOverview from "../components/detail/rating-overview"
import Review from "../components/detail/review"
import AddReview from "../components/detail/add-review"
import SideBar from "../components/detail/sidebar/sidebar"
import Pagination from "../components/shared/pagination"
import { detailMock, defaultDetailMock, _suburbs } from "../mocks"
import { GET_detail, GET_add_bookmark, GET_remove_bookmark } from "../api/endpoints"
import { MapContainer } from "../components/shared/google-map"
import { extractParametersFromSearch } from "../functions/form-functions"
import { MapListingFromResponse } from "../api/mapper"
import { GetUserId } from "../components/login/lib/cookies"
import ReviewItem from "../components/detail/review/review-item"
import Helmet from "react-helmet"


const useMock = false

class DetailPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      error: false,
      currentListing: defaultDetailMock,
      page: 1,
      listingId: "",
      userId: GetUserId()
    }
  }

  componentDidMount() {

    if (this.props.detailState !== undefined) {

      console.log("DETAIL PROPS", this.props.detailState.listing)
      this.setState({
        loading: false,
        currentListing: defaultDetailMock
      })

    } else {
      const { location, id } = this.props;
      const { state, search } = location;
      const listingIdFromLocation = this.extractParametersFromSearch(search);
      this.setState({
        listingId: listingIdFromLocation
      });

      this.GetDetail(listingIdFromLocation)
    }

  }


  extractParametersFromSearch(search) {
    return search.split('=')[1];
  }

  UnBookmarkListing = () => {
    console.log("Un Bookmarking listing")
    this.setState({ loading: true })
    axios
      .get(GET_remove_bookmark(GetUserId(), this.state.listingId))
      .then(response => {
        this.setState({
          loading: false,
          currentListing: {
            ...this.state.currentListing,
            isBookmarked: false
          }
        })
      })
      .catch(error => {
        console.log("Bad response", error)
        this.setState({ loading: false, error })
      })
  }

  BookmarkListing = () => {
    console.log("Bookmarking listing")
    this.setState({ loading: true })
    axios
      .get(GET_add_bookmark(GetUserId(), this.state.listingId))
      .then(response => {
        console.log("Added bookmark", response)
        const detail = response.data
        this.setState({
          loading: false,
          currentListing: {
            ...this.state.currentListing,
            isBookmarked: true
          }
        })
        console.log("Set state", this.state)
      })
      .catch(error => {
        console.log("Bad response", error)
        this.setState({ loading: false, error })
      })
  }

  UpdateReviewsFn = (review) => {
    console.log("Updating reviews", review)
    var review = {
      username: review.username,
      gravatar: review.userProfilePicUrl,
      userVerified: review.userVerified,
      dateCreated: review.timestamp,
      rating: review.rating,
      reviewText: review.reviewText,
      images: review.images,
      reviewUpCount: review.helpfulReviewCount,
      reviewId: review.id,
      userId: this.state.userId
    }
    this.setState({
      loading: false,
      currentListing: {
        ...this.state.currentListing,
        reviews: [...this.state.currentListing.reviews, review]

      }
    })
    console.log("reviews state ", this.state.reviews)
  }

  GetDetail = listingId => {
    console.log("Getting detail ...")
    this.setState({ loading: true, listingId: listingId })
    axios
      .get(`${GET_detail}${listingId}/${GetUserId()}`)
      .then(response => {
        console.log("Good response", response)
        const detail = response.data
        this.setState({
          loading: false,
          currentListing: MapListingFromResponse(detail),
        })
        console.log("Set state", this.state)
      })
      .catch(error => {
        console.log("Bad response", error)
        this.setState({ loading: false, error })
      })
  }

  render() {
    const {
      title,
      category,
      keywords,
      subCategory,
      location,
      suburb,
      description,
      phoneNumber,
      email,
      socialMedia,
      delivery,
      collection,
      pricePerDay,
      pricePerWeek,
      gps,
      verified,
      bookmarkCount,
      ratings,
      owner,
      terms,
      images,
      reviews,
      bookingFriendlyDates,
      listingId,
      isBookmarked
    } = this.state.currentListing

    console.log("LISTING FROM STATE", this.state.currentListing)
    const reviewCount = 1;
    const _suburb = (_suburbs.filter((val) => { return val.id === suburb })[0]);
    const _suburbName = _suburb === undefined ? "" : _suburb.suburb;

    return (
      <Layout bodyCss="" displayHeader={true}>
        <Helmet>
          <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeC3c3gl9CD4fOe7Y30uyIl3GFrLg98LY&libraries=places&callback=logInit"></script>
          <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
          {function logInit() {
            console.log("maps loaded")

          }}
        </Helmet>
        <SEO title={`Listing || ${title}`} />


        <Slider images={images} />

        <div className="container">
          <div className="row sticky-wrapper">
            <div className="col-lg-8 col-md-8 padding-right-30">
              <TitleBar
                title={title}
                category={category}
                subCategory={subCategory}
                location={location}
                reviewCount={reviewCount}
              />
              <Nav />
              <Overview
                description={description}
                terms={terms}
                phoneNumber={phoneNumber}
                email={email}
                socialMedia={socialMedia}
                delivery={delivery}
                collection={collection}
              />
              <Pricing
                title={title}
                pricePerDay={pricePerDay}
                pricePerWeek={pricePerWeek}
              />
              {/* <ListingMap gps={gps} /> */}

              <div className="pricing-list-container">
                <h3 className="listing-desc-headline margin-top-70 margin-bottom-30">
                  Location
                      </h3>
                <h4>{`${_suburbName}, ${location}`} </h4>
                <p></p>
                <p></p>
                <MapContainer gps={_suburbName} />
              </div>
              {/* <!-- Reviews --> */}
              <div id="listing-reviews" className="listing-section">
                <h3 className="listing-desc-headline margin-top-75 margin-bottom-20">
                  {/* Reviews <span>({reviewCount})</span> */}
                </h3>
                {/* <RatingOverview ratings={ratings} /> */}
                <div className="clearfix"></div>
                {/* <Review reviews={reviews} /> */}
                {/* <!-- Reviews --> */}
                <section className="comments listing-reviews">
                  <ul>
                    {reviews.map(review => (
                      <ReviewItem
                        username={review.username}
                        gravatar={review.userProfilePicUrl}
                        userVerified={review.userVerified}
                        dateCreated={review.timestamp}
                        rating={review.rating}
                        reviewText={review.reviewText}
                        images={review.images}
                        reviewUpCount={review.helpfulReviewCount}
                        reviewId={review.id}
                        userId={this.state.userId}
                      />
                    ))}
                  </ul>
                </section>
                <Pagination />
              </div>
              <AddReview UpdateReviewsFn={this.UpdateReviewsFn} listingId={this.state.listingId} userId={this.state.userId} />
            </div>
            <SideBar
              verified={verified}
              owner={owner}
              socialMedia={socialMedia}
              bookmarkCount={bookmarkCount}
              listingId={listingId}
              bookingFriendlyDates={bookingFriendlyDates}
              isBookmarked={isBookmarked}
              bookmarkFn={this.BookmarkListing}
              removeBookmarkFn={this.UnBookmarkListing}
            />
          </div>
        </div>

        {/* <!-- Back To Top Button --> */}
        <div id="backtotop">
          <a href="#"></a>
        </div>

        {/* <!-- Booking Sticky Footer --> */}
        <div className="booking-sticky-footer">
          <div className="container">
            <div className="bsf-left">
              <h4>Starting from R{pricePerDay}</h4>
              <div className="star-rating" data-rating={0}>
                <div className="rating-counter"></div>
              </div>
            </div>
            <div className="bsf-right">
              <a href="#booking-widget-anchor" className="button">
                Book Now
              </a>
            </div>
          </div>
        </div>

        )
      }
        {/* 
<!-- Scripts
================================================== -->
<script type="text/javascript" src="scripts/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="scripts/jquery-migrate-3.1.0.min.js"></script>
<script type="text/javascript" src="scripts/mmenu.min.js"></script>
<script type="text/javascript" src="scripts/chosen.min.js"></script>
<script type="text/javascript" src="scripts/slick.min.js"></script>
<script type="text/javascript" src="scripts/rangeslider.min.js"></script>
<script type="text/javascript" src="scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="scripts/waypoints.min.js"></script>
<script type="text/javascript" src="scripts/counterup.min.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="scripts/tooltips.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>

<!-- Maps -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
<script type="text/javascript" src="scripts/infobox.min.js"></script>
<script type="text/javascript" src="scripts/markerclusterer.js"></script>
<script type="text/javascript" src="scripts/maps.js"></script>	

<!-- Booking Widget - Quantity Buttons -->
<script src="scripts/quantityButtons.js"></script>

<!-- Date Range Picker - docs: http://www.daterangepicker.com/ -->
<script src="scripts/moment.min.js"></script>
<script src="scripts/daterangepicker.js"></script> */}
        {/* <script>
// Calendar Init
$(function() {
	$('#date-picker').daterangepicker({
		"opens": "left",
		singleDatePicker: true,

		// Disabling Date Ranges
		isInvalidDate: function(date) {
		// Disabling Date Range
		var disabled_start = moment('09/02/2018', 'MM/DD/YYYY');
		var disabled_end = moment('09/06/2018', 'MM/DD/YYYY');
		return date.isAfter(disabled_start) && date.isBefore(disabled_end);

		// Disabling Single Day
		// if (date.format('MM/DD/YYYY') == '08/08/2018') {
		//     return true; 
		// }
		}
	});
});

// Calendar animation
$('#date-picker').on('showCalendar.daterangepicker', function(ev, picker) {
	$('.daterangepicker').addClass('calendar-animated');
});
$('#date-picker').on('show.daterangepicker', function(ev, picker) {
	$('.daterangepicker').addClass('calendar-visible');
	$('.daterangepicker').removeClass('calendar-hidden');
});
$('#date-picker').on('hide.daterangepicker', function(ev, picker) {
	$('.daterangepicker').removeClass('calendar-visible');
	$('.daterangepicker').addClass('calendar-hidden');
});
</script>


<!-- Replacing dropdown placeholder with selected time slot -->
<script>
$(".time-slot").each(function() {
	var timeSlot = $(this);
	$(this).find('input').on('change',function() {
		var timeSlotVal = timeSlot.find('strong').text();

		$('.panel-dropdown.time-slots-dropdown a').html(timeSlotVal);
		$('.panel-dropdown').removeClass('active');
	});
});
</script>  */}
      </Layout >
    )
  }

}
export default DetailPage
