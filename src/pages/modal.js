// pages/modal-example.js

import React from 'react'
import { Link } from 'gatsby'
import { ModalRoutingContext } from 'gatsby-plugin-modal-routing'
import Profile from '../components/admin/dashboard/profile'
import DetailPage from './detail'

const Modal = (props) => {

  const { location } = props;
  const { state } = location;

  return (

    <ModalRoutingContext.Consumer>

      {({ modal, closeTo }) => (
        <div>
          {modal ? (
            <Link to={closeTo}>
              Close
          </Link>
          ) : (
              <header>
                <h1>
                  {/*{state.title? null}*/}
                </h1>
              </header>
            )}

          {state !== undefined && state.isUserProfile === true ? <Profile modal={true} userId={state.userProfile.id} /> : ""}
          {state !== undefined && state.isDetail === true ? <DetailPage detailState={state.detailState} /> : ""}


          <Link to="/">Go back to the homepage</Link>
        </div>
      )}
    </ModalRoutingContext.Consumer>
  )
}
export default Modal