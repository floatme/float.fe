import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import headerImage from "../images/header.png"
import Banner from "../components/home/banner"

import Categories from "../components/home/categories"
import FeaturedListings from "../components/home/featured-listings"

import InfoSection from "../components/home/info-section"
import Testimonials from "../components/home/testimonials"
import FeaturedBlogPosts from "../components/home/featured-blogposts"

const IndexPage = () => (
  <Layout bodyCss="transparent-header" displayHeader={true}>
    <SEO title="Just Float" />

    <div className="clearfix"></div>
    <Banner />
    <Categories />
    <FeaturedListings />
    <InfoSection />
    {/*<Testimonials />*/}
    <FeaturedBlogPosts />

    {/* <!-- Back To Top Button --> */}
    <div id="backtotop">
      <a href="#"></a>
    </div>

    {/*<Link to="/listings">Go to listing</Link>*/}
  </Layout>
);

export default IndexPage
