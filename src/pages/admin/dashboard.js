import React, { Component } from "react"


// import { Link } from 'gatsby-plugin-modal-routing'
import PrivateLayout from "../../components/private-layout"
import SEO from "../../components/seo"
import Navigation from "../../components/admin/dashboard/navigation";
import DashboardContent from "../../components/admin/dashboard/content";
import { DashBoardPageTypeEnum } from "../../components/admin/dashboard/enums/DashboardPageTypeEnum";
import { DashBoardPageTitles } from "../../components/admin/dashboard/enums/DashboardPageTypeEnum";
import { extractParamFromLocation } from "../../functions/form-functions";

class Dashboard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showLoader: true,
			currentPage: DashBoardPageTypeEnum.HOME
		};
	}


	changePage = (page) => {
		console.log("[TRACE]: Navigate ", page);
		this.setState({ showLoader: true, currentPage: page });
	};

	componentDidMount() {

		const { location, id } = this.props;
		const { search } = location;
		if (search != undefined) {
			const pageType = extractParamFromLocation(search, "page");
			if (pageType !== undefined) this.changePage(pageType);
		}

	}

	render() {
		const pageType = extractParamFromLocation(this.props.location.search, "page");
		console.log("PAGE TEST", extractParamFromLocation(this.props.location.search, "page"));

		return (<>
			<PrivateLayout
				bodyCss="transparent-header"
				headerContainerCss="fixed fullwidth dashboard"
				headerCss="not-sticky"
				displaySignUp="true"
				hideFooter="true"
			>
				<SEO title="Dashboard" />
				{/* <!-- Dashboard--> */}
				<div id="dashboard">

					<Navigation pageType={pageType} changePageFn={this.changePage} />

					<DashboardContent location={this.props.location.search} changePageFn={this.changePage} page={this.state.currentPage} title={DashBoardPageTitles[this.state.currentPage]} />

				</div>
				{/* <!-- Dashboard / End--> */}

			</PrivateLayout>

		</>)
	}
}



export default Dashboard
