import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import LoginMarkup from "../components/login/index"

const Login = () => (
    <Layout displayHeader={false} bodyCss="transparent-header">
        <SEO title="Just Float || Login"/>
        <LoginMarkup/>
        <div className="clearfix"></div>
    </Layout>
);
export default Login;