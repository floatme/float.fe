import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import RegisterMarkup from "../components/register";

const Register = () => (
    <Layout displayHeader={false} bodyCss="transparent-header">
        <SEO title="Just Float || Login" />
        <RegisterMarkup />
        <div className="clearfix"></div>
    </Layout>
);
export default Register;