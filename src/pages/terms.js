import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import headerImage from "../images/header.png";
import Banner from "../components/home/banner";


const Terms = () => (
	<Layout>
		<SEO title="Page two" />


		<div className="clearfix"></div>
		{/* <!-- Header Container / End --> */}



		{/* <!-- Footer */}
		{/* ================================================== --> */}
		<div id="footer" className="sticky-footer">
			{/* <!-- Main --> */}
			<div className="container">
				<div className="row">
					<div className="col-md-5 col-sm-6">
						<img className="footer-logo" src="images/logo-2.png" alt="" />
						<br /><br />
						<p>Lend and borrow stuff on the go.</p>
					</div>

					<div className="col-md-4 col-sm-6 ">
						<h4>Helpful Links</h4>
						<ul className="footer-links">
							<li><a href="#">Login</a></li>
							<li><a href="#">Sign Up</a></li>
							<li><a href="#">Pricing</a></li>
							<li><a href="#">Privacy Policy</a></li>
						</ul>

						<ul className="footer-links">
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Blog</a></li>
							<li><a href="#">Our Partners</a></li>
							<li><a href="#">How It Works</a></li>
							<li><a href="#">Contact</a></li>
						</ul>
						<div className="clearfix"></div>
					</div>

					<div className="col-md-3  col-sm-12">
						<h4>Contact Us</h4>
						<div className="text-widget">
							<span>80 Kathrine St, Sandton</span> <br />
							E-Mail:<span> <a href="#">hi@floatme.com</a> </span><br />
						</div>

						<ul className="social-icons margin-top-20">
							<li><a className="facebook" href="#"><i className="icon-facebook"></i></a></li>
							<li><a className="twitter" href="#"><i className="icon-twitter"></i></a></li>
							<li><a className="instagram" href="#"><i className="icon-instagram"></i></a></li>
						</ul>

					</div>

				</div>

				{/* <!-- Copyright --> */}
				<div className="row">
					<div className="col-md-12">
						<div className="copyrights">© 2019 Float. All Rights Reserved.</div>
					</div>
				</div>

			</div>

		</div>
		{/* <!-- Footer / End --> */}


		{/* <!-- Back To Top Button --> */}
		<div id="backtotop"><a href="#"></a></div>


		{/* </div> */}
		{/* <!-- Wrapper / End --> */}



		{/* <!-- Leaflet // Docs: https://leafletjs.com/ --> */}
		{/* <script src="scripts/leaflet.min.js"></script> */}

		{/* <!-- Leaflet Maps Scripts --> */}
		{/* <script src="scripts/leaflet-markercluster.min.js"></script>
<script src="scripts/leaflet-gesture-handling.min.js"></script>
<script src="scripts/leaflet-Float.js"></script> */}

		{/* <!-- Leaflet Geocoder + Search Autocomplete // Docs: https://github.com/perliedman/leaflet-control-geocoder --> */}
		{/* <script src="scripts/leaflet-autocomplete.js"></script>
<script src="scripts/leaflet-control-geocoder.js"></script> */}


		{/* <!-- Typed Script --> */}
		{/* <script type="text/javascript" src="scripts/typed.js"></script> */}
		{/* <script>
  
var typed = new Typed('.typed-words', {
strings: ["Cameras"," Textbooks","Tents", "Bike", "Camping gear"],
	typeSpeed: 80,
	backSpeed: 80,
	backDelay: 4000,
	startDelay: 1000,
	loop: true,
	showCursor: true
});
</script> */}


		{/* <!-- Style Switcher */}
		{/* ================================================== --> */}
		{/* <script src="scripts/switcher.js"></script> */}

		<div id="style-switcher">
			<h2>Color Switcher <a href="#"><i className="sl sl-icon-settings"></i></a></h2>

			<div>
				<ul className="colors" id="color1">
					<li><a href="#" className="main" title="Main"></a></li>
					<li><a href="#" className="blue" title="Blue"></a></li>
					<li><a href="#" className="green" title="Green"></a></li>
					<li><a href="#" className="orange" title="Orange"></a></li>
					<li><a href="#" className="navy" title="Navy"></a></li>
					<li><a href="#" className="yellow" title="Yellow"></a></li>
					<li><a href="#" className="peach" title="Peach"></a></li>
					<li><a href="#" className="beige" title="Beige"></a></li>
					<li><a href="#" className="purple" title="Purple"></a></li>
					<li><a href="#" className="celadon" title="Celadon"></a></li>
					<li><a href="#" className="red" title="Red"></a></li>
					<li><a href="#" className="brown" title="Brown"></a></li>
					<li><a href="#" className="cherry" title="Cherry"></a></li>
					<li><a href="#" className="cyan" title="Cyan"></a></li>
					<li><a href="#" className="gray" title="Gray"></a></li>
					<li><a href="#" className="olive" title="Olive"></a></li>
				</ul>
			</div>

		</div>
		{/* <!-- Style Switcher / End --> */}

		<Link to="/">Go back to the homepage</Link>
	</Layout>
)

export default Terms
