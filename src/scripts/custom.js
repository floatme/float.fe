/* Chosen v1.6.2 | (c) 2011-2016 by Harvest | MIT License, https://github.com/harvesthq/chosen/blob/master/LICENSE.md */
(function () { var a, b, c, d, e, f = {}.hasOwnProperty, g = function (a, b) { function d() { this.constructor = a } for (var c in b) f.call(b, c) && (a[c] = b[c]); return d.prototype = b.prototype, a.prototype = new d, a.__super__ = b.prototype, a }; d = function () { function a() { this.options_index = 0, this.parsed = [] } return a.prototype.add_node = function (a) { return "OPTGROUP" === a.nodeName.toUpperCase() ? this.add_group(a) : this.add_option(a) }, a.prototype.add_group = function (a) { var b, c, d, e, f, g; for (b = this.parsed.length, this.parsed.push({ array_index: b, group: !0, label: this.escapeExpression(a.label), title: a.title ? a.title : void 0, children: 0, disabled: a.disabled, classes: a.className }), f = a.childNodes, g = [], d = 0, e = f.length; d < e; d++)c = f[d], g.push(this.add_option(c, b, a.disabled)); return g }, a.prototype.add_option = function (a, b, c) { if ("OPTION" === a.nodeName.toUpperCase()) return "" !== a.text ? (null != b && (this.parsed[b].children += 1), this.parsed.push({ array_index: this.parsed.length, options_index: this.options_index, value: a.value, text: a.text, html: a.innerHTML, title: a.title ? a.title : void 0, selected: a.selected, disabled: c === !0 ? c : a.disabled, group_array_index: b, group_label: null != b ? this.parsed[b].label : null, classes: a.className, style: a.style.cssText })) : this.parsed.push({ array_index: this.parsed.length, options_index: this.options_index, empty: !0 }), this.options_index += 1 }, a.prototype.escapeExpression = function (a) { var b, c; return null == a || a === !1 ? "" : /[\&\<\>\"\'\`]/.test(a) ? (b = { "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#x27;", "`": "&#x60;" }, c = /&(?!\w+;)|[\<\>\"\'\`]/g, a.replace(c, function (a) { return b[a] || "&amp;" })) : a }, a }(), d.select_to_array = function (a) { var b, c, e, f, g; for (c = new d, g = a.childNodes, e = 0, f = g.length; e < f; e++)b = g[e], c.add_node(b); return c.parsed }, b = function () { function a(b, c) { this.form_field = b, this.options = null != c ? c : {}, a.browser_is_supported() && (this.is_multiple = this.form_field.multiple, this.set_default_text(), this.set_default_values(), this.setup(), this.set_up_html(), this.register_observers(), this.on_ready()) } return a.prototype.set_default_values = function () { var a = this; return this.click_test_action = function (b) { return a.test_active_click(b) }, this.activate_action = function (b) { return a.activate_field(b) }, this.active_field = !1, this.mouse_on_container = !1, this.results_showing = !1, this.result_highlighted = null, this.allow_single_deselect = null != this.options.allow_single_deselect && null != this.form_field.options[0] && "" === this.form_field.options[0].text && this.options.allow_single_deselect, this.disable_search_threshold = this.options.disable_search_threshold || 0, this.disable_search = this.options.disable_search || !1, this.enable_split_word_search = null == this.options.enable_split_word_search || this.options.enable_split_word_search, this.group_search = null == this.options.group_search || this.options.group_search, this.search_contains = this.options.search_contains || !1, this.single_backstroke_delete = null == this.options.single_backstroke_delete || this.options.single_backstroke_delete, this.max_selected_options = this.options.max_selected_options || 1 / 0, this.inherit_select_classes = this.options.inherit_select_classes || !1, this.display_selected_options = null == this.options.display_selected_options || this.options.display_selected_options, this.display_disabled_options = null == this.options.display_disabled_options || this.options.display_disabled_options, this.include_group_label_in_selected = this.options.include_group_label_in_selected || !1, this.max_shown_results = this.options.max_shown_results || Number.POSITIVE_INFINITY, this.case_sensitive_search = this.options.case_sensitive_search || !1 }, a.prototype.set_default_text = function () { return this.form_field.getAttribute("data-placeholder") ? this.default_text = this.form_field.getAttribute("data-placeholder") : this.is_multiple ? this.default_text = this.options.placeholder_text_multiple || this.options.placeholder_text || a.default_multiple_text : this.default_text = this.options.placeholder_text_single || this.options.placeholder_text || a.default_single_text, this.results_none_found = this.form_field.getAttribute("data-no_results_text") || this.options.no_results_text || a.default_no_result_text }, a.prototype.choice_label = function (a) { return this.include_group_label_in_selected && null != a.group_label ? "<b class='group-name'>" + a.group_label + "</b>" + a.html : a.html }, a.prototype.mouse_enter = function () { return this.mouse_on_container = !0 }, a.prototype.mouse_leave = function () { return this.mouse_on_container = !1 }, a.prototype.input_focus = function (a) { var b = this; if (this.is_multiple) { if (!this.active_field) return setTimeout(function () { return b.container_mousedown() }, 50) } else if (!this.active_field) return this.activate_field() }, a.prototype.input_blur = function (a) { var b = this; if (!this.mouse_on_container) return this.active_field = !1, setTimeout(function () { return b.blur_test() }, 100) }, a.prototype.results_option_build = function (a) { var b, c, d, e, f, g, h; for (b = "", e = 0, h = this.results_data, f = 0, g = h.length; f < g && (c = h[f], d = "", d = c.group ? this.result_add_group(c) : this.result_add_option(c), "" !== d && (e++ , b += d), (null != a ? a.first : void 0) && (c.selected && this.is_multiple ? this.choice_build(c) : c.selected && !this.is_multiple && this.single_set_selected_text(this.choice_label(c))), !(e >= this.max_shown_results)); f++); return b }, a.prototype.result_add_option = function (a) { var b, c; return a.search_match && this.include_option_in_results(a) ? (b = [], a.disabled || a.selected && this.is_multiple || b.push("active-result"), !a.disabled || a.selected && this.is_multiple || b.push("disabled-result"), a.selected && b.push("result-selected"), null != a.group_array_index && b.push("group-option"), "" !== a.classes && b.push(a.classes), c = document.createElement("li"), c.className = b.join(" "), c.style.cssText = a.style, c.setAttribute("data-option-array-index", a.array_index), c.innerHTML = a.search_text, a.title && (c.title = a.title), this.outerHTML(c)) : "" }, a.prototype.result_add_group = function (a) { var b, c; return (a.search_match || a.group_match) && a.active_options > 0 ? (b = [], b.push("group-result"), a.classes && b.push(a.classes), c = document.createElement("li"), c.className = b.join(" "), c.innerHTML = a.search_text, a.title && (c.title = a.title), this.outerHTML(c)) : "" }, a.prototype.results_update_field = function () { if (this.set_default_text(), this.is_multiple || this.results_reset_cleanup(), this.result_clear_highlight(), this.results_build(), this.results_showing) return this.winnow_results() }, a.prototype.reset_single_select_options = function () { var a, b, c, d, e; for (d = this.results_data, e = [], b = 0, c = d.length; b < c; b++)a = d[b], a.selected ? e.push(a.selected = !1) : e.push(void 0); return e }, a.prototype.results_toggle = function () { return this.results_showing ? this.results_hide() : this.results_show() }, a.prototype.results_search = function (a) { return this.results_showing ? this.winnow_results() : this.results_show() }, a.prototype.winnow_results = function () { var a, b, c, d, e, f, g, h, i, j, k, l; for (this.no_results_clear(), d = 0, f = this.get_search_text(), a = f.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"), i = new RegExp(a, "i"), c = this.get_search_regex(a), l = this.results_data, j = 0, k = l.length; j < k; j++)b = l[j], b.search_match = !1, e = null, this.include_option_in_results(b) && (b.group && (b.group_match = !1, b.active_options = 0), null != b.group_array_index && this.results_data[b.group_array_index] && (e = this.results_data[b.group_array_index], 0 === e.active_options && e.search_match && (d += 1), e.active_options += 1), b.search_text = b.group ? b.label : b.html, b.group && !this.group_search || (b.search_match = this.search_string_match(b.search_text, c), b.search_match && !b.group && (d += 1), b.search_match ? (f.length && (g = b.search_text.search(i), h = b.search_text.substr(0, g + f.length) + "</em>" + b.search_text.substr(g + f.length), b.search_text = h.substr(0, g) + "<em>" + h.substr(g)), null != e && (e.group_match = !0)) : null != b.group_array_index && this.results_data[b.group_array_index].search_match && (b.search_match = !0))); return this.result_clear_highlight(), d < 1 && f.length ? (this.update_results_content(""), this.no_results(f)) : (this.update_results_content(this.results_option_build()), this.winnow_results_set_highlight()) }, a.prototype.get_search_regex = function (a) { var b, c; return b = this.search_contains ? "" : "^", c = this.case_sensitive_search ? "" : "i", new RegExp(b + a, c) }, a.prototype.search_string_match = function (a, b) { var c, d, e, f; if (b.test(a)) return !0; if (this.enable_split_word_search && (a.indexOf(" ") >= 0 || 0 === a.indexOf("[")) && (d = a.replace(/\[|\]/g, "").split(" "), d.length)) for (e = 0, f = d.length; e < f; e++)if (c = d[e], b.test(c)) return !0 }, a.prototype.choices_count = function () { var a, b, c, d; if (null != this.selected_option_count) return this.selected_option_count; for (this.selected_option_count = 0, d = this.form_field.options, b = 0, c = d.length; b < c; b++)a = d[b], a.selected && (this.selected_option_count += 1); return this.selected_option_count }, a.prototype.choices_click = function (a) { if (a.preventDefault(), !this.results_showing && !this.is_disabled) return this.results_show() }, a.prototype.keyup_checker = function (a) { var b, c; switch (b = null != (c = a.which) ? c : a.keyCode, this.search_field_scale(), b) { case 8: if (this.is_multiple && this.backstroke_length < 1 && this.choices_count() > 0) return this.keydown_backstroke(); if (!this.pending_backstroke) return this.result_clear_highlight(), this.results_search(); break; case 13: if (a.preventDefault(), this.results_showing) return this.result_select(a); break; case 27: return this.results_showing && this.results_hide(), !0; case 9: case 38: case 40: case 16: case 91: case 17: case 18: break; default: return this.results_search() } }, a.prototype.clipboard_event_checker = function (a) { var b = this; return setTimeout(function () { return b.results_search() }, 50) }, a.prototype.container_width = function () { return null != this.options.width ? this.options.width : "" + this.form_field.offsetWidth + "px" }, a.prototype.include_option_in_results = function (a) { return !(this.is_multiple && !this.display_selected_options && a.selected) && (!(!this.display_disabled_options && a.disabled) && !a.empty) }, a.prototype.search_results_touchstart = function (a) { return this.touch_started = !0, this.search_results_mouseover(a) }, a.prototype.search_results_touchmove = function (a) { return this.touch_started = !1, this.search_results_mouseout(a) }, a.prototype.search_results_touchend = function (a) { if (this.touch_started) return this.search_results_mouseup(a) }, a.prototype.outerHTML = function (a) { var b; return a.outerHTML ? a.outerHTML : (b = document.createElement("div"), b.appendChild(a), b.innerHTML) }, a.browser_is_supported = function () { return "Microsoft Internet Explorer" === window.navigator.appName ? document.documentMode >= 8 : !(/iP(od|hone)/i.test(window.navigator.userAgent) || /IEMobile/i.test(window.navigator.userAgent) || /Windows Phone/i.test(window.navigator.userAgent) || /BlackBerry/i.test(window.navigator.userAgent) || /BB10/i.test(window.navigator.userAgent) || /Android.*Mobile/i.test(window.navigator.userAgent)) }, a.default_multiple_text = "Select Some Options", a.default_single_text = "Select an Option", a.default_no_result_text = "No results match", a }(), a = jQuery, a.fn.extend({ chosen: function (d) { return b.browser_is_supported() ? this.each(function (b) { var e, f; return e = a(this), f = e.data("chosen"), "destroy" === d ? void (f instanceof c && f.destroy()) : void (f instanceof c || e.data("chosen", new c(this, d))) }) : this } }), c = function (b) { function c() { return e = c.__super__.constructor.apply(this, arguments) } return g(c, b), c.prototype.setup = function () { return this.form_field_jq = a(this.form_field), this.current_selectedIndex = this.form_field.selectedIndex, this.is_rtl = this.form_field_jq.hasClass("chosen-rtl") }, c.prototype.set_up_html = function () { var b, c; return b = ["chosen-container"], b.push("chosen-container-" + (this.is_multiple ? "multi" : "single")), this.inherit_select_classes && this.form_field.className && b.push(this.form_field.className), this.is_rtl && b.push("chosen-rtl"), c = { class: b.join(" "), style: "width: " + this.container_width() + ";", title: this.form_field.title }, this.form_field.id.length && (c.id = this.form_field.id.replace(/[^\w]/g, "_") + "_chosen"), this.container = a("<div />", c), this.is_multiple ? this.container.html('<ul class="chosen-choices"><li class="search-field"><input type="text" value="' + this.default_text + '" class="default" autocomplete="off" style="width:25px;" /></li></ul><div class="chosen-drop"><ul class="chosen-results"></ul></div>') : this.container.html('<a class="chosen-single chosen-default"><span>' + this.default_text + '</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off" /></div><ul class="chosen-results"></ul></div>'), this.form_field_jq.hide().after(this.container), this.dropdown = this.container.find("div.chosen-drop").first(), this.search_field = this.container.find("input").first(), this.search_results = this.container.find("ul.chosen-results").first(), this.search_field_scale(), this.search_no_results = this.container.find("li.no-results").first(), this.is_multiple ? (this.search_choices = this.container.find("ul.chosen-choices").first(), this.search_container = this.container.find("li.search-field").first()) : (this.search_container = this.container.find("div.chosen-search").first(), this.selected_item = this.container.find(".chosen-single").first()), this.results_build(), this.set_tab_index(), this.set_label_behavior() }, c.prototype.on_ready = function () { return this.form_field_jq.trigger("chosen:ready", { chosen: this }) }, c.prototype.register_observers = function () { var a = this; return this.container.bind("touchstart.chosen", function (b) { return a.container_mousedown(b), b.preventDefault() }), this.container.bind("touchend.chosen", function (b) { return a.container_mouseup(b), b.preventDefault() }), this.container.bind("mousedown.chosen", function (b) { a.container_mousedown(b) }), this.container.bind("mouseup.chosen", function (b) { a.container_mouseup(b) }), this.container.bind("mouseenter.chosen", function (b) { a.mouse_enter(b) }), this.container.bind("mouseleave.chosen", function (b) { a.mouse_leave(b) }), this.search_results.bind("mouseup.chosen", function (b) { a.search_results_mouseup(b) }), this.search_results.bind("mouseover.chosen", function (b) { a.search_results_mouseover(b) }), this.search_results.bind("mouseout.chosen", function (b) { a.search_results_mouseout(b) }), this.search_results.bind("mousewheel.chosen DOMMouseScroll.chosen", function (b) { a.search_results_mousewheel(b) }), this.search_results.bind("touchstart.chosen", function (b) { a.search_results_touchstart(b) }), this.search_results.bind("touchmove.chosen", function (b) { a.search_results_touchmove(b) }), this.search_results.bind("touchend.chosen", function (b) { a.search_results_touchend(b) }), this.form_field_jq.bind("chosen:updated.chosen", function (b) { a.results_update_field(b) }), this.form_field_jq.bind("chosen:activate.chosen", function (b) { a.activate_field(b) }), this.form_field_jq.bind("chosen:open.chosen", function (b) { a.container_mousedown(b) }), this.form_field_jq.bind("chosen:close.chosen", function (b) { a.input_blur(b) }), this.search_field.bind("blur.chosen", function (b) { a.input_blur(b) }), this.search_field.bind("keyup.chosen", function (b) { a.keyup_checker(b) }), this.search_field.bind("keydown.chosen", function (b) { a.keydown_checker(b) }), this.search_field.bind("focus.chosen", function (b) { a.input_focus(b) }), this.search_field.bind("cut.chosen", function (b) { a.clipboard_event_checker(b) }), this.search_field.bind("paste.chosen", function (b) { a.clipboard_event_checker(b) }), this.is_multiple ? this.search_choices.bind("click.chosen", function (b) { a.choices_click(b) }) : this.container.bind("click.chosen", function (a) { a.preventDefault() }) }, c.prototype.destroy = function () { return a(this.container[0].ownerDocument).unbind("click.chosen", this.click_test_action), this.search_field[0].tabIndex && (this.form_field_jq[0].tabIndex = this.search_field[0].tabIndex), this.container.remove(), this.form_field_jq.removeData("chosen"), this.form_field_jq.show() }, c.prototype.search_field_disabled = function () { return this.is_disabled = this.form_field_jq[0].disabled, this.is_disabled ? (this.container.addClass("chosen-disabled"), this.search_field[0].disabled = !0, this.is_multiple || this.selected_item.unbind("focus.chosen", this.activate_action), this.close_field()) : (this.container.removeClass("chosen-disabled"), this.search_field[0].disabled = !1, this.is_multiple ? void 0 : this.selected_item.bind("focus.chosen", this.activate_action)) }, c.prototype.container_mousedown = function (b) { if (!this.is_disabled && (b && "mousedown" === b.type && !this.results_showing && b.preventDefault(), null == b || !a(b.target).hasClass("search-choice-close"))) return this.active_field ? this.is_multiple || !b || a(b.target)[0] !== this.selected_item[0] && !a(b.target).parents("a.chosen-single").length || (b.preventDefault(), this.results_toggle()) : (this.is_multiple && this.search_field.val(""), a(this.container[0].ownerDocument).bind("click.chosen", this.click_test_action), this.results_show()), this.activate_field() }, c.prototype.container_mouseup = function (a) { if ("ABBR" === a.target.nodeName && !this.is_disabled) return this.results_reset(a) }, c.prototype.search_results_mousewheel = function (a) { var b; if (a.originalEvent && (b = a.originalEvent.deltaY || -a.originalEvent.wheelDelta || a.originalEvent.detail), null != b) return a.preventDefault(), "DOMMouseScroll" === a.type && (b = 40 * b), this.search_results.scrollTop(b + this.search_results.scrollTop()) }, c.prototype.blur_test = function (a) { if (!this.active_field && this.container.hasClass("chosen-container-active")) return this.close_field() }, c.prototype.close_field = function () { return a(this.container[0].ownerDocument).unbind("click.chosen", this.click_test_action), this.active_field = !1, this.results_hide(), this.container.removeClass("chosen-container-active"), this.clear_backstroke(), this.show_search_field_default(), this.search_field_scale() }, c.prototype.activate_field = function () { return this.container.addClass("chosen-container-active"), this.active_field = !0, this.search_field.val(this.search_field.val()), this.search_field.focus() }, c.prototype.test_active_click = function (b) { var c; return c = a(b.target).closest(".chosen-container"), c.length && this.container[0] === c[0] ? this.active_field = !0 : this.close_field() }, c.prototype.results_build = function () { return this.parsing = !0, this.selected_option_count = null, this.results_data = d.select_to_array(this.form_field), this.is_multiple ? this.search_choices.find("li.search-choice").remove() : this.is_multiple || (this.single_set_selected_text(), this.disable_search || this.form_field.options.length <= this.disable_search_threshold ? (this.search_field[0].readOnly = !0, this.container.addClass("chosen-container-single-nosearch")) : (this.search_field[0].readOnly = !1, this.container.removeClass("chosen-container-single-nosearch"))), this.update_results_content(this.results_option_build({ first: !0 })), this.search_field_disabled(), this.show_search_field_default(), this.search_field_scale(), this.parsing = !1 }, c.prototype.result_do_highlight = function (a) { var b, c, d, e, f; if (a.length) { if (this.result_clear_highlight(), this.result_highlight = a, this.result_highlight.addClass("highlighted"), d = parseInt(this.search_results.css("maxHeight"), 10), f = this.search_results.scrollTop(), e = d + f, c = this.result_highlight.position().top, b = c + this.result_highlight.outerHeight(), b >= e) return this.search_results.scrollTop(b - d > 0 ? b - d : 0); if (c < f) return this.search_results.scrollTop(c) } }, c.prototype.result_clear_highlight = function () { return this.result_highlight && this.result_highlight.removeClass("highlighted"), this.result_highlight = null }, c.prototype.results_show = function () { return this.is_multiple && this.max_selected_options <= this.choices_count() ? (this.form_field_jq.trigger("chosen:maxselected", { chosen: this }), !1) : (this.container.addClass("chosen-with-drop"), this.results_showing = !0, this.search_field.focus(), this.search_field.val(this.search_field.val()), this.winnow_results(), this.form_field_jq.trigger("chosen:showing_dropdown", { chosen: this })) }, c.prototype.update_results_content = function (a) { return this.search_results.html(a) }, c.prototype.results_hide = function () { return this.results_showing && (this.result_clear_highlight(), this.container.removeClass("chosen-with-drop"), this.form_field_jq.trigger("chosen:hiding_dropdown", { chosen: this })), this.results_showing = !1 }, c.prototype.set_tab_index = function (a) { var b; if (this.form_field.tabIndex) return b = this.form_field.tabIndex, this.form_field.tabIndex = -1, this.search_field[0].tabIndex = b }, c.prototype.set_label_behavior = function () { var b = this; if (this.form_field_label = this.form_field_jq.parents("label"), !this.form_field_label.length && this.form_field.id.length && (this.form_field_label = a("label[for='" + this.form_field.id + "']")), this.form_field_label.length > 0) return this.form_field_label.bind("click.chosen", function (a) { return b.is_multiple ? b.container_mousedown(a) : b.activate_field() }) }, c.prototype.show_search_field_default = function () { return this.is_multiple && this.choices_count() < 1 && !this.active_field ? (this.search_field.val(this.default_text), this.search_field.addClass("default")) : (this.search_field.val(""), this.search_field.removeClass("default")) }, c.prototype.search_results_mouseup = function (b) { var c; if (c = a(b.target).hasClass("active-result") ? a(b.target) : a(b.target).parents(".active-result").first(), c.length) return this.result_highlight = c, this.result_select(b), this.search_field.focus() }, c.prototype.search_results_mouseover = function (b) { var c; if (c = a(b.target).hasClass("active-result") ? a(b.target) : a(b.target).parents(".active-result").first()) return this.result_do_highlight(c) }, c.prototype.search_results_mouseout = function (b) { if (a(b.target).hasClass("active-result")) return this.result_clear_highlight() }, c.prototype.choice_build = function (b) { var c, d, e = this; return c = a("<li />", { class: "search-choice" }).html("<span>" + this.choice_label(b) + "</span>"), b.disabled ? c.addClass("search-choice-disabled") : (d = a("<a />", { class: "search-choice-close", "data-option-array-index": b.array_index }), d.bind("click.chosen", function (a) { return e.choice_destroy_link_click(a) }), c.append(d)), this.search_container.before(c) }, c.prototype.choice_destroy_link_click = function (b) { if (b.preventDefault(), b.stopPropagation(), !this.is_disabled) return this.choice_destroy(a(b.target)) }, c.prototype.choice_destroy = function (a) { if (this.result_deselect(a[0].getAttribute("data-option-array-index"))) return this.show_search_field_default(), this.is_multiple && this.choices_count() > 0 && this.search_field.val().length < 1 && this.results_hide(), a.parents("li").first().remove(), this.search_field_scale() }, c.prototype.results_reset = function () { if (this.reset_single_select_options(), this.form_field.options[0].selected = !0, this.single_set_selected_text(), this.show_search_field_default(), this.results_reset_cleanup(), this.form_field_jq.trigger("change"), this.active_field) return this.results_hide() }, c.prototype.results_reset_cleanup = function () { return this.current_selectedIndex = this.form_field.selectedIndex, this.selected_item.find("abbr").remove() }, c.prototype.result_select = function (a) { var b, c; if (this.result_highlight) return b = this.result_highlight, this.result_clear_highlight(), this.is_multiple && this.max_selected_options <= this.choices_count() ? (this.form_field_jq.trigger("chosen:maxselected", { chosen: this }), !1) : (this.is_multiple ? b.removeClass("active-result") : this.reset_single_select_options(), b.addClass("result-selected"), c = this.results_data[b[0].getAttribute("data-option-array-index")], c.selected = !0, this.form_field.options[c.options_index].selected = !0, this.selected_option_count = null, this.is_multiple ? this.choice_build(c) : this.single_set_selected_text(this.choice_label(c)), (a.metaKey || a.ctrlKey) && this.is_multiple || this.results_hide(), this.show_search_field_default(), (this.is_multiple || this.form_field.selectedIndex !== this.current_selectedIndex) && this.form_field_jq.trigger("change", { selected: this.form_field.options[c.options_index].value }), this.current_selectedIndex = this.form_field.selectedIndex, a.preventDefault(), this.search_field_scale()) }, c.prototype.single_set_selected_text = function (a) { return null == a && (a = this.default_text), a === this.default_text ? this.selected_item.addClass("chosen-default") : (this.single_deselect_control_build(), this.selected_item.removeClass("chosen-default")), this.selected_item.find("span").html(a) }, c.prototype.result_deselect = function (a) { var b; return b = this.results_data[a], !this.form_field.options[b.options_index].disabled && (b.selected = !1, this.form_field.options[b.options_index].selected = !1, this.selected_option_count = null, this.result_clear_highlight(), this.results_showing && this.winnow_results(), this.form_field_jq.trigger("change", { deselected: this.form_field.options[b.options_index].value }), this.search_field_scale(), !0) }, c.prototype.single_deselect_control_build = function () { if (this.allow_single_deselect) return this.selected_item.find("abbr").length || this.selected_item.find("span").first().after('<abbr class="search-choice-close"></abbr>'), this.selected_item.addClass("chosen-single-with-deselect") }, c.prototype.get_search_text = function () { return a("<div/>").text(a.trim(this.search_field.val())).html() }, c.prototype.winnow_results_set_highlight = function () { var a, b; if (b = this.is_multiple ? [] : this.search_results.find(".result-selected.active-result"), a = b.length ? b.first() : this.search_results.find(".active-result").first(), null != a) return this.result_do_highlight(a) }, c.prototype.no_results = function (b) { var c; return c = a('<li class="no-results">' + this.results_none_found + ' "<span></span>"</li>'), c.find("span").first().html(b), this.search_results.append(c), this.form_field_jq.trigger("chosen:no_results", { chosen: this }) }, c.prototype.no_results_clear = function () { return this.search_results.find(".no-results").remove() }, c.prototype.keydown_arrow = function () { var a; return this.results_showing && this.result_highlight ? (a = this.result_highlight.nextAll("li.active-result").first()) ? this.result_do_highlight(a) : void 0 : this.results_show() }, c.prototype.keyup_arrow = function () { var a; return this.results_showing || this.is_multiple ? this.result_highlight ? (a = this.result_highlight.prevAll("li.active-result"), a.length ? this.result_do_highlight(a.first()) : (this.choices_count() > 0 && this.results_hide(), this.result_clear_highlight())) : void 0 : this.results_show() }, c.prototype.keydown_backstroke = function () { var a; return this.pending_backstroke ? (this.choice_destroy(this.pending_backstroke.find("a").first()), this.clear_backstroke()) : (a = this.search_container.siblings("li.search-choice").last(), a.length && !a.hasClass("search-choice-disabled") ? (this.pending_backstroke = a, this.single_backstroke_delete ? this.keydown_backstroke() : this.pending_backstroke.addClass("search-choice-focus")) : void 0) }, c.prototype.clear_backstroke = function () { return this.pending_backstroke && this.pending_backstroke.removeClass("search-choice-focus"), this.pending_backstroke = null }, c.prototype.keydown_checker = function (a) { var b, c; switch (b = null != (c = a.which) ? c : a.keyCode, this.search_field_scale(), 8 !== b && this.pending_backstroke && this.clear_backstroke(), b) { case 8: this.backstroke_length = this.search_field.val().length; break; case 9: this.results_showing && !this.is_multiple && this.result_select(a), this.mouse_on_container = !1; break; case 13: this.results_showing && a.preventDefault(); break; case 32: this.disable_search && a.preventDefault(); break; case 38: a.preventDefault(), this.keyup_arrow(); break; case 40: a.preventDefault(), this.keydown_arrow() } }, c.prototype.search_field_scale = function () { var b, c, d, e, f, g, h, i, j; if (this.is_multiple) { for (d = 0, h = 0, f = "position:absolute; left: -1000px; top: -1000px; display:none;", g = ["font-size", "font-style", "font-weight", "font-family", "line-height", "text-transform", "letter-spacing"], i = 0, j = g.length; i < j; i++)e = g[i], f += e + ":" + this.search_field.css(e) + ";"; return b = a("<div />", { style: f }), b.text(this.search_field.val()), a("body").append(b), h = b.width() + 25, b.remove(), c = this.container.outerWidth(), h > c - 10 && (h = c - 10), this.search_field.css({ width: h + "px" }) } }, c }(b) }).call(this);



/* ----------------- Start Document ----------------- */
(function ($) {
	"use strict";

	$(document).ready(function () {

		/*--------------------------------------------------*/
		/*  Mobile Menu - mmenu.js
		/*--------------------------------------------------*/
		$(function () {
			function mmenuInit() {
				var wi = $(window).width();
				if (wi <= '1024') {

					$(".mmenu-init").remove();
					$("#navigation").clone().addClass("mmenu-init").insertBefore("#navigation").removeAttr('id').removeClass('style-1 style-2')
						.find('ul, div').removeClass('style-1 style-2 mega-menu mega-menu-content mega-menu-section').removeAttr('id');
					$(".mmenu-init").find("ul").addClass("mm-listview");
					$(".mmenu-init").find(".mobile-styles .mm-listview").unwrap();


					$(".mmenu-init").mmenu({
						"counters": true
					}, {
						// configuration
						offCanvas: {
							pageNodetype: "#wrapper"
						}
					});

					var mmenuAPI = $(".mmenu-init").data("mmenu");
					debugger;
					var $icon = $(".hamburger");

					$(".mmenu-trigger").click(function () {
						mmenuAPI.open();
					});

					mmenuAPI.bind("open:finish", function () {
						setTimeout(function () {
							$icon.addClass("is-active");
						});
					});
					mmenuAPI.bind("close:finish", function () {
						setTimeout(function () {
							$icon.removeClass("is-active");
						});
					});


				}
				$(".mm-next").addClass("mm-fullsubopen");
			}
			mmenuInit();
			$(window).resize(function () { mmenuInit(); });
		});

		/*  User Menu */
		$('.user-menu').on('click', function () {
			$(this).toggleClass('active');
		});


		// Closes notification dropdown on click outside the conatainer
		var mouse_is_inside = false;

		$(".user-menu").on("mouseenter", function () {
			mouse_is_inside = true;
		});
		$(".user-menu").on("mouseleave", function () {
			mouse_is_inside = false;
		});

		$("body").mouseup(function () {
			if (!mouse_is_inside) $(".user-menu").removeClass('active');
		});



		/*----------------------------------------------------*/
		/*  Sticky Header
		/*----------------------------------------------------*/
		$("#header").not("#header.not-sticky").clone(true).addClass('cloned unsticky').insertAfter("#header");
		$('#header.cloned #sign-in-dialog').remove();
		$("#navigation.style-2").clone(true).addClass('cloned unsticky').insertAfter("#navigation.style-2");

		// Logo for header style 2
		$("#logo .sticky-logo").clone(true).prependTo("#navigation.style-2.cloned ul#responsive");


		// sticky header script
		var headerOffset = 140; // height on which the sticky header will shows

		$(window).scroll(function () {
			if ($(window).scrollTop() >= headerOffset) {
				$("#header.cloned").addClass('sticky').removeClass("unsticky");
				$("#navigation.style-2.cloned").addClass('sticky').removeClass("unsticky");
			} else {
				$("#header.cloned").addClass('unsticky').removeClass("sticky");
				$("#navigation.style-2.cloned").addClass('unsticky').removeClass("sticky");
			}
		});

		// Sticky Logo
		$(window).on('scroll load', function () {
			$("#header.cloned #logo img").attr("src", $('#header #logo img').attr('data-sticky-logo'));
		});

		/*----------------------------------------------------*/
		/*  Back to Top
		/*----------------------------------------------------*/
		var pxShow = 600; // height on which the button will show
		var scrollSpeed = 500; // how slow / fast you want the button to scroll to top.

		$(window).scroll(function () {
			if ($(window).scrollTop() >= pxShow) {
				$("#backtotop").addClass('visible');
			} else {
				$("#backtotop").removeClass('visible');
			}
		});

		$('#backtotop a').on('click', function () {
			$('html, body').animate({ scrollTop: 0 }, scrollSpeed);
			return false;
		});


		/*----------------------------------------------------*/
		/*  Inline CSS replacement for backgrounds etc.
		/*----------------------------------------------------*/
		function inlineCSS() {

			// Common Inline CSS
			$(".main-search-container, section.fullwidth, .listing-slider .item, .listing-slider-small .item, .address-container, .img-box-background, .image-edge, .edge-bg").each(function () {
				var attrImageBG = $(this).attr('data-background-image');
				var attrColorBG = $(this).attr('data-background-color');

				if (attrImageBG !== undefined) {
					$(this).css('background-image', 'url(' + attrImageBG + ')');
				}

				if (attrColorBG !== undefined) {
					$(this).css('background', '' + attrColorBG + '');
				}
			});

		}

		// Init
		inlineCSS();

		function parallaxBG() {

			$('.parallax').prepend('<div class="parallax-overlay"></div>');

			$(".parallax").each(function () {
				var attrImage = $(this).attr('data-background');
				var attrColor = $(this).attr('data-color');
				var attrOpacity = $(this).attr('data-color-opacity');

				if (attrImage !== undefined) {
					$(this).css('background-image', 'url(' + attrImage + ')');
				}

				if (attrColor !== undefined) {
					$(this).find(".parallax-overlay").css('background-color', '' + attrColor + '');
				}

				if (attrOpacity !== undefined) {
					$(this).find(".parallax-overlay").css('opacity', '' + attrOpacity + '');
				}

			});
		}

		parallaxBG();



		/*----------------------------------------------------*/
		/*  Image Box
		/*----------------------------------------------------*/
		$('.category-box').each(function () {

			// add a photo container
			$(this).append('<div class="category-box-background"></div>');

			// set up a background image for each tile based on data-background-image attribute
			$(this).children('.category-box-background').css({ 'background-image': 'url(' + $(this).attr('data-background-image') + ')' });

			// background animation on mousemove
			// $(this).on('mousemove', function(e){
			//   $(this).children('.category-box-background').css({'transform-origin': ((e.pageX - $(this).offset().left) / $(this).width()) * 100 + '% ' + ((e.pageY - $(this).offset().top) / $(this).height()) * 100 +'%'});
			// })
		});


		/*----------------------------------------------------*/
		/*  Image Box
		/*----------------------------------------------------*/
		$('.img-box').each(function () {
			$(this).append('<div class="img-box-background"></div>');
			$(this).children('.img-box-background').css({ 'background-image': 'url(' + $(this).attr('data-background-image') + ')' });
		});



		/*----------------------------------------------------*/
		/*  Parallax
		/*----------------------------------------------------*/

		/* detect touch */
		if ("ontouchstart" in window) {
			document.documentElement.className = document.documentElement.className + " touch";
		}
		if (!$("html").hasClass("touch")) {
			/* background fix */
			$(".parallax").css("background-attachment", "fixed");
		}

		/* fix vertical when not overflow
		call fullscreenFix() if .fullscreen content changes */
		function fullscreenFix() {
			var h = $('body').height();
			// set .fullscreen height
			$(".content-b").each(function (i) {
				if ($(this).innerHeight() > h) {
					$(this).closest(".fullscreen").addClass("overflow");
				}
			});
		}
		$(window).resize(fullscreenFix);
		fullscreenFix();

		/* resize background images */
		function backgroundResize() {
			var windowH = $(window).height();
			$(".parallax").each(function (i) {
				var path = $(this);
				// variables
				var contW = path.width();
				var contH = path.height();
				var imgW = path.attr("data-img-width");
				var imgH = path.attr("data-img-height");
				var ratio = imgW / imgH;
				// overflowing difference
				var diff = 100;
				diff = diff ? diff : 0;
				// remaining height to have fullscreen image only on parallax
				var remainingH = 0;
				if (path.hasClass("parallax") && !$("html").hasClass("touch")) {
					//var maxH = contH > windowH ? contH : windowH;
					remainingH = windowH - contH;
				}
				// set img values depending on cont
				imgH = contH + remainingH + diff;
				imgW = imgH * ratio;
				// fix when too large
				if (contW > imgW) {
					imgW = contW;
					imgH = imgW / ratio;
				}
				//
				path.data("resized-imgW", imgW);
				path.data("resized-imgH", imgH);
				path.css("background-size", imgW + "px " + imgH + "px");
			});
		}


		$(window).resize(backgroundResize);
		$(window).focus(backgroundResize);
		backgroundResize();

		/* set parallax background-position */
		function parallaxPosition(e) {
			var heightWindow = $(window).height();
			var topWindow = $(window).scrollTop();
			var bottomWindow = topWindow + heightWindow;
			var currentWindow = (topWindow + bottomWindow) / 2;
			$(".parallax").each(function (i) {
				var path = $(this);
				var height = path.height();
				var top = path.offset().top;
				var bottom = top + height;
				// only when in range
				if (bottomWindow > top && topWindow < bottom) {
					//var imgW = path.data("resized-imgW");
					var imgH = path.data("resized-imgH");
					// min when image touch top of window
					var min = 0;
					// max when image touch bottom of window
					var max = - imgH + heightWindow;
					// overflow changes parallax
					var overflowH = height < heightWindow ? imgH - height : imgH - heightWindow; // fix height on overflow
					top = top - overflowH;
					bottom = bottom + overflowH;


					// value with linear interpolation
					// var value = min + (max - min) * (currentWindow - top) / (bottom - top);
					var value = 0;
					if ($('.parallax').is(".titlebar")) {
						value = min + (max - min) * (currentWindow - top) / (bottom - top) * 2;
					} else {
						value = min + (max - min) * (currentWindow - top) / (bottom - top);
					}

					// set background-position
					var orizontalPosition = path.attr("data-oriz-pos");
					orizontalPosition = orizontalPosition ? orizontalPosition : "50%";
					$(this).css("background-position", orizontalPosition + " " + value + "px");
				}
			});
		}
		if (!$("html").hasClass("touch")) {
			$(window).resize(parallaxPosition);
			//$(window).focus(parallaxPosition);
			$(window).scroll(parallaxPosition);
			parallaxPosition();
		}

		// Jumping background fix for IE
		if (navigator.userAgent.match(/Trident\/7\./)) { // if IE
			$('body').on("mousewheel", function () {
				event.preventDefault();

				var wheelDelta = event.wheelDelta;
				var currentScrollPosition = window.pageYOffset;
				window.scrollTo(0, currentScrollPosition - wheelDelta);
			});
		}


		/*----------------------------------------------------*/
		/*  Chosen Plugin
		/*----------------------------------------------------*/

		// var config = {
		// 	'.chosen-select': { disable_search_threshold: 10, width: "100%" },
		// 	'.chosen-select-deselect': { allow_single_deselect: true, width: "100%" },
		// 	'.chosen-select-no-single': { disable_search_threshold: 100, width: "100%" },
		// 	'.chosen-select-no-single.no-search': { disable_search_threshold: 10, width: "100%" },
		// 	'.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
		// 	'.chosen-select-width': { width: "95%" }
		// };

		// for (var selector in config) {
		// 	if (config.hasOwnProperty(selector)) {
		// 		$(selector).chosen(config[selector]);
		// 	}
		// }


		/*----------------------------------------------------*/
		/*  Magnific Popup
		/*----------------------------------------------------*/

		$('.mfp-gallery-container').each(function () { // the containers for all your galleries

			$(this).magnificPopup({
				type: 'image',
				delegate: 'a.mfp-gallery',

				fixedContentPos: true,
				fixedBgPos: true,

				overflowY: 'auto',

				closeBtnInside: false,
				preloader: true,

				removalDelay: 0,
				mainClass: 'mfp-fade',

				gallery: { enabled: true, tCounter: '' }
			});
		});

		$('.popup-with-zoom-anim').magnificPopup({
			type: 'inline',

			fixedContentPos: false,
			fixedBgPos: true,

			overflowY: 'auto',

			closeBtnInside: true,
			preloader: false,

			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
		});

		$('.mfp-image').magnificPopup({
			type: 'image',
			closeOnContentClick: true,
			mainClass: 'mfp-fade',
			image: {
				verticalFit: true
			}
		});

		$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
			disableOn: 700,
			type: 'iframe',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,

			fixedContentPos: false
		});



		/*----------------------------------------------------*/
		/*  Slick Carousel
		/*----------------------------------------------------*/

		$('.fullwidth-slick-carousel').slick({
			centerMode: true,
			centerPadding: '20%',
			slidesToShow: 3,
			dots: true,
			arrows: false,
			responsive: [
				{
					breakpoint: 1920,
					settings: {
						centerPadding: '15%',
						slidesToShow: 3
					}
				},
				{
					breakpoint: 1441,
					settings: {
						centerPadding: '10%',
						slidesToShow: 3
					}
				},
				{
					breakpoint: 1025,
					settings: {
						centerPadding: '10px',
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 767,
					settings: {
						centerPadding: '10px',
						slidesToShow: 1
					}
				}
			]
		});
		// Fix for carousel if there are less than 4 categories
		$(window).on('load resize', function (e) {
			var carouselListItems = $(".fullwidth-slick-carousel .fw-carousel-item").length;
			if (carouselListItems < 4) {
				$('.fullwidth-slick-carousel .slick-slide').css({
					'pointer-events': 'all',
					'opacity': '1',
				});
			}
		});



		$('.testimonial-carousel').slick({
			centerMode: true,
			centerPadding: '34%',
			slidesToShow: 1,
			dots: true,
			arrows: false,
			responsive: [
				{
					breakpoint: 1025,
					settings: {
						centerPadding: '10px',
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 767,
					settings: {
						centerPadding: '10px',
						slidesToShow: 1
					}
				}
			]
		});


		$('.listing-slider').slick({
			centerMode: true,
			centerPadding: '20%',
			slidesToShow: 2,
			responsive: [
				{
					breakpoint: 1367,
					settings: {
						centerPadding: '15%'
					}
				},
				{
					breakpoint: 1025,
					settings: {
						centerPadding: '0'
					}
				},
				{
					breakpoint: 767,
					settings: {
						centerPadding: '0',
						slidesToShow: 1
					}
				}
			]
		});


		$('.listing-slider-small').slick({
			centerMode: true,
			centerPadding: '0',
			slidesToShow: 3,
			responsive: [
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 1
					}
				}
			]
		});
		// Mobile fix for small listing slider
		$(window).on('load resize', function (e) {
			var carouselListItems = $(".listing-slider-small .slick-track").children().length
			if (carouselListItems < 2) {
				$('.listing-slider-small .slick-track').css({
					transform: 'none'
				});
			}
		});


		$('.simple-slick-carousel').slick({
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 3,
			dots: true,
			arrows: true,
			responsive: [
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
					breakpoint: 769,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});


		$('.simple-fw-slick-carousel').slick({
			infinite: true,
			slidesToShow: 5,
			slidesToScroll: 1,
			dots: true,
			arrows: false,

			responsive: [
				{
					breakpoint: 1610,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 1365,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 1,
					}
				}
			]
		});


		$('.logo-slick-carousel').slick({
			infinite: true,
			slidesToShow: 5,
			slidesToScroll: 4,
			dots: true,
			arrows: true,
			responsive: [
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3
					}
				},
				{
					breakpoint: 769,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});


		/*----------------------------------------------------*/
		/*  Tabs
		/*----------------------------------------------------*/

		var $tabsNav = $('.tabs-nav'),
			$tabsNavLis = $tabsNav.children('li');

		$tabsNav.each(function () {
			var $this = $(this);

			$this.next().children('.tab-content').stop(true, true).hide()
				.first().show();

			$this.children('li').first().addClass('active').stop(true, true).show();
		});

		$tabsNavLis.on('click', function (e) {
			var $this = $(this);

			$this.siblings().removeClass('active').end()
				.addClass('active');

			$this.parent().next().children('.tab-content').stop(true, true).hide()
				.siblings($this.find('a').attr('href')).fadeIn();

			e.preventDefault();
		});
		var hash = window.location.hash;
		var anchor = $('.tabs-nav a[href="' + hash + '"]');
		if (anchor.length === 0) {
			$(".tabs-nav li:first").addClass("active").show(); //Activate first tab
			$(".tab-content:first").show(); //Show first tab content
		} else {
			anchor.parent('li').click();
		}


		/*----------------------------------------------------*/
		/*  Accordions
		/*----------------------------------------------------*/
		var $accor = $('.accordion');

		$accor.each(function () {
			$(this).toggleClass('ui-accordion ui-widget ui-helper-reset');
			$(this).find('h3').addClass('ui-accordion-header ui-helper-reset ui-state-default ui-accordion-icons ui-corner-all');
			$(this).find('div').addClass('ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom');
			$(this).find("div").hide();

		});

		var $trigger = $accor.find('h3');

		$trigger.on('click', function (e) {
			var location = $(this).parent();

			if ($(this).next().is(':hidden')) {
				var $triggerloc = $('h3', location);
				$triggerloc.removeClass('ui-accordion-header-active ui-state-active ui-corner-top').next().slideUp(300);
				$triggerloc.find('span').removeClass('ui-accordion-icon-active');
				$(this).find('span').addClass('ui-accordion-icon-active');
				$(this).addClass('ui-accordion-header-active ui-state-active ui-corner-top').next().slideDown(300);
			}
			e.preventDefault();
		});


		/*----------------------------------------------------*/
		/*	Toggle
		/*----------------------------------------------------*/

		$(".toggle-container").hide();

		$('.trigger, .trigger.opened').on('click', function (a) {
			$(this).toggleClass('active');
			a.preventDefault();
		});

		$(".trigger").on('click', function () {
			$(this).next(".toggle-container").slideToggle(300);
		});

		$(".trigger.opened").addClass("active").next(".toggle-container").show();


		/*----------------------------------------------------*/
		/*  Tooltips
		/*----------------------------------------------------*/

		$(".tooltip.top").tipTip({
			defaultPosition: "top"
		});

		$(".tooltip.bottom").tipTip({
			defaultPosition: "bottom"
		});

		$(".tooltip.left").tipTip({
			defaultPosition: "left"
		});

		$(".tooltip.right").tipTip({
			defaultPosition: "right"
		});



		/*----------------------------------------------------*/
		/*  Rating Overview Background Colors
		/*----------------------------------------------------*/
		function ratingOverview(ratingElem) {

			$(ratingElem).each(function () {
				var dataRating = $(this).attr('data-rating');

				// Rules
				if (dataRating >= 4.0) {
					$(this).addClass('high');
					$(this).find('.rating-bars-rating-inner').css({ width: (dataRating / 5) * 100 + "%", });
				} else if (dataRating >= 3.0) {
					$(this).addClass('mid');
					$(this).find('.rating-bars-rating-inner').css({ width: (dataRating / 5) * 80 + "%", });
				} else if (dataRating < 3.0) {
					$(this).addClass('low');
					$(this).find('.rating-bars-rating-inner').css({ width: (dataRating / 5) * 60 + "%", });
				}

			});
		} ratingOverview('.rating-bars-rating');

		$(window).on('resize', function () {
			ratingOverview('.rating-bars-rating');
		});


		/*----------------------------------------------------*/
		/*  Custom Upload Button
		/*----------------------------------------------------*/

		var uploadButton = {
			$button: $('.uploadButton-input'),
			$nameField: $('.uploadButton-file-name')
		};

		uploadButton.$button.on('change', function () {
			_populateFileField($(this));
		});

		function _populateFileField($button) {
			var selectedFile = [];
			for (var i = 0; i < $button.get(0).files.length; ++i) {
				selectedFile.push($button.get(0).files[i].name + '<br>');
			}
			uploadButton.$nameField.html(selectedFile);
		}


		/*----------------------------------------------------*/
		/*  Recaptcha Holder
		/*----------------------------------------------------*/
		$('.message-vendor').on('click', function () {
			$('.captcha-holder').addClass('visible')
		});


		/*----------------------------------------------------*/
		/*  Like Icon Trigger
		/*----------------------------------------------------*/
		$('.like-icon, .widget-button, .like-button').on('click', function (e) {
			e.preventDefault();
			$(this).toggleClass('liked');
			$(this).children('.like-icon').toggleClass('liked');
		});

		/*----------------------------------------------------*/
		/*  Searh Form More Options
		/*----------------------------------------------------*/
		$('.more-search-options-trigger').on('click', function (e) {
			e.preventDefault();
			$('.more-search-options, .more-search-options-trigger').toggleClass('active');
			$('.more-search-options.relative').animate({ height: 'toggle', opacity: 'toggle' }, 300);
		});


		/*----------------------------------------------------*/
		/*  Half Screen Map Adjustments
		/*----------------------------------------------------*/
		$(window).on('load resize', function () {
			var winWidth = $(window).width();
			var headerHeight = $("#header-container").height(); // height on which the sticky header will shows

			$('.fs-inner-container, .fs-inner-container.map-fixed, #dashboard').css('padding-top', headerHeight);

			if (winWidth < 992) {
				$('.fs-inner-container.map-fixed').insertBefore('.fs-inner-container.content');
			} else {
				$('.fs-inner-container.content').insertBefore('.fs-inner-container.map-fixed');
			}

		});


		/*----------------------------------------------------*/
		/*  Counters
		/*----------------------------------------------------*/
		$(window).on('load', function () {
			$('.dashboard-stat-content h4').counterUp({
				delay: 100,
				time: 800
			});
		});


		/*----------------------------------------------------*/
		/*  Rating Script Init
		/*----------------------------------------------------*/

		// Leave Rating
		$('.leave-rating input').change(function () {
			var $radio = $(this);
			$('.leave-rating .selected').removeClass('selected');
			$radio.closest('label').addClass('selected');
		});


		/*----------------------------------------------------*/
		/* Dashboard Scripts
		/*----------------------------------------------------*/
		$('.dashboard-nav ul li a').on('click', function () {
			if ($(this).closest('li').has('ul').length) {
				$(this).parent('li').toggleClass('active');
			}
		});

		// Dashbaord Nav Scrolling
		$(window).on('load resize', function () {
			var wrapperHeight = window.innerHeight;
			var headerHeight = $("#header-container").height();
			var winWidth = $(window).width();

			if (winWidth > 992) {
				$(".dashboard-nav-inner").css('max-height', wrapperHeight - headerHeight);
			} else {
				$(".dashboard-nav-inner").css('max-height', '');
			}
		});


		// Tooltip
		$(".tip").each(function () {
			var tipContent = $(this).attr('data-tip-content');
			$(this).append('<div class="tip-content">' + tipContent + '</div>');
		});

		$(".verified-badge.with-tip").each(function () {
			var tipContent = $(this).attr('data-tip-content');
			$(this).append('<div class="tip-content">' + tipContent + '</div>');
		});

		$(window).on('load resize', function () {
			var verifiedBadge = $('.verified-badge.with-tip');
			verifiedBadge.find('.tip-content').css({
				'width': verifiedBadge.outerWidth(),
				'max-width': verifiedBadge.outerWidth(),
			});
		});


		// Switcher
		$(".add-listing-section").each(function () {

			var switcherSection = $(this);
			var switcherInput = $(this).find('.switch input');

			if (switcherInput.is(':checked')) {
				$(switcherSection).addClass('switcher-on');
			}

			switcherInput.change(function () {
				if (this.checked === true) {
					$(switcherSection).addClass('switcher-on');
				} else {
					$(switcherSection).removeClass('switcher-on');
				}
			});

		});


		// Responsive Nav Trigger
		$('.dashboard-responsive-nav-trigger').on('click', function (e) {
			e.preventDefault();
			$(this).toggleClass('active');

			var dashboardNavContainer = $('body').find(".dashboard-nav");

			if ($(this).hasClass('active')) {
				$(dashboardNavContainer).addClass('active');
			} else {
				$(dashboardNavContainer).removeClass('active');
			}

		});

		// Dashbaord Messages Alignment
		$(window).on('load resize', function () {
			var msgContentHeight = $(".message-content").outerHeight();
			var msgInboxHeight = $(".messages-inbox ul").height();

			if (msgContentHeight > msgInboxHeight) {
				$(".messages-container-inner .messages-inbox ul").css('max-height', msgContentHeight)
			}
		});


		/*----------------------------------------------------*/
		/* Time Slots
		/*----------------------------------------------------*/

		// Add validation parts
		$('.day-slots').each(function () {

			var daySlots = $(this);

			daySlots.find('.add-slot-btn').on('click', function () {

				var slotTime_Start = daySlots.find('.add-slot-inputs input.time-slot-start').val();
				var slotTimePM_AM_Start = daySlots.find('.add-slot-inputs select.time-slot-start').val();

				var slotTime_End = daySlots.find('.add-slot-inputs input.time-slot-end').val();
				var slotTimePM_AM_End = daySlots.find('.add-slot-inputs select.time-slot-end').val();

				// Checks if input values are not blank
				if (slotTime_Start.length > 0 && slotTime_End.length > 0) {

					// New Time Slot Div
					var newTimeSlot = daySlots
						.find('.single-slot.cloned')
						.clone(true)
						.addClass('slot-animation')
						.removeClass('cloned');

					setTimeout(function () {
						newTimeSlot.removeClass('slot-animation');
					}, 300);

					newTimeSlot.find('.plusminus input').val('1');

					// Plus - Minus Init
					newTimeSlot.find('.plusminus').numberPicker();

					// Check if there's am/pm dropdown
					var $twelve_hr = $('.add-slot-inputs select.twelve-hr');

					if ($twelve_hr.length) {
						newTimeSlot.find('.single-slot-time').html(slotTime_Start + ' ' + '<i class="am-pm">' + slotTimePM_AM_Start + '</i>' + ' - ' + slotTime_End + ' ' + '<i class="am-pm">' + slotTimePM_AM_End + '</i>');
					} else {
						newTimeSlot.find('.single-slot-time').html('' + slotTime_Start + ' - ' + slotTime_End);
					}

					// Appending new slot
					newTimeSlot.appendTo(daySlots.find('.slots-container'));

					// Refresh sotrable script
					$(".slots-container").sortable('refresh');
				}

				// Validation Error
				else {
					daySlots.find('.add-slot').addClass('add-slot-shake-error');
					setTimeout(function () {
						daySlots.find('.add-slot').removeClass('add-slot-shake-error');
					}, 600);
				}
			});

			// Removing "no slots" message
			function hideSlotInfo() {
				var slotCount = daySlots.find(".slots-container").children().length;
				if (slotCount < 1) {
					daySlots.find(".no-slots")
						.addClass("no-slots-fadein")
						.removeClass("no-slots-fadeout");
				}
			}
			hideSlotInfo();


			// Removing Slot
			daySlots.find('.remove-slot').bind('click', function () {
				$(this).closest('.single-slot').animate({ height: 0, opacity: 0 }, 'fast', function () {
					$(this).remove();
				});

				// Removing "no slots" message
				setTimeout(function () {
					hideSlotInfo()
				}, 400);

			});

			// Showing "no slots" message
			daySlots.find('.add-slot-btn').on('click', function () {
				var slotCount = daySlots.find(".slots-container").children().length;
				if (slotCount >= 1) {
					daySlots.find(".no-slots")
						.removeClass("no-slots-fadein")
						.addClass("no-slots-fadeout");
				}
			});

		});

		// Sotrable Script
		$(".slots-container").sortable();

		// 24 hour clock type switcher
		if ($('.availability-slots').attr('data-clock-type') == '24hr') {
			$('.availability-slots').addClass('twenty-four-clock');
			$('.availability-slots').find('input[type="time"]').attr({ "max": "24:00" });
		}

		// Number Picker - TobyJ
		(function ($) {
			$.fn.numberPicker = function () {
				var dis = 'disabled';
				return this.each(function () {
					var picker = $(this),
						p = picker.find('button:last-child'),
						m = picker.find('button:first-child'),
						input = picker.find('input'),
						min = parseInt(input.attr('min'), 10),
						max = parseInt(input.attr('max'), 10),
						inputFunc = function (picker) {
							var i = parseInt(input.val(), 10);
							if ((i <= min) || (!i)) {
								input.val(min);
								p.prop(dis, false);
								m.prop(dis, true);
							} else if (i >= max) {
								input.val(max);
								p.prop(dis, true);
								m.prop(dis, false);
							} else {
								p.prop(dis, false);
								m.prop(dis, false);
							}
						},
						changeFunc = function (picker, qty) {
							var q = parseInt(qty, 10),
								i = parseInt(input.val(), 10);
							if ((i < max && (q > 0)) || (i > min && !(q > 0))) {
								input.val(i + q);
								inputFunc(picker);
							}
						};
					m.on('click', function () { changeFunc(picker, -1); });
					p.on('click', function () { changeFunc(picker, 1); });
					input.on('change', function () { inputFunc(picker); });
					inputFunc(picker); //init
				});
			};
		}(jQuery));

		// Init
		$('.plusminus').numberPicker();



		/*----------------------------------------------------*/
		/* Pricing List
		/*----------------------------------------------------*/
		function newMenuItem() {
			var newElem = $('tr.pricing-list-item.pattern').first().clone();
			newElem.find('input').val('');
			newElem.appendTo('table#pricing-list-container');
		}

		if ($("table#pricing-list-container").is('*')) {
			$('.add-pricing-list-item').on('click', function (e) {
				e.preventDefault();
				newMenuItem();
			});

			// remove ingredient
			$(document).on("click", "#pricing-list-container .delete", function (e) {
				e.preventDefault();
				$(this).parent().parent().remove();
			});

			// add submenu
			$('.add-pricing-submenu').on('click', function (e) {
				e.preventDefault();

				var newElem = $('' +
					'<tr class="pricing-list-item pricing-submenu">' +
					'<td>' +
					'<div class="fm-move"><i class="sl sl-icon-cursor-move"></i></div>' +
					'<div class="fm-input"><input type="text" placeholder="Category Title" /></div>' +
					'<div class="fm-close"><a class="delete" href="#"><i class="fa fa-remove"></i></a></div>' +
					'</td>' +
					'</tr>');

				newElem.appendTo('table#pricing-list-container');
			});

			$('table#pricing-list-container tbody').sortable({
				forcePlaceholderSize: true,
				forceHelperSize: false,
				placeholder: 'sortableHelper',
				zIndex: 999990,
				opacity: 0.6,
				tolerance: "pointer",
				start: function (e, ui) {
					ui.placeholder.height(ui.helper.outerHeight());
				}
			});
		}


		// Unit character
		var fieldUnit = $('.pricing-price').children('input').attr('data-unit');
		$('.pricing-price').children('input').before('<i class="data-unit">' + fieldUnit + '</i>');



		/*----------------------------------------------------*/
		/*  Notifications
		/*----------------------------------------------------*/
		$("a.close").removeAttr("href").on('click', function () {

			function slideFade(elem) {
				var fadeOut = { opacity: 0, transition: 'opacity 0.5s' };
				elem.css(fadeOut).slideUp();
			}
			slideFade($(this).parent());

		});


		/*----------------------------------------------------*/
		/* Panel Dropdown
		/*----------------------------------------------------*/
		function close_panel_dropdown() {
			$('.panel-dropdown').removeClass("active");
			$('.fs-inner-container.content').removeClass("faded-out");
		}

		$('.panel-dropdown a').on('click', function (e) {

			if ($(this).parent().is(".active")) {
				close_panel_dropdown();
			} else {
				close_panel_dropdown();
				$(this).parent().addClass('active');
				$('.fs-inner-container.content').addClass("faded-out");
			}

			e.preventDefault();
		});

		// Apply / Close buttons
		$('.panel-buttons button').on('click', function (e) {
			$('.panel-dropdown').removeClass('active');
			$('.fs-inner-container.content').removeClass("faded-out");
		});

		// Closes dropdown on click outside the conatainer
		var mouse_is_inside = false;

		$('.panel-dropdown').hover(function () {
			mouse_is_inside = true;
		}, function () {
			mouse_is_inside = false;
		});

		$("body").mouseup(function () {
			if (!mouse_is_inside) close_panel_dropdown();
		});

		// "All" checkbox
		$('.checkboxes.categories input').on('change', function () {
			if ($(this).hasClass('all')) {
				$(this).parents('.checkboxes').find('input').prop('checked', false);
				$(this).prop('checked', true);
			} else {
				$('.checkboxes input.all').prop('checked', false);
			}
		});


		$('input[type="range"].distance-radius').rangeslider({
			polyfill: false,
			onInit: function () {
				this.output = $('<div class="range-output" />').insertBefore(this.$range).html(this.$element.val());

				var radiustext = $('.distance-radius').attr('data-title');
				$('.range-output').after('<i class="data-radius-title">' + radiustext + '</i>');

			},
			onSlide: function (position, value) {
				this.output.html(value);
			}
		});


		/*----------------------------------------------------*/
		/*  Show More Button
		/*----------------------------------------------------*/
		$('.show-more-button').on('click', function (e) {
			e.preventDefault();
			$(this).toggleClass('active');

			$('.show-more').toggleClass('visible');
			if ($('.show-more').is(".visible")) {

				var el = $('.show-more'),
					curHeight = el.height(),
					autoHeight = el.css('height', 'auto').height();
				el.height(curHeight).animate({ height: autoHeight }, 400);


			} else { $('.show-more').animate({ height: '450px' }, 400); }

		});


		/*----------------------------------------------------*/
		/* Listing Page Nav
		/*----------------------------------------------------*/

		$(window).on('load resize scroll', function () {
			var containerWidth = $(".container").width();
			$('.listing-nav-container.cloned .listing-nav').css('width', containerWidth);
		});

		if (document.getElementById("listing-nav") !== null) {
			$(window).scroll(function () {
				var window_top = $(window).scrollTop();
				var div_top = $('.listing-nav').not('.listing-nav-container.cloned .listing-nav').offset().top + 90;
				if (window_top > div_top) {
					$('.listing-nav-container.cloned').addClass('stick');
				} else {
					$('.listing-nav-container.cloned').removeClass('stick');
				}
			});
		}

		$(".listing-nav-container").clone(true).addClass('cloned').prependTo("body");


		// Smooth scrolling using scrollto.js
		$('.listing-nav a, a.listing-address, .star-rating a').on('click', function (e) {
			e.preventDefault();
			$('html,body').scrollTo(this.hash, this.hash, { gap: { y: -20 } });
		});

		$(".listing-nav li:first-child a, a.add-review-btn, a[href='#add-review']").on('click', function (e) {
			e.preventDefault();
			$('html,body').scrollTo(this.hash, this.hash, { gap: { y: -100 } });
		});


		// Highlighting functionality.
		$(window).on('load resize', function () {
			var aChildren = $(".listing-nav li").children();
			var aArray = [];
			for (var i = 0; i < aChildren.length; i++) {
				var aChild = aChildren[i];
				var ahref = $(aChild).attr('href');
				aArray.push(ahref);
			}

			$(window).scroll(function () {
				var windowPos = $(window).scrollTop();
				for (var i = 0; i < aArray.length; i++) {
					var theID = aArray[i];
					var divPos = $(theID).offset().top - 150;
					var divHeight = $(theID).height();
					if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
						$("a[href='" + theID + "']").addClass("active");
					} else {
						$("a[href='" + theID + "']").removeClass("active");
					}
				}
			});
		});


		/*----------------------------------------------------*/
		/*  Payment Accordion
		/*----------------------------------------------------*/
		var radios = document.querySelectorAll('.payment-tab-trigger > input');

		for (var i = 0; i < radios.length; i++) {
			radios[i].addEventListener('change', expandAccordion);
		}

		function expandAccordion(event) {
			var allTabs = document.querySelectorAll('.payment-tab');
			for (var i = 0; i < allTabs.length; i++) {
				allTabs[i].classList.remove('payment-tab-active');
			}
			event.target.parentNode.parentNode.classList.add('payment-tab-active');
		}


		/*----------------------------------------------------*/
		/*  Booking Sticky Footer
		/*----------------------------------------------------*/
		$('.booking-sticky-footer a.button').on('click', function (e) {
			var $anchor = $(this);
			$("html, body").animate({ scrollTop: $($anchor.attr('href')).offset().top - 100 }, 1000);
		});


		/*----------------------------------------------------*/
		/*  Contact Form
		/*----------------------------------------------------*/

		var shake = "No";

		$('#message').hide();

		// Add validation parts
		$('#contact input[type=text], #contact input[type=number], #contact input[type=email], #contact input[type=url], #contact input[type=tel], #contact select, #contact textarea').each(function () {

		});

		// Validate as you type
		$('#name, #comments, #subject').focusout(function () {
			if (!$(this).val()) {
				$(this).addClass('error').parent().find('mark').removeClass('valid').addClass('error');
			}
			else {
				$(this).removeClass('error').parent().find('mark').removeClass('error').addClass('valid');
			}
			$('#submit')
				.prop('disabled', false)
				.removeClass('disabled');
		});
		$('#email').focusout(function () {
			if (!$(this).val() || !isEmail($(this).val())) {
				$(this).addClass('error').parent().find('mark').removeClass('valid').addClass('error');
			} else {
				$(this).removeClass('error').parent().find('mark').removeClass('error').addClass('valid');
			}
		});

		$('#email').focusin(function () {
			$('#submit')
				.prop('disabled', false)
				.removeClass('disabled');
		});

		$('#submit').on('click', function () {
			$("#contact-message").slideUp(200, function () {
				$('#contact-message').hide();

				// Kick in Validation
				$('#name, #subject, #phone, #comments, #website, #email').triggerHandler("focusout");

				if ($('#contact mark.error').size() > 0) {
					if (shake == "Yes") {
						$('#contact').effect('shake', { times: 2 }, 75, function () {
							$('#contact input.error:first, #contact textarea.error:first').focus();
						});
					} else $('#contact input.error:first, #contact textarea.error:first').focus();

					return false;
				}

			});
		});

		$('#contactform').submit(function () {

			if ($('#contact mark.error').size() > 0) {
				if (shake == "Yes") {
					$('#contact').effect('shake', { times: 2 }, 75);
				}
				return false;
			}

			var action = $(this).attr('action');

			$('#contact #submit').after('<img src="images/loader.gif" class="loader" />');

			$('#submit')
				.prop('disabled', true)
				.addClass('disabled');

			$.post(action, $('#contactform').serialize(),
				function (data) {
					$('#contact-message').html(data);
					$('#contact-message').slideDown();
					$('#contactform img.loader').fadeOut('slow', function () { $(this).remove(); });
					// $('#contactform #submit').removeAttr('disabled');
					if (data.match('success') !== null) $('#contactform').slideUp('slow');

				}
			);

			return false;

		});

		function isEmail(emailAddress) {

			var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

			return pattern.test(emailAddress);
		}

		// ------------------ End Document ------------------ //
	});

})(this.jQuery);


/*!
 * jquery.scrollto.js 0.0.1 - https://github.com/yckart/jquery.scrollto.js
 * Copyright (c) 2012 Yannick Albert (http://yckart.com)
 * Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php).
 **/

$.scrollTo = $.fn.scrollTo = function (x, y, options) {
	if (!(this instanceof $)) return $.fn.scrollTo.apply($('html, body'), arguments);

	options = $.extend({}, {
		gap: {
			x: 0,
			y: 0
		},
		animation: {
			easing: 'swing',
			duration: 600,
			complete: $.noop,
			step: $.noop
		}
	}, options);

	return this.each(function () {
		var elem = $(this);
		elem.stop().animate({
			scrollLeft: !isNaN(Number(x)) ? x : $(y).offset().left + options.gap.x,
			scrollTop: !isNaN(Number(y)) ? y : $(y).offset().top + options.gap.y
		}, options.animation);
	});
};


/*----------------------------------------------------*/
/*  Ratings Script
/*----------------------------------------------------*/

/*  Numerical Script
/*--------------------------*/
function numericalRating(ratingElem) {

	$(ratingElem).each(function () {
		var dataRating = $(this).attr('data-rating');

		// Rules
		if (dataRating >= 4.0) {
			$(this).addClass('high');
		} else if (dataRating >= 3.0) {
			$(this).addClass('mid');
		} else if (dataRating < 3.0) {
			$(this).addClass('low');
		}

	});

} numericalRating('.numerical-rating');


/*  Star Rating
/*--------------------------*/
function starRating(ratingElem) {

	$(ratingElem).each(function () {

		var dataRating = $(this).attr('data-rating');

		// Rating Stars Output
		function starsOutput(firstStar, secondStar, thirdStar, fourthStar, fifthStar) {
			return ('' +
				'<span class="' + firstStar + '"></span>' +
				'<span class="' + secondStar + '"></span>' +
				'<span class="' + thirdStar + '"></span>' +
				'<span class="' + fourthStar + '"></span>' +
				'<span class="' + fifthStar + '"></span>');
		}

		var fiveStars = starsOutput('star', 'star', 'star', 'star', 'star');

		var fourHalfStars = starsOutput('star', 'star', 'star', 'star', 'star half');
		var fourStars = starsOutput('star', 'star', 'star', 'star', 'star empty');

		var threeHalfStars = starsOutput('star', 'star', 'star', 'star half', 'star empty');
		var threeStars = starsOutput('star', 'star', 'star', 'star empty', 'star empty');

		var twoHalfStars = starsOutput('star', 'star', 'star half', 'star empty', 'star empty');
		var twoStars = starsOutput('star', 'star', 'star empty', 'star empty', 'star empty');

		var oneHalfStar = starsOutput('star', 'star half', 'star empty', 'star empty', 'star empty');
		var oneStar = starsOutput('star', 'star empty', 'star empty', 'star empty', 'star empty');

		// Rules
		if (dataRating >= 4.75) {
			$(this).append(fiveStars);
		} else if (dataRating >= 4.25) {
			$(this).append(fourHalfStars);
		} else if (dataRating >= 3.75) {
			$(this).append(fourStars);
		} else if (dataRating >= 3.25) {
			$(this).append(threeHalfStars);
		} else if (dataRating >= 2.75) {
			$(this).append(threeStars);
		} else if (dataRating >= 2.25) {
			$(this).append(twoHalfStars);
		} else if (dataRating >= 1.75) {
			$(this).append(twoStars);
		} else if (dataRating >= 1.25) {
			$(this).append(oneHalfStar);
		} else if (dataRating < 1.25) {
			$(this).append(oneStar);
		}

	});

} starRating('.star-rating');
