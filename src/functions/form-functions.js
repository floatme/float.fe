

export const destroyDateTimePicker = (index, allPickers) => {
    return allPickers.filter(function (el, i) {
        return index !== i;
    });
}

export const extractParametersFromSearch = (search) => {
    return search.split('=')[1];
}

export const extractSecondParametersFromSearch = (key, segment) => {
    const output = window.location.href.split(key)[1];
    console.log("OUTPUT", output)
    return output;
}

export const extractParamFromLocation = (location, param) => {
    var vars = {};
    location.replace(
        /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
        function (m, key, value) { // callback
            vars[key] = value !== undefined ? value : '';
        }
    );

    if (param) {
        return vars[param] ? vars[param] : null;
    }
    return vars;
}

