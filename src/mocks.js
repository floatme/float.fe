import { BookingStatusEnum } from "./components/admin/dashboard/enums/BookingStatusEnum"

import { ListingStatusEnum } from "./components/admin/dashboard/enums/ListingStatusEnum"

export const detailMock = {
  timestamp: "1567440961",
  id: "01",
  ownerId: "U1",
  pricePerDay: 78.0,
  pricePerWeek: 50.0,
  title: "Mountain Bike",
  mainImage: "https://i.ebayimg.com/images/g/MkgAAOSwhABdg0eK/s-l800.png",
  description: "Trek Mountain Bicycle, 4300 model, Good condition",
  images: [
    {
      url: "https://i.ebayimg.com/images/g/MkgAAOSwhABdg0eK/s-l800.png",
      title: "image title",
    },
    {
      url: "https://i.ebayimg.com/images/g/CUUAAOSw415dg0eK/s-l800.png",
      title: "image title",
    },
  ],
  location: "Newlands, CapeTown",
  gps: { latitude: "-33.934060", longitude: "18.420325" },
  collection: "false",
  delivery: "true",
  verified: "true",
  dateCreated: "05/29/2015 05:50",
  subCategory: "Sports Equipment",
  category: "Sports/Leisure",
  ratings: {
    rating: 4.2,
    valueForMoney: 4.2,
    owner: 4.6,
    quality: 5,
    service: 4.5,
  },
  bookmarkCount: "15",
  bookingFriendlyDates: [
    {
      startDate: "Fri, 28 August",
      days: "5 days",
    },
    {
      startDate: "Tue, 29 September",
      days: "1 day",
    },
  ],
  owner: {
    name: "Tom Perrin",
    phone: "0788554444",
    email: "tom@example.com",
    gravatar:
      "http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=70",
    fb: "",
    tw: "",
  },
  reviews: [
    {
      userName: "Kathy Brown",
      gravatar:
        "http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=70",
      dateCreated: "29 Jun 2019",
      reviewText:
        "Morbi velit eros, sagittis in facilisis non, rhoncus et erat. Nam posuere tristique sem, eu ultricies tortor imperdiet vitae. Curabitur lacinia neque non metus",
      images: [""],
      reviewUpCount: 2,
      rating: 5,
    },
  ],
  socialMedia: {
    fbHandle: "fb.me.com",
    twHandle: "yt.me.com",
    ytHandle: "",
    inHandle: "in.me.com",
  },
  phoneNumber: "0214444444",
  email: "bike@me.com",
}

export const defaultDetailMock = {
  timestamp: "",
  id: "",
  ownerId: "",
  pricePerDay: "0",
  pricePerWeek: "0",
  title: "",
  mainImage: "",
  description: "",
  images: [],
  location: "",
  gps: { latitude: "", longitude: "" },
  collection: "",
  delivery: "false",
  verified: "false",
  dateCreated: "",
  subCategory: "",
  bookingFriendlyDates: [],
  category: "",
  ratings: {
    rating: "",
    valueForMoney: "",
    owner: "",
    quality: "",
    service: "",
  },
  bookmarkCount: "",
  bookings: [],
  owner: {
    name: "",
    phone: "",
    email: "",
    gravatar:
      "",
    fb: "",
    tw: "",
  },
  reviews: [],
  socialMedia: {
    fbHandle: "",
    twHandle: "",
    ytHandle: "",
    inHandle: "",
  },
  phoneNumber: "",
  email: "",
}

export const mockListings = {
  timestamp: "1567440961",
  listings: [
    {
      id: "01",
      ownerId: "U1",
      pricePerDay: 78.0,
      title: "Mountain Bike",
      mainImage: "https://i.ebayimg.com/images/g/MkgAAOSwhABdg0eK/s-l800.png",
      description: "Trek Mountain Bicycle, 4300 model, Good condition",
      images: [
        "https://i.ebayimg.com/images/g/MkgAAOSwhABdg0eK/s-l800.png",
        "https://i.ebayimg.com/images/g/CUUAAOSw415dg0eK/s-l800.png",
      ],
      location: "Newlands, CapeTown",
      collection: "0",
      delivery: "1",
      dateCreated: "05/29/2015 05:50",
      subCategory: "Sports Equipment",
      category: "Sports/Leisure",
      rating: "3.1",
      reviewCount: "7",
      promoText: "Recomended!",
    },
    {
      id: "02",
      ownerId: "U2",
      pricePerDay: 238.0,
      title: "Beuler Chainsaw",
      description: "Heavy duty chainsaw.",
      images: ["https://i.ebayimg.com/images/g/a-8AAOSwYv9dhgSr/s-l800.png"],
      mainImage: "https://i.ebayimg.com/images/g/a-8AAOSwYv9dhgSr/s-l800.png",
      location: "Observatory, Cape Town",
      collection: "1",
      delivery: "1",
      dateCreated: "05/08/2015 14:50",
      subCategory: "Garden Tools",
      category: "Garden/Tools",
      rating: "0",
      reviewCount: "0",
      promoText: "New!",
    },
    {
      id: "03",
      ownerId: "U3",
      pricePerDay: 100.0,
      title: "Kennilworth, Cape Town",
      description:
        "Karcher K2.21 Pressure washer High pressure water system for cleaning cars, boats, caravans, very powerful!",
      images: [
        "https://i.ebayimg.com/images/g/PcUAAOSwjZFdgNEo/s-l800.png",
        "https://i.ebayimg.com/images/g/tYgAAOSwn2ldgNEo/s-l800.png",
      ],
      mainImage: "https://i.ebayimg.com/images/g/PcUAAOSwjZFdgNEo/s-l800.png",
      location: "Salt River, Cape Town",
      collection: "0",
      delivery: "1",
      "date-created": "03/20/2015 15:10",
      subCategory: "Outdoor Equipment",
      category: "Garden/Tools",
      rating: "5",
      reviewCount: "1",
      promoText: "",
    },
  ],
}

export const mockBookings = [
  {
    status: BookingStatusEnum.APPROVED,
    bookingId: "01",
    profilePicUrl: "",
    title: "Mock title",
    timePeriod: "20/11/2019 - 24/11/2018",
    price: "R58.00",
    username: "Mock Username",
    email: "m@m.com",
    phone: "021 888 4444",
    paid: true,
    lastUpdated: "18/08/2019",
    requestDate: "15/08/2019"
  },
  {
    status: BookingStatusEnum.REJECTED,
    bookingId: "02",
    profilePicUrl: "",
    title: "Mock title 2",
    timePeriod: "20/08/2019 - 24/09/2018",
    price: "R58.00",
    username: "Mock Username 2",
    email: "j@j.com",
    phone: "021 888 4444",
    paid: false,
    lastUpdated: "18/08/2019",
    requestDate: "15/08/2019"

  },
  {
    status: BookingStatusEnum.PENDING,
    bookingId: "04",
    profilePicUrl: "",
    title: "Mock title 3",
    timePeriod: "18/10/2019 - 29/10/2018",
    price: "R98.00",
    username: "Mock Username 3",
    email: "k@k.com",
    phone: "021 888 4444",
    paid: false,
    lastUpdated: "17/10/2019",
    requestDate: "17/10/2019"
  },
  {
    status: BookingStatusEnum.CANCELLED,
    bookingId: "05",
    profilePicUrl: "",
    title: "Mock title 4",
    timePeriod: "20/07/2019 - 24/07/2018",
    price: "R58.00",
    username: "Mock Username 4",
    email: "j@j.com",
    phone: "021 888 4444",
    paid: false,
    lastUpdated: "18/07/2019",
    requestDate: "18/07/2019"
  }

]

export const mockProfileSummary = {
  stats: [
    {
      title: "Active Listings",
      number: "2",
      iconClass: "im-icon-Map2",
      color: "1"
    },
    {
      title: "Total Views",
      number: "25",
      iconClass: "im-icon-Line-Chart",
      color: "2"
    },
    {
      title: "Total Reviews",
      number: "2",
      iconClass: "im-icon-Add-UserStar",
      color: "3"
    },
    {
      title: "Times Bookmarked",
      number: "1",
      iconClass: "im-icon-Heart",
      color: "4"
    },

  ],
  activities: [
    {
      subject: "Kathy Brown",
      listing: "Trek Mountain Bike",
      activity: "left a review",
      iconClass: "sl-icon-star",
      reviewRating: "5",
      listingUrl: "./detail"
    },
    {
      subject: "Your listing: ",
      listing: "Trek Mountain Bike",
      activity: "has been approved!",
      iconClass: "sl-icon-star",
      reviewRating: "",
      link: "./detail"
    },
    {
      subject: "Your listing: ",
      listing: "Beuler Chainsaw",
      activity: "has been approved!",
      iconClass: " sl-icon-layers",
      reviewRating: "",
      link: "./detail"
    },
    {
      subject: "Someone bookmarked your ",
      listing: "Beuler Chainsaw",
      activity: "listing!",
      iconClass: "sl-icon-layers",
      reviewRating: "",
      link: "./detail"
    }
  ],

  invoices: [
    {

      title: "Trek Mountain Bike: (20/07/2019) 3 Days",
      paid: false,
      orderNo: "0021",
      date: "20/07/2019",
      status: BookingStatusEnum.COMPLETE,
      invoiceLink: "#"
    },
    {

      title: "Trek Mountain Bike: (01/07/2019) 1 Days",
      paid: true,
      orderNo: "0018",
      date: "20/07/2019",
      status: BookingStatusEnum.COMPLETE,
      invoiceLink: "#"
    }

  ],
  notifications: [
    {
      subject: "Kathy Brown",
      listing: "Trek Mountain Bike",
      activity: "left a review on "
    }
  ]
}

export const mockUserListings = [
  {
    ID: "014",
    status: ListingStatusEnum.ENABLED,
    listingUrl: "#",
    title: "Trek Mountain Bike",
    description: "Mountain Bike, very sturdy. 2016 model ...",
    rating: "4.5",
    reviewCount: "2",
    mainImageUrl: "https://i.ebayimg.com/images/g/MkgAAOSwhABdg0eK/s-l800.png"
  },
  {
    ID: "022",
    status: ListingStatusEnum.ENABLED,
    listingUrl: "#",
    title: "Beuler Chain saw",
    description: "Beuler chainsaw, powerful motor...",
    rating: "0",
    reviewCount: "0",
    mainImageUrl: "https://i.ebayimg.com/images/g/a-8AAOSwYv9dhgSr/s-l800.png",
  }
];

export const mockAddPayload = {
  title: "",
  category: "",
  keywords: "",
  city: "",
  address: "",
  zipCode: "",
  description: "",
  phone: "",
  email: "",
  fb: "",
  tw: "",
  delivery: false,
  collection: false,
  price: "",
  files: [],
  filePreviews: [],
  //additional components
  bookings: [],
  bookmarkCount: "0",
  dateCreated: "",
  gps: "",
  images: [],
  mainImage: [],
  owner: {},
  ownerId: "",
  pricePerWeek: "",
  ratings: {},
  reviews: [],
  socialMedia: {},
  subCategory: "",
  timestamp: "",
  verified: true
}

export const mockUser = {
  user: {
    isLoggedIn: false,
    username: "",
    userId: ""
  }
}


export const _allCategories = [
  {
    id: "5dcc36d24157d00d783eb8b1",
    name: "Sports/Leisure"
  },
  {
    id: "5db07592200b356ee4d7831a",
    name: "Text Books"
  },
  {
    id: "5de3b8f19a8363641086134d",
    name: "Events"
  },
  {
    id: "5de3b8979a8363641086134c",
    name: "Hardware & Tools"
  },
  {
    id: "5de3b7f89a8363641086134b",
    name: "Electronics"
  }
]

export const _suburbs = [
  {
    city: "Johannesburg",
    suburb: "Bryanston"
  },
  {
    city: "Johannesburg",
    suburb: "Rivonia"
  },
  {
    city: "Pretoria",
    suburb: "Centurion"
  },
  {
    city: "Cape Town",
    suburb: "Rondebosch"
  }
]

export const _cities = [
  "Johannesburg",
  "Cape Town",
  "Pretoria"
]

