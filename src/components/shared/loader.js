import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import { DotLoader } from "react-spinners";


const Loader = (props) => {
    const { showLoader } = props;

    return (
        <>
            <div className={showLoader === true ? "float-loader active-loader" : "float-loader"}>

                {/* <div className="float-loader"> */}

                <div className="loader-inner">
                    <DotLoader
                        css={"override loader"}
                        sizeUnit={"px"}
                        size={150}
                        color={'#50E3C2'}
                        loading={true} />

                </div>
            </div>


        </>
    )
}

export default Loader
