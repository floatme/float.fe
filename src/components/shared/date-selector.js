import React, { Component } from "react"
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';
import moment from 'moment';

const newDate = moment('2016-01-01');

class DateSelector extends Component {

    state = {
        // focusedInput: "",
        startDate: newDate,
        endDate: newDate
    }

    render() {
        return (
            <>
                <DateRangePicker
                    startDate={newDate} // momentPropTypes.momentObj or null,
                    startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
                    endDate={newDate} // momentPropTypes.momentObj or null,
                    endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
                    onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })} // PropTypes.func.isRequired,
                    focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                    onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                />
            </>

        )
    }
}

DateSelector.propTypes = {}

export default DateSelector


