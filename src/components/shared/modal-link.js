// pages/modal-example.js

import React from 'react'
import { Link } from 'gatsby'

const ModalLink = (props) => {


    const { isUserProfile, isDetail, userProfile, detailState, linkText, modalTitle, className } = props;

    return (
        <Link
            className={className}
            to="/modal/"
            state={{
                modal: true,
                userProfile: userProfile,
                detailState: detailState,
                title: modalTitle,
                isUserProfile: isUserProfile,
                isDetail: isDetail
            }}
        >
            {linkText}
        </Link>

        //         <Link
        //             to="/modal/"
        //             state={{
        //                 modal: true,
        //                 userProfile: undefined,
        //                 detail: {}
        //             }}
        //         >
        //             Detail Modal
        // </Link>
    )
}
export default ModalLink