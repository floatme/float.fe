import React from "react"
import PropTypes from "prop-types"

const Pagination = props => {
  const { page, count, onClickFn } = props

  const itemsPerPage = 10
  const totalPages = Math.ceil(count / itemsPerPage)
  const prevPage = page - 1
  const nextPage = page + 1
  const moreNextPage = page + 2
  const hasNextPage = totalPages - page > 1
  const hasMoreNextPage = totalPages - page > 2
  const hasEvenMoreNextPage = totalPages - page > 3

  return (
    <>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12">
          <div class="pagination-container margin-top-20 margin-bottom-40">
            <nav class="pagination">
              <ul>
                <li>
                  <a href="#" class="current-page">
                    {page > 1 ? prevPage : page}
                  </a>
                </li>
                {hasNextPage && (
                  <li>
                    <a href="#"> {page > 1 ? page : nextPage}</a>
                  </li>
                )}
                {hasMoreNextPage && (
                  <li>
                    <a href="#"> {page > 1 ? moreNextPage : nextPage}</a>
                  </li>
                )}
                {hasEvenMoreNextPage && (
                  <li>
                    <a href="#">
                      <i class="sl sl-icon-arrow-right"></i>
                    </a>
                  </li>
                )}
              </ul>
            </nav>
          </div>
        </div>
      </div>
      <div className="clearfix"></div>
    </>
  )
}

Pagination.propTypes = {}

export default Pagination
