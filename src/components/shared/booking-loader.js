import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import { DotLoader } from "react-spinners";


const BookingLoader = (props) => {
    const { showLoader } = props;

    return (
        <>
            <div className={showLoader === true ? "float-loader active-loader" : "float-loader"}>

                {/* <div className="float-loader"> */}

                <div className="loader-inner">
                    <img src={require("../../images/booking-loader.gif")} />
                    {/* <BookingLoaderGif /> */}
                </div>
            </div>


        </>
    )
}

export default BookingLoader
