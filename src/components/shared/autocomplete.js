import { Link } from "gatsby"
import PropTypes from "prop-types"
import React, { Component } from "react"
import { DotLoader } from "react-spinners";
import Autocomplete from "react-autocomplete";


class AutoCompleteContainer extends Component {

    constructor(props) {
        super(props)
        this.state = {
            value: '',
        }
    }

    // const { items, onChangeFn } = props;
    render() {
        console.log("props", this.props)
        return (
            <>
                <Autocomplete
                    items={this.props.items}
                    shouldItemRender={(item, value) => item.toLowerCase().indexOf(value.toLowerCase()) > -1}
                    getItemValue={item => item}
                    inputProps={{ placeholder: 'Location' }}
                    renderItem={(item, highlighted) =>
                        <div
                            key={item}
                            style={{ backgroundColor: highlighted ? '#eee' : 'transparent' }}
                        >
                            {item}
                        </div>
                    }
                    value={this.state.value}
                    onChange={e => this.setState({ value: e.target.value })}
                    onSelect={(value) => {
                        this.props.onChangeFn(value)
                        this.setState({
                            value: value
                        })
                    }}

                />

            </>
        )
    }

}

export default AutoCompleteContainer
