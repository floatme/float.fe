
import React from 'react';
import PlacesAutocomplete, {
    geocodeByAddress,
    getLatLng,
} from 'react-places-autocomplete';

class LocationSearchInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = { address: '' };
        const { inputCss, geoCode } = props;
    }

    handleChange = address => {
        this.setState({ address: address });
        //console.log("Adddress", address)

    };

    handleSelect = address => {
        this.setState({ address: address });
        if (this.props.changeLocation !== undefined) { this.props.changeLocation(address); }

        //Geocoding Service: You must enable Billing on the Google Cloud Project at https://console.cloud.google.com/project/_/billing/enable 
        if (this.props.geoCode === true) {
            // geocodeByAddress(address)
            //     .then(results => getLatLng(results[0]))
            //     .then(latLng => console.log('Success', latLng))
            //     .catch(error => console.error('Error', error));
        }
    };

    render() {
        return (

            <PlacesAutocomplete
                value={this.state.address}
                onChange={this.handleChange}
                onSelect={this.handleSelect}
            >
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (

                    < div >
                        {console.log(getInputProps)}
                        <input
                            value={this.state.address}
                            {...getInputProps({
                                placeholder: 'Search Places ...',
                                className: `location-search-input ${this.props.inputCss}`,
                            })}
                        />
                        <div className="autocomplete-dropdown-container">

                            {loading && <div>Loading...</div>}
                            {suggestions.map(suggestion => {
                                const className = suggestion.active
                                    ? 'suggestion-item--active'
                                    : 'suggestion-item';
                                // inline style for demonstration purpose
                                const style = suggestion.active
                                    ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                    : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                return (
                                    <div
                                        {...getSuggestionItemProps(suggestion, {
                                            className,
                                            style,
                                        })}
                                    >
                                        <span>{suggestion.description}</span>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                )
                }
            </PlacesAutocomplete>
        );
    }
}

export default LocationSearchInput