import Cookies from "js-cookie";
import {LoginStatusEnum} from "../enums/LoginStatusEnum";

const loginCookieName = "float-session-token";
const userIdCookieName = "float-user-id";
const userInfoCookieName = "float-user-info";

export function GetUserId() {
    const userId = Cookies.get(userIdCookieName);
    if (userId !== null && userId !== undefined) return userId;
    return undefined
}

export function GetJwToken() {
    const jwToken = Cookies.get(loginCookieName);
    if (jwToken !== null && jwToken !== undefined) return jwToken;
    return undefined
}

export function GetUserInfo() {
    const info = Cookies.get(userInfoCookieName)

    if (info === undefined) return {status: LoginStatusEnum.NONE};

    const hydratedUserInfo = JSON.parse(info);
    console.log("HYDR INFO", info)
    return hydratedUserInfo;
}


export function SetUserId(value) {
    Cookies.set(userIdCookieName, value);
}

export function SetJwToken(value) {
    Cookies.set(loginCookieName, value);
}

export function SetUserInfo(userInfo) {
    const dehydratedUserInfo = userInfo
    Cookies.set(userInfoCookieName, dehydratedUserInfo);
}

export function RemoveUserInfo() {
    Cookies.remove(userInfoCookieName);
}

export function RemoveUserId() {
    Cookies.remove(userIdCookieName);
}

export function RemoveJwToken() {
    Cookies.remove(loginCookieName);
}





