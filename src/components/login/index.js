import React, { Component } from "react"
import axios from "axios"
import { POST_verify_otp, POST_register } from "../../api/endpoints.js";
import header from "../../images/header.png"
import register from "../../images/svgs/undraw_address_udes.svg"
import otp from "../../images/svgs/undraw_people_search_wctu.svg"
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
const disabled = "tab-content hide-login";
const enabled = "tab-content show-login";
const TAB = {
    LOGIN: 'LOGIN',
    REGISTER: 'REGISTER',
    OTP: 'OTP',
    THANKS: 'THANKS'
};

export default class LoginMarkup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cellphone: "",
            password: "",
            confirmpassword: "",
            otp: "",
            firstname: "",
            surname: "",
            address: "",
            current_page: TAB.LOGIN.toString(),
            promptText: ""
        };
        this.register = this.register.bind(this);
    }


    toggleTabSelection = (active) => {
        this.setState({
            current_page: active.toString()
        });
    };

    verifyPassword() {
        const config = {
            headers: {'Access-Control-Allow-Origin': '*'}
        };
        axios.post(POST_verify_otp, { "CellPhone": this.state.cellphone, "OTP": this.state.otp }, config)
            .then(response => {

            })
    }

    register(e) {
        e.preventDefault();
        const data = {
            cellphone: this.state.cellphone,
            firstName: this.state.firstname,
            lastName: this.state.surname,
            password: this.state.password,
        };
        const config = {
            headers: {'Access-Control-Allow-Origin': '*'}
        };
        axios.post(POST_register, data, config)
            .then(response => {
                this.setState({
                    current_page: TAB.LOGIN.toString(),
                    promptText: "Please login using the OTP sent to your phone"
                });
            })
    }

    extractParametersFromSearch(search) {
        return search.split('=')[1];
    }

    componentDidMount() {
        const { location } = this.props;

        if (location !== undefined) {
            const { search } = location;
            const tabState = this.extractParametersFromSearch(search);
            if (tabState === TAB.LOGIN) this.toggleTabSelection(TAB.LOGIN);
            if (tabState === TAB.REGISTER) this.toggleTabSelection(TAB.REGISTER);
            if (tabState === TAB.OTP) this.toggleTabSelection(TAB.OTP);
        }
    }

    handleInputChange = event => {
        if (event !== undefined) {
            const target = event.target;
            const value = target.type === 'checkbox' ? target.checked : target.value;
            const name = target.name;
            this.setState({
                [name]: value,
            })
        }
    };

    responseFacebook = (response) => {
        console.log(response);
    };

    render() {
        return (
            <div id="sign-in-dialog" class="zoom-anim-dialog">
                <div className="small-dialog-header">
                    <h3>Sign In</h3>
                </div>

                {/* <!--Tabs --> */}
                <div className="sign-in-form style-1">
                    <ul className="tabs-nav">
                        <li><a onClick={() => this.toggleTabSelection(TAB.LOGIN)}>Log In</a></li>
                        <li><a onClick={() => this.toggleTabSelection(TAB.REGISTER)}>Register</a></li>
                        <li><a onClick={() => this.toggleTabSelection(TAB.OTP)}>Reset Password</a></li>
                    </ul>
                    <br />
                    <div className="tabs-container alt">
                        {/* <!-- Login --> */}
                        <div className={this.state.current_page === TAB.LOGIN.toString() ? enabled : disabled} id="tab1">
                            <div className="login">
                                <p className="form-row form-row-wide">
                                    <label htmlFor="username">Phone:
                                        <i className="im im-icon-Phone"></i>
                                        <input type="text"
                                               className={this.props.loginError !== false ? "login-error input-text" : "input-text"}
                                               name="cellphone" id="phone"
                                               value={this.state.cellphone}
                                               onChange={this.handleInputChange} />
                                    </label>
                                </p>

                                <p className="form-row form-row-wide">
                                    <label htmlFor="password">Password:
                                        <i className="im im-icon-Lock-2"></i>
                                        <input
                                            className={this.props.loginError !== false ? "login-error input-text" : "input-text"}
                                            type="password"
                                            name="password"
                                            value={this.state.password} onChange={this.handleInputChange} />
                                    </label>
                                    {this.props.loginError !== false ? <> <span className="incorrect_password">
                                        Incorrect username/password
                                    </span> <br /></> : ""}
                                    {this.props.promptText !== "" ? <> <span className="">
                                        {this.state.promptText}
                                    </span> <br /></> : ""}
                                    <div className="form-row">
                                        <p style={{textAlign:'center'}}>
                                            <input type="submit" className="button border margin-top-5" name="login" onClick={() => this.props.loginFn(this.state.cellphone, this.state.password)} value="Login" />
                                        </p>
                                    </div>
                                    <span className="lost_password">
                                        <br/>
                                        <p style={{textAlign:'center', color:'black'}}>Or login using</p>
                                        <br/>
                                    </span>
                                    <span className="lost_password">
                                        <p style={{textAlign:'center', color:'black'}}>
                                            <FacebookLogin
                                                appId="1088597931155576"
                                                autoLoad={false}
                                                fields="name,email,picture"
                                                callback={this.responseFacebook}
                                                cssClass="my-facebook-button-class"
                                                icon="fa-facebook"
                                            />
                                        </p>
                                    </span>

                                </p>

                            </div>
                            <br />
                            <img src={header} />
                        </div>

                        {/* <!-- Register --> */}
                        <div className={this.state.current_page === TAB.REGISTER ? enabled : disabled} id="tab2">
                            <div className="register">
                                <p className="form-row form-row-wide">
                                    <label for="firstname">First name:
                                        <i className="im im-icon-Phone"></i>
                                        <input type="text" className="input-text"
                                               name="firstname" id="firstname"
                                               value={this.state.firstname}
                                               onChange={this.handleInputChange} />
                                    </label>
                                </p>

                                <p className="form-row form-row-wide">
                                    <label htmlFor="surname">Surname:
                                        <i className="im im-icon-Phone"></i>
                                        <input type="text" className="input-text"
                                               name="surname" id="surname"
                                               value={this.state.surname}
                                               onChange={this.handleInputChange} />
                                    </label>
                                </p>

                                <p className="form-row form-row-wide">
                                    <label htmlFor="cellphone">Cellphone:
                                        <i className="im im-icon-Phone"></i>
                                        <input type="text" className="input-text" name="cellphone" id="cellphone"
                                               value="" value={this.state.cellphone} onChange={this.handleInputChange} />
                                    </label>
                                </p>
                                <p className="form-row form-row-wide">
                                    <div className="form-row">
                                        <input type="submit"
                                               className="button border margin-top-5"
                                               name="registers"
                                               onClick={this.register}
                                               value="Register" />
                                    </div>
                                </p>
                            </div>
                            <img src={register} />
                        </div>


                        <div className={this.state.current_page === TAB.OTP ? enabled : disabled} id="tab2">
                            <form method="post" className="register">
                                <p className="form-row form-row-wide">
                                    <label htmlFor="firstname">Cell Phone:
                                        <i className="im im-icon-Phone"></i>
                                        <input type="text" className="input-text"
                                               name="firstname" id="firstname"
                                               value={this.state.cellphone}
                                               onChange={this.handleInputChange} />
                                    </label>
                                </p>

                                <p className="form-row form-row-wide">
                                    <label htmlFor="firstname">OTP:
                                        <i className="im im-icon-Phone"></i>
                                        <input type="text" className="input-text"
                                               name="firstname" id="firstname"
                                               value={this.state.otp}
                                               onChange={this.handleInputChange} />
                                    </label>
                                </p>

                                <p className="form-row form-row-wide">
                                    <div className="form-row">
                                        <input type="submit"
                                               className="button border margin-top-5"
                                               name="sendOTP"
                                               onClick={() => this.sendOTP(this.state.phoneNumber)}
                                               value="Send OTP" /></div>
                                </p>

                            </form>
                            <img src={otp} />
                        </div>
                        <div className={this.state.current_page === TAB.THANKS ? enabled : disabled} id="tab5">
                            <form className="register">

                                <h4>
                                    Thank you {this.state.firstname} you are one step closer to floating items.<br /><br />
                                    We will be in touch soon.
                                </h4>

                            </form>
                        </div>

                    </div>
                </div>

            </div>

        )
    }
}
