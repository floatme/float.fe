export const LoginStatusEnum = Object.freeze({
    NONE: "NONE",
    SUCCESS: "SUCCESS",
    FAIL: "FAIL",
    BLOCK_TEMP: "BLOCK_TEMP",
    BLOCK_PERM: "BLOCK_PERM"
})
