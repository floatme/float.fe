/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */
import React, { Component } from "react"
import PropTypes from "prop-types"
import Header from "./header"
import Footer from "./footer"
import { DashBoardPageTypeEnum } from "../components/admin/dashboard/enums/DashboardPageTypeEnum";
import { LoginStatusEnum } from "../components/login/enums/LoginStatusEnum"
import { POST_login } from "../api/endpoints.js"
import { GetUserId, GetJwToken, SetUserId, SetJwToken, RemoveUserId, RemoveJwToken, GetUserInfo, SetUserInfo, RemoveUserInfo } from "../components/login/lib/cookies"

import Login from "./login/index"
import Loader from "../components/shared/loader"
import axios from "axios"
import "../css/style.css"
import "../css/main-color.css"
import { MapUserProfileFromResponse } from "../api/mapper";

const useMock = false;
const jwToken = GetJwToken();
const userId = GetUserId();
class Layout extends Component {
  constructor(props) {
    super(props);
    const initialLoginStatus = this.checkLoginStatus() === false ? LoginStatusEnum.FAIL : LoginStatusEnum.SUCCESS

    this.state = {
      loading: true,
      currentPage: DashBoardPageTypeEnum.HOME,
      loginError: false,
      user: {
        userId: userId,
        status: initialLoginStatus,
        token: jwToken,
        role: "",
        cellphone: "",
        password: "",
        firstname: "",
        surname: "",
        address: "",
        isVerified: false,
        isActive: true,
        profilePicUrl: "",
        lastLogin: "",

      }
    };
  }

  checkLoginStatus() {
    const jwToken = GetJwToken();
    const userId = GetUserId();
    const loginStatus = ((jwToken === null || jwToken === undefined || jwToken === "undefined")
        || (userId === null || userId === undefined || userId === "undefined"));

    return !loginStatus;
  }

  handleInputChange = event => {
    if (event !== undefined) {
      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;
      this.setState({
        [name]: value,
      })
    }
  }


  logout = () => {

    this.setState({
      loading: false,
      user: {
        status: LoginStatusEnum.NONE,
        token: "",
        cellphone: "",
        password: ""
      }
    })
    RemoveUserId();
    RemoveJwToken();
    RemoveUserInfo();
  }

  toggleLoader = (active) => {
    if (active === false) {
      setTimeout(() => {
        this.setState({
          loading: active
        });
      }, 200);
    } else {
      this.setState({
        loading: active
      });
    }
  }

  enforceLogin = (cellphone, password) => {
    this.setState({
      loading: true,
    });

    if (useMock !== true) {
      const jwToken = GetJwToken();
      const userId = GetUserId();
      const config = {
        headers: { 'Access-Control-Allow-Origin': '*' }
      };
      if (this.checkLoginStatus() === false) {
        axios.post(POST_login, { cellphone: cellphone, password: password }, config)
            .then(res => {
              console.log("LOGIN RESPONSE", res.data.token);
              this.setState({
                loading: false,
              });
              if (res.data.token !== null) {
                const receivedUser = MapUserProfileFromResponse(res.data);
                console.log("RECIEVED USER", receivedUser);
                this.setState({
                  user: receivedUser
                });
                SetJwToken(receivedUser.token);
                SetUserId(receivedUser.userId);
                SetUserInfo(receivedUser);
              } else {
                this.setState({
                  user: {
                    status: LoginStatusEnum.FAIL,
                    token: null,
                    cellphone: res.data.cellphone,
                    password: "",
                    userId: null
                  }
                });
                RemoveJwToken();
                RemoveUserId();
                RemoveUserInfo();
              }

            }).catch(
            error => {
              this.setState({
                loading: false,
                loginError: true
              });
            }
        )
      } else {
        //cookie exists
        this.setState({
          loading: false,
          user: {
            status: LoginStatusEnum.SUCCESS,
            token: jwToken,
            cellphone: "",
            password: "",
            userId: userId
          }
        })
      }
    }
  };

  register(firstName, surname, address, cellphone) {
    axios.post("http://ec2-18-234-53-88.compute-1.amazonaws.com/api/user/authenticate/",
        {
          firstName: firstName,
          surname: surname,
          address: address,
          cellphone: cellphone
        });
  }
  extractParametersFromSearch(search) {
    return search.split('=')[1];
  }
  componentDidMount() {
    this.toggleLoader(false);
    if (this.state.user === undefined) {
      this.setState({ user: { isLoggedIn: false } })
    }
  }

  render() {
    console.log("RENDER STATE", this.props.children)
    return (
        <>
          < body className={this.props.bodyCss}>
          <div id="wrapper">
            <>
              {(this.props.displayHeader === true) ?
                  <Header
                      userInfo={this.state.user}
                      logoutFn={this.logout}
                      headerContainerClass={this.props.headerContainerClass}
                      headerClass={this.props.headerClass}
                      displaySignUp={this.props.displaySignUp}
                  /> : ""}
              <main>{this.props.children}</main>
              {this.props.hideFooter === "true" ? "" :
                  <Footer logoutFn={this.logout} />
              }
            </>
          </div>
          <Loader showLoader={this.state.loading} />
          </body>
        </>
    )
  }
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;