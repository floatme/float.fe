import React from "react"
import PropTypes from "prop-types"
import $ from "jquery"

import Helmet from "react-helmet"
import { withPrefix, Link } from "gatsby"

class Footer extends React.Component {
  componentDidMount() {
    /*--------------------------------------------------*/
    /*  Mobile Menu - mmenu.js
  /*--------------------------------------------------*/
    // $(function () {
    //   function mmenuInit() {
    //     var wi = $(window).width()
    //     if (wi <= "1024") {
    //       $(".mmenu-init").remove()
    //       $("#navigation")
    //         .clone()
    //         .addClass("mmenu-init")
    //         .insertBefore("#navigation")
    //         .removeAttr("id")
    //         .removeClass("style-1 style-2")
    //         .find("ul, div")
    //         .removeClass(
    //           "style-1 style-2 mega-menu mega-menu-content mega-menu-section"
    //         )
    //         .removeAttr("id")
    //       $(".mmenu-init")
    //         .find("ul")
    //         .addClass("mm-listview")
    //       $(".mmenu-init")
    //         .find(".mobile-styles .mm-listview")
    //         .unwrap()
    //       debugger;
    //       $(".mmenu-init").mmenu(
    //         {
    //           counters: true,
    //         },
    //         {
    //           // configuration
    //           offCanvas: {
    //             pageNodetype: "#wrapper",
    //           },
    //         }
    //       )
    //       var mmenuAPI = $(".mmenu-init").data("mmenu")
    //       debugger;
    //       var $icon = $(".hamburger")
    //       $(".mmenu-trigger").click(function () {
    //         mmenuAPI.open()
    //       })
    //       mmenuAPI.bind("open:finish", function () {
    //         setTimeout(function () {
    //           $icon.addClass("is-active")
    //         })
    //       })
    //       mmenuAPI.bind("close:finish", function () {
    //         setTimeout(function () {
    //           $icon.removeClass("is-active")
    //         })
    //       })
    //     }
    //     $(".mm-next").addClass("mm-fullsubopen")
    //   }
    //   mmenuInit()
    //   $(window).resize(function () {
    //     mmenuInit()
    //   })
    // })
    // /*  User Menu */
    // $(".user-menu").on("click", function () {
    //   $(this).toggleClass("active")
    // })


  }
  render() {
    const { logoutFn } = this.props;
    return (
      <>
        {/* <Helmet title="foo bar" defer={false} /> */}
        <Helmet>
          {/* <script
            src={withPrefix("jquery-3.4.1.min.js")}
            type="text/javascript"
          />
          
          <script src={withPrefix("jquery-migrate-3.1.0.min.js")} type="text/javascript" /> */}
          {/* <script src={withPrefix("mmenu.min.js")} type="text/javascript" /> */}
          {/* <html className="PIE"></html> */}

          {/* <script src={withPrefix("chosen.min.js")} type="text/javascript" />
           */}


          {/* <script src={withPrefix("custom.js")} type="text/javascript" /> */}
        </Helmet>
        <div id="footer" className="sticky-footer">
          {/* <!-- Main --> */}
          <div className="container">
            <div className="row">
              <div className="col-md-5 col-sm-6">
                <img className="footer-logo" src="images/logo-2.png" alt="" />
                <br />
                <br />
                <p>Lend and borrow stuff on the go.</p>
              </div>

              <div className="col-md-4 col-sm-6 ">
                <h4>Helpful Links</h4>
                <ul className="footer-links">


                  <li>
                    {/* <a href="/">Login</a> */}
                    <Link
                      to={`../../?Tab=LOGIN`}
                      onClick={() => logoutFn()}

                    >Login</Link>
                  </li>
                  <li>
                    <a href="#">Sign Up</a>
                    <Link
                      to={`../../?Tab=REGISTER`}
                      onClick={() => logoutFn()}

                    >Register</Link>
                  </li>
                  <li>
                    <a href="#">Pricing</a>
                  </li>
                  <li>
                    <a href="#">Privacy Policy</a>
                  </li>
                </ul>

                <ul className="footer-links">
                  <li>
                    <a href="#">FAQ</a>
                  </li>
                  <li>
                    <a href="#">Blog</a>
                  </li>
                  <li>
                    <a href="#">Our Partners</a>
                  </li>
                  <li>
                    <a href="#">How It Works</a>
                  </li>
                  <li>
                    <a href="#">Contact</a>
                  </li>
                </ul>
                <div className="clearfix"></div>
              </div>

              <div className="col-md-3  col-sm-12">
                <h4>Contact Us</h4>
                <div className="text-widget">
                  <span>80 Kathrine St, Sandton</span> <br />
                  E-Mail:
                  <span>
                    {" "}
                    <a href="mailto:support@justfloat.app">support@justfloat.app</a>{" "}
                  </span>
                  <br />
                </div>

                <ul className="social-icons margin-top-20">
                  <li>
                    <a className="facebook" href="https://www.facebook.com/float.app.za/" target="_blank">
                      <i className="icon-facebook"/>
                    </a>
                  </li>
                  <li>
                    <a className="twitter" href="https://twitter.com/za_float" target="_blank">
                      <i className="icon-twitter"/>
                    </a>
                  </li>
                  <li>
                    <a className="instagram" href="https://www.instagram.com/justfloat.app/" target="_blank">
                      <i className="icon-instagram"/>
                    </a>
                  </li>
                </ul>
              </div>
            </div>

            {/* <!-- Copyright --> */}
            <div className="row">
              <div className="col-md-12">
                <div className="copyrights">
                  © 2020 Float. All Rights Reserved.
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* <script
        type="text/javascript"
        src="../scripts/jquery-3.4.1.min.js"
      ></script>
      <script
        type="text/javascript"
        src="../scripts/jquery-migrate-3.1.0.min.js"
      ></script>
      <script type="text/javascript" src="../scripts/mmenu.min.js"></script>
      <script type="text/javascript" src="../scripts/custom.js"></script> */}
      </>
    )
  }
}

// const Footer = () => {
//   return (
//     <>
//       <div id="footer" className="sticky-footer">
//         {/* <!-- Main --> */}
//         <div className="container">
//           <div className="row">
//             <div className="col-md-5 col-sm-6">
//               <img className="footer-logo" src="images/logo-2.png" alt="" />
//               <br />
//               <br />
//               <p>Lend and borrow stuff on the go.</p>
//             </div>

//             <div className="col-md-4 col-sm-6 ">
//               <h4>Helpful Links</h4>
//               <ul className="footer-links">
//                 <li>
//                   <a href="#">Login</a>
//                 </li>
//                 <li>
//                   <a href="#">Sign Up</a>
//                 </li>
//                 <li>
//                   <a href="#">Pricing</a>
//                 </li>
//                 <li>
//                   <a href="#">Privacy Policy</a>
//                 </li>
//               </ul>

//               <ul className="footer-links">
//                 <li>
//                   <a href="#">FAQ</a>
//                 </li>
//                 <li>
//                   <a href="#">Blog</a>
//                 </li>
//                 <li>
//                   <a href="#">Our Partners</a>
//                 </li>
//                 <li>
//                   <a href="#">How It Works</a>
//                 </li>
//                 <li>
//                   <a href="#">Contact</a>
//                 </li>
//               </ul>
//               <div className="clearfix"></div>
//             </div>

//             <div className="col-md-3  col-sm-12">
//               <h4>Contact Us</h4>
//               <div className="text-widget">
//                 <span>80 Kathrine St, Sandton</span> <br />
//                 E-Mail:
//                 <span>
//                   {" "}
//                   <a href="#">hi@floatme.com</a>{" "}
//                 </span>
//                 <br />
//               </div>

//               <ul className="social-icons margin-top-20">
//                 <li>
//                   <a className="facebook" href="#">
//                     <i className="icon-facebook"></i>
//                   </a>
//                 </li>
//                 <li>
//                   <a className="twitter" href="#">
//                     <i className="icon-twitter"></i>
//                   </a>
//                 </li>
//                 <li>
//                   <a className="instagram" href="#">
//                     <i className="icon-instagram"></i>
//                   </a>
//                 </li>
//               </ul>
//             </div>
//           </div>

//           {/* <!-- Copyright --> */}
//           <div className="row">
//             <div className="col-md-12">
//               <div className="copyrights">
//                 © 2019 Float. All Rights Reserved.
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//       {/* <script
//         type="text/javascript"
//         src="../scripts/jquery-3.4.1.min.js"
//       ></script>
//       <script
//         type="text/javascript"
//         src="../scripts/jquery-migrate-3.1.0.min.js"
//       ></script>
//       <script type="text/javascript" src="../scripts/mmenu.min.js"></script>
//       <script type="text/javascript" src="../scripts/custom.js"></script> */}
//     </>
//   )
// }

Footer.propTypes = {}

export default Footer
