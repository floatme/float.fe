import axios from "axios"
import React, { Component } from "react"
import PropTypes from "prop-types"
import SlideImage from "../slider/slide"
import { Result } from "antd"
import { GET_detail, GET_upvote_review } from "../../../api/endpoints"

class ReviewItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isUpvoted: false,
      upvoteEnabled: true
    }
  }


  UpvoteFn = listingId => {
    console.log("Getting detail ...")
    this.setState({ loading: true })
    axios
      .get(`${GET_upvote_review}/${this.props.reviewId}/${this.props.listingId}/${this.props.userId}/`)
      .then(response => {
        this.setState({
          loading: false,
          upcount: response.data
        })
      })
      .catch(error => {
        console.log("Bad response", error)
        this.setState({ loading: false, error })
      })
  }

  createRating = (ratingInt) => {
    let rating = [];

    for (let x = 0; x <= ratingInt; x++) {
      rating.push(<span class="star"></span>)
    }
    return rating;
  }

  render() {
    const {
      username,
      gravatar,
      dateCreated,
      reviewText,
      images,
      reviewUpCount,
      userVerified,
      rating
    } = this.props
    const ratingInt = parseInt(rating, 10);
    console.log("RATINg", rating)

    return (
      <>
        <li>
          <div className="avatar">
            <img src={gravatar} alt="" />
          </div>
          <div className="comment-content">
            <div className="arrow-comment"></div>
            <div className="comment-by">
              {username}{" "}
              {userVerified === true ? <i
                className="tip"
                data-tip-content="Person who left this review actually was a customer"
              ></i> : ""}
              <span className="date">{dateCreated}</span>

              <div class="star-rating">
                {
                  this.createRating(ratingInt)
                }
              </div>
            </div>
            <p>{reviewText}</p>

            <div className="review-images mfp-gallery-container">
              {images === null ? "" : images.map(value => (
                <SlideImage
                  key={value}
                  title={value}
                  image={value}
                  className={"mfp-gallery"}
                />
              ))}
            </div>
            {this.props.showUpvote !== false ? <a onClick={() => { this.UpvoteFn() }} className={this.state.upvoteEnabled === true ? "rate-review" : "disabled rate-review"} >
              <i className="sl sl-icon-like"></i> Helpful Review{" "}
              <span>{reviewUpCount}</span>
            </a> : ""}
          </div>
        </li >
      </>
    )
  }
}

ReviewItem.propTypes = {
  userName: PropTypes.string,
  gravatar: PropTypes.string,
  dateCreated: PropTypes.string,
  rating: PropTypes.string,
  reviewText: PropTypes.string,
  images: PropTypes.string,
  reviewUpCount: PropTypes.string,
}

export default ReviewItem
