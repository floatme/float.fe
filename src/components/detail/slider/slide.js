import React from "react"
import PropTypes from "prop-types"

const SlideImage = props => {
  const { image, title, className } = props

  return (
    <>
      <a
        href={image}
        data-background-image={image}
        className={className}
        title={title}
      >
        <img className={className} src={image} />
      </a>
    </>
  )
}

SlideImage.propTypes = {
  image: PropTypes.string,
  className: PropTypes.string,
}

export default SlideImage
