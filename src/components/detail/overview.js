import React from "react"
import PropTypes from "prop-types"

const Overview = props => {
  const {
    description,
    terms,
    phoneNumber,
    email,
    socialMedia,
    delivery,
    collection,
  } = props
  console.log("OVERVIEW, props", props)
  return (
    <>
      {/* <!-- Overview --> */}
      <div id="listing-overview" className="listing-section">
        {/* <!-- Description --> */}

        <p>{description}</p>

        {/* <!-- Listing Contacts --> */}
        <div className="listing-links-container">
          <ul className="listing-links contact-links">
            <li>
              <a href="tel:12-345-678" className="listing-links">
                <i className="fa fa-phone"></i> {phoneNumber}
              </a>
            </li>
            <li>
              <a href="mailto:mail@example.com" className="listing-links">
                <i className="fa fa-envelope-o"></i>
                {email}
              </a>
            </li>
          </ul>
          <div className="clearfix"></div>
          {socialMedia !== undefined ? (<ul className="listing-links">
            {socialMedia.fbHandle !== "" && (
              <li>
                <a
                  href={socialMedia.fbHandle}
                  target="_blank"
                  className="listing-links-fb"
                >
                  <i className="fa fa-facebook-square"></i> Facebook
    </a>
              </li>
            )}
            {socialMedia.ytHandle !== "" && (
              <li>
                <a
                  href={socialMedia.ytHandle}
                  target="_blank"
                  className="listing-links-yt"
                >
                  <i className="fa fa-youtube-play"></i> YouTube
    </a>
              </li>
            )}
            {socialMedia.inHandle !== "" && (
              <li>
                <a
                  href={socialMedia.inHandle}
                  target="_blank"
                  className="listing-links-ig"
                >
                  <i className="fa fa-instagram"></i> Instagram
    </a>
              </li>
            )}
            {socialMedia.twHandle !== "" && (
              <li>
                <a
                  href={socialMedia.twHandle}
                  target="_blank"
                  className="listing-links-tt"
                >
                  <i className="fa fa-twitter"></i> Twitter
    </a>
              </li>
            )}
          </ul>) : ""}

          <div className="clearfix"></div>
        </div>
        <div className="clearfix"></div>

        {/* <!-- Amenities --> */}
        <h3 className="listing-desc-headline">Delivery / Collection</h3>
        <ul className="listing-features checkboxes margin-top-0">
          {delivery === true && <li>Delivery available</li>}
          {collection === true && <li>Collection available</li>}
        </ul>

        <h2 className="listing-desc-headline">Terms of use</h2>
        <p>{terms}</p>
      </div>
    </>
  )
}

Overview.propTypes = {
  description: PropTypes.string,
  phoneNumber: PropTypes.string,
  email: PropTypes.string,
  socialMedia: PropTypes.object,
  delivery: PropTypes.bool,
  collection: PropTypes.bool,
}

export default Overview
