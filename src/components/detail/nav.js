import React from "react"
import PropTypes from "prop-types"

const Nav = props => {
  return (
    <>
      {/* <!-- Listing Nav --> */}
      <div id="listing-nav" className="listing-nav-container">
        <ul className="listing-nav">
          <li>
            <a href="#listing-overview" className="active">
              Overview
            </a>
          </li>
          <li>
            <a href="#listing-pricing-list">Pricing</a>
          </li>
          <li>
            <a href="#listing-location">Location</a>
          </li>
          <li>
            <a href="#listing-reviews">Reviews</a>
          </li>
          <li>
            <a href="#add-review">Add Review</a>
          </li>
        </ul>
      </div>
    </>
  )
}

Nav.propTypes = {}

export default Nav
