import React from "react"
import PropTypes from "prop-types"

const TitleBar = props => {
  const { title, category, subCategory, location, reviewCount } = props
  return (
    <>
      {/* <!-- Titlebar --> */}
      <div id="titlebar" className="listing-titlebar">
        <div className="listing-titlebar-title">
          <h2>
            {title} <span className="listing-tag">{subCategory.name}</span>
          </h2>
          <span>
            <a href="#listing-location" className="listing-address">
              <i className="fa fa-map-marker"></i>
              {location}
            </a>
          </span>
          <div className="star-rating" data-rating="5">
            <div className="rating-counter">
              <a href="#listing-reviews">({reviewCount} reviews)</a>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

TitleBar.propTypes = {
  title: PropTypes.node.isRequired,
  subCategory: PropTypes.node.isRequired,
  location: PropTypes.node.isRequired,
  reviewCount: PropTypes.node.isRequired,
}

export default TitleBar
