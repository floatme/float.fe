import React from "react"
import PropTypes from "prop-types"

const ListingMap = props => {
  const { gps } = props
  return (
    <>
      {/* <!-- Location --> */}
      <div id="listing-location" className="listing-section">
        <h3 className="listing-desc-headline margin-top-60 margin-bottom-30">
          Location
        </h3>

        <div id="singleListingMap-container">
          <div
            id="singleListingMap"
            data-latitude={gps.latitude}
            data-longitude={gps.longitude}
            data-map-icon="im im-icon-Hamburger"
          ></div>
          <a href="#" id="streetView">
            Street View
          </a>
        </div>
      </div>
    </>
  )
}

ListingMap.propTypes = {
  gps: PropTypes.object,
}

export default ListingMap
