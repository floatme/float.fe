import React from "react"
import PropTypes from "prop-types"

const Pricing = props => {
  const { title, pricePerDay, pricePerWeek } = props

  return (
    <>
      <div id="listing-pricing-list" className="listing-section">
        <h3 className="listing-desc-headline margin-top-70 margin-bottom-30">
          Pricing
        </h3>

        <div className="show-mor">
          <div className="pricing-list-container">
            <h4>{title} price/day</h4>
            <ul>
              <li>
                <h5>R{pricePerDay}.00</h5>
                <span>incl. vat</span>
              </li>
            </ul>
            {/* {pricePerWeek !== "" && (
              <>
                <h4>{title} price/week</h4>
                <ul>
                  <li>
                    <h5>R{pricePerWeek}</h5>
                    <span>incl. vat</span>
                  </li>
                </ul>
              </>
            )} */}
          </div>
        </div>
        {/* <a
          href="#"
          className="show-more-button"
          data-more-title="Show More"
          data-less-title="Show Less"
        >
          <i className="fa fa-angle-down"></i>
        </a> */}
      </div>
    </>
  )
}

Pricing.propTypes = {
  title: PropTypes.string,
  pricePerDay: PropTypes.number,
  pricePerWeek: PropTypes.number,
}

export default Pricing
