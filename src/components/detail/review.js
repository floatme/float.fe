import React from "react"
import PropTypes from "prop-types"
import ReviewItem from "../detail/review/review-item"

const Review = props => {
  const { reviews } = props
  return (
    <>
      {/* <!-- Reviews --> */}
      <section className="comments listing-reviews">
        <ul>
          {reviews.map(value => (
            <ReviewItem
              username={value.username}
              gravatar={value.gravatar}
              dateCreated={value.dateCreated}
              rating={value.rating}
              reviewText={value.reviewText}
              images={value.images}
              reviewUpCount={value.reviewUpCount}
              userVerified={value.userVerified}
            />
          ))}
        </ul>
      </section>
    </>
  )
}

Review.propTypes = {}

export default Review
