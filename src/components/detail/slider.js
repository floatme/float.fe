import React from "react"
import PropTypes from "prop-types"
import SlideImage from "../detail/slider/slide"
import { Carousel } from "antd"

const Slider = props => {
  const { images } = props
  console.log("SLKIDER", images)
  return (
    <>
      {/* <!-- Slider
================================================== --> */}
      <div class="listing-slider mfp-gallery-container margin-bottom-0">
        <Carousel autoplay>
          {images.map(value => (
            <>

              <SlideImage
                key={value.url}
                title={value.title}
                image={value.url}
                className={"item mfp-gallery detail-gallery"}
              />
            </>
          ))}
        </Carousel>
      </div>
    </>
  )
}

Slider.propTypes = {
  images: PropTypes.array,
}

export default Slider
