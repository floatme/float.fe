import React from "react"
import PropTypes from "prop-types"

const RatingOverview = props => {
  const { ratings } = props
  return (
    <>
      {/* <!-- Rating Overview --> */}
      <div className="rating-overview">
        <div className="rating-overview-box">
          <span className="rating-overview-box-total">{ratings.rating}</span>
          <span className="rating-overview-box-percent">out of 5.0</span>
          <div className="star-rating" data-rating="5"></div>
        </div>

        <div className="rating-bars">
          <div className="rating-bars-item">
            <span className="rating-bars-name">
              Service{" "}
              <i
                className="tip"
                data-tip-content="Quality of customer service and attitude to work with you"
              ></i>
            </span>
            <span className="rating-bars-inner">
              <span
                className="rating-bars-rating"
                data-rating={ratings.service}
              >
                <span className="rating-bars-rating-inner"></span>
              </span>
              <strong>{ratings.service}</strong>
            </span>
          </div>
          <div className="rating-bars-item">
            <span className="rating-bars-name">
              Value for Money{" "}
              <i
                className="tip"
                data-tip-content="Overall experience received for the amount spent"
              ></i>
            </span>
            <span className="rating-bars-inner">
              <span
                className="rating-bars-rating"
                data-rating={ratings.valueForMoney}
              >
                <span className="rating-bars-rating-inner"></span>
              </span>
              <strong>{ratings.valueForMoney}</strong>
            </span>
          </div>
          <div className="rating-bars-item">
            <span className="rating-bars-name">
              Quality{" "}
              <i
                className="tip"
                data-tip-content="Visibility, commute or nearby parking spots"
              ></i>
            </span>
            <span className="rating-bars-inner">
              <span
                className="rating-bars-rating"
                data-rating={ratings.quality}
              >
                <span className="rating-bars-rating-inner"></span>
              </span>
              <strong>{ratings.quality}</strong>
            </span>
          </div>
          <div className="rating-bars-item">
            <span className="rating-bars-name">
              Owner{" "}
              <i
                className="tip"
                data-tip-content="The physical condition of the business"
              ></i>
            </span>
            <span className="rating-bars-inner">
              <span className="rating-bars-rating" data-rating={ratings.owner}>
                <span className="rating-bars-rating-inner"></span>
              </span>
              <strong>{ratings.owner}</strong>
            </span>
          </div>
        </div>
      </div>
      {/* <!-- Rating Overview / End --> */}
    </>
  )
}

RatingOverview.propTypes = {
  ratings: PropTypes.object,
}

export default RatingOverview
