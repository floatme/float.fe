import React from "react"
import PropTypes from "prop-types"
import { DatePicker } from "antd"
import axios from 'axios';
import BookingForm from "./booking-form";
import { GET_add_bookmark } from "../../../api/endpoints";
import { GET_remove_bookmark } from "../../../api/endpoints"

const SideBar = props => {
  console.log("SIDEBAR PROPS", props)
  const { verified, bookingFriendlyDates, owner, socialMedia, bookmarkCount, isBookmarked, bookmarkFn, removeBookmarkFn, listingId } = props
  return (
    <>
      {/* <!-- Sidebar */}
      {/* ================================================== --> */}
      <div className="col-lg-4 col-md-4 margin-top-75 sticky">
        {/* <!-- Verified Badge --> */}
        {verified === "true" && (
          <div
            className="verified-badge with-tip"
            data-tip-content="Listing has been verified and belongs the business owner or manager."
          >
            <i className="sl sl-icon-check"></i> Verified Listing
          </div>
        )}
        {/* <!-- Book Now --> */}
        <div
          id="booking-widget-anchor"
          className="boxed-widget booking-widget margin-top-35"
        >
          <h3>
            <i className="fa fa-calendar-check-o "></i> Booking
          </h3>

          <BookingForm listingId={listingId} />
        </div>
        {/* <!-- Book Now / End --> */}

        {/* <!-- Opening Hours --> */}
        <div className="boxed-widget opening-hours margin-top-35">
          <div className="listing-badge now-open">Bookings</div>
          <h3>
            <i className="sl sl-icon-clock"></i> Current bookings
          </h3>
          <ul>
            {bookingFriendlyDates.length > 0 ? bookingFriendlyDates.map(value => (
              <li>
                {value.startDate} <span> {value.days} days </span>
              </li>
            )) : " No bookings yet.."}
          </ul>
        </div>
        {/* <!-- Opening Hours / End --> */}

        {/* <!-- Contact --> */}
        <div className="boxed-widget margin-top-35">
          <div className="hosted-by-title">
            <h4>
              <span>Hosted by</span>{" "}
              <a href="pages-user-profile.html">{owner.name}</a>
            </h4>
            <a href="pages-user-profile.html" className="hosted-by-avatar">
              <img src={owner.gravatar} alt="" />
            </a>
          </div>
          <ul className="listing-details-sidebar">
            <li>
              <i className="sl sl-icon-phone"></i> {owner.phone}
            </li>
            {/* <!-- <li><i className="sl sl-icon-globe"></i> <a href="#">http://example.com</a></li> --> */}
            <li>
              <i className="fa fa-envelope-o"></i> <a href="#">{owner.email}</a>
            </li>
          </ul>

          <ul className="listing-details-sidebar social-profiles">
            {socialMedia.fbHandle !== "" && (
              <li>
                <a href={socialMedia.fbHandle} className="facebook-profile">
                  <i className="fa fa-facebook-square"></i> Facebook
                </a>
              </li>
            )}
            {socialMedia.twHandle !== "" && (
              <li>
                <a href={socialMedia.twHandle} className="twitter-profile">
                  <i className="fa fa-twitter"></i> Twitter
                </a>
              </li>
            )}
            {/* <!-- <li><a href="#" className="gplus-profile"><i className="fa fa-google-plus"></i> Google Plus</a></li> --> */}
          </ul>

          {/* <!-- Reply to review popup --> */}
          <div id="small-dialog" className="zoom-anim-dialog mfp-hide">
            <div className="small-dialog-header">
              <h3>Send Message</h3>
            </div>
            <div className="message-reply margin-top-0">
              <textarea
                cols="40"
                rows="3"
                placeholder="Your message to Tom"
              ></textarea>
              <button className="button">Send Message</button>
            </div>
          </div>

          <a
            href="#small-dialog"
            className="send-message-to-owner button popup-with-zoom-anim"
          >
            <i className="sl sl-icon-envelope-open"></i> Send Message
          </a>
        </div>
        {/* <!-- Contact / End--> */}

        {/* <!-- Share / Like --> */}
        <div className="listing-share margin-top-40 margin-bottom-40 no-border">
          {isBookmarked === true ? (
            <button onClick={() => removeBookmarkFn()} className="like-button">
              <span className="like-icon liked"></span>Remove bookmark
          </button>
          ) :
            (
              <button onClick={() => bookmarkFn()} className="like-button">
                <span className="like-icon"></span> Bookmark this listing
            </button>
            )
          }

          <span>{bookmarkCount} people bookmarked this place</span>

          {/* <!-- Share Buttons --> */}
          <ul className="share-buttons margin-top-40 margin-bottom-0">
            <li>
              <a className="fb-share" href="#">
                <i className="fa fa-facebook"></i> Share
              </a>
            </li>
            <li>
              <a className="twitter-share" href="#">
                <i className="fa fa-twitter"></i> Tweet
              </a>
            </li>
            <li>
              <a className="gplus-share" href="#">
                <i className="fa fa-google-plus"></i> Share
              </a>
            </li>
            {/* <!-- <li><a className="pinterest-share" href="#"><i className="fa fa-pinterest-p"></i> Pin</a></li> --> */}
          </ul>
          <div className="clearfix"></div>
        </div>
      </div>
      {/* <!-- Sidebar / End --> */}
    </>
  )
}

SideBar.propTypes = {}

export default SideBar
