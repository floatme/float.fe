import React, { Component } from "react"
import axios from 'axios';
import { Link } from "gatsby"

import { POST_add_booking, POST_check_availability } from "../../../api/endpoints"
import { MapAddBookingFromState } from "../../../api/mapper";
import { GetUserId } from "../../../components/login/lib/cookies";
import { throws } from "assert";
import { AvailabilityEnum, AvailabilityValidation } from "../../admin/dashboard/enums/AvailabilityEnum";
import Loader from "../../shared/loader";
import BookingLoader from "../../shared/booking-loader";
import { Button } from "antd";

class BookingForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: "2019-10-10",
            endDate: "2019-10-10",
            delivery: false,
            listingId: "",
            userId: GetUserId(),
            availability: AvailabilityEnum.UNAVAILABLE,
            loading: false,
            validationError: ""
        };
    }

    handleInputChange = event => {
        if (event !== undefined) {
            const target = event.target
            const value = target.type === 'checkbox' ? target.checked : target.value;
            const name = target.name
            this.setState({
                [name]: value,
            })
        }
    }

    toggleLoader(active) {
        if (active === true) {
            this.setState({ loading: true });
        }
        else {
            setTimeout(
                function () {
                    this.setState({ loading: false });
                }
                    .bind(this),
                5000
            );
        }
    }

    checkAvailability() {

        //  e.preventDefault();
        this.toggleLoader(true);
        const payload = { startDate: this.state.startDate, endDate: this.state.endDate };
        axios.post(POST_check_availability(this.props.listingId, this.state.startDate, this.state.endDate), payload)
            .then(res => {
                console.log('[SUCCESS] Check availability response', res);
                this.setState({ availability: res.data, validationError: "" })
                if (res.data !== AvailabilityEnum.AVAILABLE) {
                    console.log("AvailabilityValidation, ", AvailabilityValidation)
                    var validationMessage = AvailabilityValidation[res.data]
                    this.setState({ validationError: validationMessage })

                }
                this.toggleLoader(false);
            }).catch(error => {
                console.log(`[ERROR] Check availability error: `, { error })
                this.toggleLoader(false);
                this.setState({ availability: AvailabilityEnum.UNAVAILABLE, error })
            })
    }

    handleSubmit = () => {
        this.toggleLoader(true);
        //e.preventDefault();
        const payload = MapAddBookingFromState(this.state, this.state.userId, this.props.listingId)
        console.log("PAYLOAD", payload)
        axios.post(POST_add_booking, payload)
            .then(res => {
                console.log('[SUCCESS] Add booking response', res);
                this.setState({ availability: false })
                this.toggleLoader(false);
            }).catch(error => {
                console.log(`[ERROR] Adding booking, error: `, { error })
                this.toggleLoader(false);
                this.setState({ availability: false, error })
            })
    }


    render() {
        return (
            <>
                <div className="row with-forms  margin-top-0">
                    <form
                        onSubmit={this.state.availability === AvailabilityEnum.AVAILABLE ? this.handleSubmit : this.checkAvailability}
                    >
                        {/* <!-- Date Range Picker - docs: http://www.daterangepicker.com/ --> */}
                        <div className="col-lg-12">
                            <label>From: </label>
                            <input
                                type="date"
                                name="startDate"
                                id="date-picker"
                                placeholder="From"
                                onChange={this.handleInputChange}
                                required
                            // readonly="readonly"
                            />
                            {/* <DateSelector /> */}
                            {/* <DatePicker /> */}
                            <label>Until: </label>
                            <input
                                type="date"
                                id="date-picker"
                                name="endDate"
                                placeholder="Until"
                                onChange={this.handleInputChange}
                                required
                            // readonly="readonly"
                            />
                        </div>
                        {this.state.validationError === "" ? "" : (
                            <span className="booking-validation-error">
                                {this.state.validationError}
                            </span>
                        )}

                        {/* <!-- Book Now --> */}
                        {this.state.availability === AvailabilityEnum.AVAILABLE ?

                            <button type="button"
                                onClick={() => this.handleSubmit()}
                                //value=""
                                className="button book-now fullwidth margin-top-5 green-background-button"
                            >Request Booking</button>
                            :
                            <button type="button"
                                onClick={() => this.checkAvailability()}
                                value=""
                                className="button book-now fullwidth margin-top-5"
                            >Check Availability</button>
                        }
                    </form>

                    <BookingLoader showLoader={this.state.loading} />
                </div>

            </>
        )
    }

}

BookingForm.propTypes = {}

export default BookingForm
