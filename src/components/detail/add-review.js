import axios from 'axios';
import React, { Component } from "react"
import PropTypes from "prop-types"
import { GetUserInfo } from "../../components/login/lib/cookies"
import FileUpload from "../admin/dashboard/addListing/fileUpload";
import { POST_add_review_image, POST_add_review } from "../../api/endpoints"
import { MapAddListingFromState, MapAddReviewFromState, MapAddImageFromState } from "../../api/mapper";
import Loader from '../shared/loader';

class AddReview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      error: false,
      valueForMoney: "",
      service: "",
      quality: "",
      availability: "",
      reviewText: "",
      name: "",
      email: "",
      images: [],
      files: [],
      uploadImages: false,
      listingId: "",
      userId: ""
    }
  }

  setDefaultState() {
    this.setState({
      ...this.state,
      loading: false,
      error: false,
      valueForMoney: "",
      service: "",
      quality: "",
      availability: "",
      reviewText: "",
      name: "",
      email: "",
      images: [],
      files: [],
      uploadImages: false,
    })
  }
  handleInputChange = event => {
    if (event !== undefined) {
      const target = event.target
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name
      this.setState({
        [name]: value,
      })
    }
  }

  submitReview = (e) => {
    e.preventDefault();

    this.setState({ loading: true })
    const payload = MapAddReviewFromState(this.state, this.props.listingId, this.props.userId)
    axios.post(POST_add_review, payload)
      .then(res => {
        const reviewId = res.data.id;
        const newReview = res.data;
        console.log("RESPONSE ", reviewId);
        if (this.state.uploadImages === true) {
          console.log("Uploading images...")
          const imagePayload = MapAddImageFromState(this.state.files);
          axios.post(`${POST_add_review_image(this.props.listingId, reviewId)}`, imagePayload, { headers: { 'Content-Type': 'multipart/form-data' } })
            .then(res => {
              this.setState({ loading: false, uploadImages: false })
              const newReviewWithImages = { ...newReview, images: res.data }
              this.props.UpdateReviewsFn(newReviewWithImages)
              this.setDefaultState();
            });

        } else {
          this.props.UpdateReviewsFn(newReview);
          this.setDefaultState();
        }
      }).catch(error => {
        console.log(`[ERROR] Adding review, error: `, error)

      })
    // }
  }

  handleFileUpload = event => {

    const files = event.target.files;
    const filesArr = Array.prototype.slice.call(files);
    console.log("files", this.state)
    this.setState({
      ...this.state,
      files: [...this.state.files, ...filesArr],
      uploadImages: true
    });
    console.log("STATE HERE AFTER!!", this.state)
  }

  handleFileRemoval = (fileName) => {
    this.setState({ listing: { files: this.state.files.filter(x => x.name !== fileName) } });
  }


  render() {

    const {
      valueForMoney,
      service,
      quality,
      availability,
      reviewText,
      name,
      email,
      images: []
    } = this.state


    // const AddReview = props => {
    return (
      <>
        {/* <!-- Add Review Box --> */}
        < div id="add-review" className="add-review-box" >
          <form onSubmit={this.submitReview}>
            {/* <!-- Add Review --> */}
            < h3 className="listing-desc-headline margin-bottom-10" > Add Review</h3 >
            <p className="comment-notes">
              Your email address will not be published.
        </p>

            {/* <!-- Subratings Container --> */}
            <div className="sub-ratings-container">
              {/* <!-- Subrating #1 --> */}
              <div className="add-sub-rating">
                <div className="sub-rating-title">
                  Service{" "}
                  <i
                    className="tip"
                    data-tip-content="Quality of customer service and attitude to work with you"
                  ></i>
                </div>
                <div className="sub-rating-stars">
                  {/* <!-- Leave Rating --> */}
                  <div className="clearfix"></div>
                  <form className="leave-rating">
                    <input type="radio" onChange={this.handleInputChange} name="service" id="rating-1" value="5" />
                    <label for="rating-1" className="fa fa-star"></label>
                    <input type="radio" onChange={this.handleInputChange} name="service" id="rating-2" value="4" />
                    <label for="rating-2" className="fa fa-star"></label>
                    <input type="radio" onChange={this.handleInputChange} name="service" id="rating-3" value="3" />
                    <label for="rating-3" className="fa fa-star"></label>
                    <input type="radio" onChange={this.handleInputChange} name="service" id="rating-4" value="2" />
                    <label for="rating-4" className="fa fa-star"></label>
                    <input type="radio" onChange={this.handleInputChange} name="service" id="rating-5" value="1" />
                    <label for="rating-5" className="fa fa-star"></label>
                  </form>
                </div>
              </div>

              {/* <!-- Subrating #2 --> */}
              <div className="add-sub-rating">
                <div className="sub-rating-title">
                  Value for Money{" "}
                  <i
                    className="tip"
                    data-tip-content="Overall experience received for the amount spent"
                  ></i>
                </div>
                <div className="sub-rating-stars">
                  {/* <!-- Leave Rating --> */}
                  <div className="clearfix"></div>
                  <form className="leave-rating">
                    <input type="radio" onChange={this.handleInputChange} name="valueForMoney" id="rating-11" value="5" />
                    <label for="rating-11" className="fa fa-star"></label>
                    <input type="radio" onChange={this.handleInputChange} name="valueForMoney" id="rating-12" value="4" />
                    <label for="rating-12" className="fa fa-star"></label>
                    <input type="radio" onChange={this.handleInputChange} name="valueForMoney" id="rating-13" value="3" />
                    <label for="rating-13" className="fa fa-star"></label>
                    <input type="radio" onChange={this.handleInputChange} name="valueForMoney" id="rating-14" value="2" />
                    <label for="rating-14" className="fa fa-star"></label>
                    <input type="radio" onChange={this.handleInputChange} name="valueForMoney" id="rating-15" value="1" />
                    <label for="rating-15" className="fa fa-star"></label>
                  </form>
                </div>
              </div>

              {/* <!-- Subrating #3 --> */}
              <div className="add-sub-rating">
                <div className="sub-rating-title">
                  Quality{" "}
                  <i
                    className="tip"
                    data-tip-content="Visibility, commute or nearby parking spots"
                  ></i>
                </div>
                <div className="sub-rating-stars">
                  {/* <!-- Leave Rating --> */}
                  <div className="clearfix"></div>
                  <form className="leave-rating">
                    <input type="radio" onChange={this.handleInputChange} name="quality" id="rating-21" value="5" />
                    <label for="rating-21" className="fa fa-star"></label>
                    <input type="radio" onChange={this.handleInputChange} name="quality" name="rating" id="rating-22" value="4" />
                    <label for="rating-22" className="fa fa-star"></label>
                    <input type="radio" onChange={this.handleInputChange} name="quality" name="rating" id="rating-23" value="3" />
                    <label for="rating-23" className="fa fa-star"></label>
                    <input type="radio" onChange={this.handleInputChange} name="quality" name="rating" id="rating-24" value="2" />
                    <label for="rating-24" className="fa fa-star"></label>
                    <input type="radio" onChange={this.handleInputChange} name="quality" name="rating" id="rating-25" value="1" />
                    <label for="rating-25" className="fa fa-star"></label>
                  </form>
                </div>
              </div>

              {/* <!-- Subrating #4 --> */}
              <div className="add-sub-rating">
                <div className="sub-rating-title">
                  Availability{" "}
                  <i
                    className="tip"
                    data-tip-content="The physical condition of the business"
                  ></i>
                </div>
                <div className="sub-rating-stars">
                  {/* <!-- Leave Rating --> */}
                  <div className="clearfix"></div>
                  <form className="leave-rating">
                    <input type="radio" onChange={this.handleInputChange} name="availability" id="rating-31" value="5" />
                    <label for="rating-31" className="fa fa-star"></label>
                    <input type="radio" onChange={this.handleInputChange} name="availability" id="rating-32" value="4" />
                    <label for="rating-32" className="fa fa-star"></label>
                    <input type="radio" onChange={this.handleInputChange} name="availability" id="rating-33" value="3" />
                    <label for="rating-33" className="fa fa-star"></label>
                    <input type="radio" onChange={this.handleInputChange} name="availability" id="rating-34" value="2" />
                    <label for="rating-34" className="fa fa-star"></label>
                    <input type="radio" onChange={this.handleInputChange} name="availability" id="rating-35" value="1" />
                    <label for="rating-35" className="fa fa-star"></label>
                  </form>
                </div>
              </div>

              {/* <!-- Uplaod Photos --> */}
              <div className="uploadButton margin-top-15">
                {/* {/* <label className="uploadButton-button ripple-effect" for="upload"> */}
                <FileUpload
                  images={this.state.images === undefined ? [] : this.state.images}
                  files={this.state.files === undefined ? [] : this.state.files}
                  handleFileUpload={this.handleFileUpload}
                  handleFileRemoval={this.handleFileRemoval}
                  onDrop={this.onDrop}
                  cssClass="uploadButton-button"
                  buttonText=""
                  iconClass=""
                  previewWidth="80px"
                />
                {/* <label className="uploadButton-button ripple-effect" for="upload">

              </label> */}
                <span className="uploadButton-file-name"></span>
              </div>
              {/* <!-- Uplaod Photos / End --> */}
            </div>
            {/* <!-- Subratings Container / End --> */}

            {/* <!-- Review Comment --> */}
            <form id="add-comment" className="add-comment">
              <fieldset>
                <div className="row">
                  <div className="col-md-6">
                    <label>Name:</label>
                    <input type="text" onChange={this.handleInputChange} name="name" value={name} />
                  </div>

                  <div className="col-md-6">
                    <label>Email:</label>
                    <input type="text" onChange={this.handleInputChange} name="email" value={email} />
                  </div>
                </div>

                <div>
                  <label>Review:</label>
                  <textarea cols="40" rows="3" name="reviewText" value={reviewText} onChange={this.handleInputChange} ></textarea>
                </div>
              </fieldset>

              <input type="submit" className="button" value="Submit Review" />
              <div className="clearfix"></div>
            </form>
          </form>
          <Loader showLoader={this.state.loading} />
        </div >
        {/* <!-- Add Review Box / End --> */}
      </>
    )

  }
}
AddReview.propTypes = {}

export default AddReview
