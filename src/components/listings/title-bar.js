import React from "react"
import PropTypes from "prop-types"

const TitleBar = props => {
  const { title, description } = props
  return (
    <>
      <div id="titlebar" class="gradient">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h2>{title}</h2>
              <span>{description}</span>

              <nav id="breadcrumbs">
                <ul>
                  <li>
                    <a href="#">Home</a>
                  </li>
                  <li>Listings</li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

TitleBar.propTypes = {}

export default TitleBar
