
export const SearchFilterTypes = Object.freeze({
    LOCATION: "LOCATION",
    OTHER: "OTHER",
    CATEGORIES: "CATEGORIES",
    NONE: "NONE",
    DATE: "DATE"
});

export const OtherFilterTypes = Object.freeze({
    DELIVERY: "DELIVERY",
    COLLECTION: "COLLECTION",
    VERIFIED_USER: "VERIFIED_USER",
    HIGH_RATING: "HIGH_RATING"

})

export const LocationFilterTypes = Object.freeze({
    FIVEKM: "FIVEKM",
    FIFTEENKM: "FIFTEENKM",
    TWENTYKM: "TWENTYKM",
    FIFTYKM: "FIFTYKM"
})
