import React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"

const ListItem = props => {
  const {
    key,
    id,
    title,
    location,
    image,
    category,
    rating,
    reviewCount,
    badgeText,
    likeButtonFn,
  } = props
  console.log("ITEM props", props)
  const showBadge = badgeText === ""

  return (
    <>
      <div id={key} class="col-lg-12 col-md-12">
        <div class="listing-item-container list-layout">
          <Link
            to={`/detail?id=${id}`}
            state={{ id: id }}
            className={"listing-item"}
          >
            {/* <a href={`/detail/${id}`} class="listing-item"> */}
            <div class="listing-item-image">
              <img src={image} alt="" />
              <span class="tag">{category === null ? "" : category.name}</span>
            </div>

            <div class="listing-item-content">
              {showBadge === true && (
                <div class="listing-badge now-open">{badgeText}</div>
              )}

              <div class="listing-item-inner">
                <h3>
                  {title} <i class="verified-icon"></i>
                </h3>
                <span>{location}</span>
                <div class="star-rating" data-rating={rating}>
                  <div class="rating-counter">({reviewCount} reviews)</div>
                </div>
              </div>

              <span class="like-icon" onClick={() => likeButtonFn()}></span>
            </div>
            {/* </a> */}

          </Link>
        </div>
      </div>
    </>
  )
}

ListItem.propTypes = {
  title: PropTypes.string,
  location: PropTypes.string,
  image: PropTypes.string,
  category: PropTypes.string,
  rating: PropTypes.string,
  reviewCount: PropTypes.string,
  badgeText: PropTypes.string,
  likeButtonFn: PropTypes.func,
}

export default ListItem
