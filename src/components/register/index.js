import React, { Component } from "react"
import axios from "axios"
import { POST_complete_register } from "../../api/endpoints.js";
import header from "../../images/header.png"
import register from "../../images/svgs/undraw_address_udes.svg"
import otp from "../../images/svgs/undraw_people_search_wctu.svg"

export default class RegisterMarkup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // cellphone: "",
            email: "",
            firstName: "",
            surname: "",
            idNumber: "",
            city: "",
            suburb: "",
            lastName: "",
            password: "",
            bio: "",
            address: "",
            facebook: "",
            google: "",
            twitter: "",
            isGMUser: ""
        };
        this.register = this.register.bind(this);
    }


    register(e) {
        e.preventDefault();
        const data = {
            email: this.state.email,
            cellphone: this.state.cellphone,
            firstName: this.state.firstname,
            surname: this.state.surname,
            idNumber: this.state.idNumber,
            city: this.state.city,
            suburb: this.state.suburb,
            lastName: this.state.surname,
            password: this.state.password,
            bio: this.state.bio,
            address: this.state.address,
            facebook: this.state.fb,
            google: this.state.google,
            twitter: this.state.twitter,
            isGMUser: this.props.isGMUser
        };
        const config = {
            headers: { 'Access-Control-Allow-Origin': '*' }
        };
        axios.post(POST_complete_register, data, config)
            .then(response => {

                this.setState({
                    promptText: "Registration complete, redirecting..."
                });
            })
    }

    extractParametersFromSearch(search) {
        return search.split('=')[1];
    }

    componentDidMount() {
        const { location } = this.props;

    }

    handleInputChange = event => {
        if (event !== undefined) {
            const target = event.target;
            const value = target.type === 'checkbox' ? target.checked : target.value;
            const name = target.name;
            this.setState({
                [name]: value,
            })
        }
    };

    render() {
        const { isGMUser, firstName } = this.props;
        return (
            <div id="sign-in-dialog" class="zoom-anim-dialog">
                <div className="small-dialog-header">
                    <h3>Register</h3>
                </div>

                {/* <!--Tabs --> */}
                <div className="sign-in-form style-1">

                    <div className="tab-container alt">
                        {/* <!-- Login --> */}


                        <div className="enabled" id="">
                            <div className="register">
                                <p className="form-row form-row-wide">
                                    <label for="firstname">First name:
                                        <i className="im im-icon-User"></i>
                                        <input type="text" className="input-text"
                                            name="firstname" id="firstname"
                                            value={(isGMUser !== true || firstName === undefined) ? this.state.firstname : this.props.firstName}
                                            onChange={this.handleInputChange} />
                                    </label>
                                </p>

                                <p className="form-row form-row-wide">
                                    <label htmlFor="surname">Surname:
                                        <i className="im im-icon-User"></i>
                                        <input type="text" className="input-text"
                                            name="surname" id="surname"
                                            value={this.state.surname}
                                            onChange={this.handleInputChange} />
                                    </label>
                                </p>
                                <p className="form-row form-row-wide">
                                    <label htmlFor="cellphone">Cellphone:
                                        <i className="im im-icon-Phone"></i>
                                        <input type="text" className="input-text" name="cellphone" id="cellphone"
                                            value="" value={this.state.cellphone} onChange={this.handleInputChange} />
                                    </label>
                                </p>
                                <p className="form-row form-row-wide">
                                    <label htmlFor="email">Email:
                                        <i className="im im-icon-Mail"></i>
                                        <input type="text" className="input-text" name="email" id="email"
                                            value="" value={this.state.cellphone} onChange={this.handleInputChange} />
                                    </label>
                                </p>
                                <p className="form-row form-row-wide">
                                    <label htmlFor="surname">SA Identity Number:
                                        <i className="im im-icon-Identification-Badge"></i>
                                        <input type="text" className="input-text"
                                            name="surname" id="surname"
                                            value={this.state.surname}
                                            onChange={this.handleInputChange} />
                                    </label>
                                </p>

                                <p className="form-row form-row-wide">
                                    <label htmlFor="surname">City:
                                        <i className="im im-icon-Location-2"></i>
                                        <input type="text" className="input-text"
                                            name="city" id="city"
                                            value={this.state.surname}
                                            onChange={this.handleInputChange} />
                                    </label>
                                </p>

                                <p className="form-row form-row-wide">
                                    <label htmlFor="surname">Closest Suburb:
                                        <i className="im im-icon-Home"></i>
                                        <input type="text" className="input-text"
                                            name="suburb" id="suburb"
                                            value={this.state.surname}
                                            onChange={this.handleInputChange} />
                                    </label>
                                </p>
                                {/* 
                                <p className="form-row form-row-wide">
                                    <label htmlFor="surname">
                                        <i className="im im-icon-Phone"></i>
                                        <input type="text" className="input-text"
                                            name="suburb" id="suburb"
                                            value={this.state.surname}
                                            onChange={this.handleInputChange} />
                                    </label>
                                </p> */}

                                <p className="form-row form-row-wide">
                                    <div className="form-row">
                                        <input type="submit"
                                            className="button border margin-top-5"
                                            name="registers"
                                            onClick={this.register}
                                            value="Register" />
                                    </div>
                                </p>
                            </div>
                            <img src={register} />
                        </div>
                        {/* 
                        <div className="enabled" id="tab5">
                            <form className="register">

                                <h4>
                                    Thank you {this.state.firstname} you are one step closer to floating items.<br /><br />
                                    We will be in touch soon.
                                </h4>


                            </form>
                        </div> */}

                    </div>
                </div>

            </div>

        )
    }
}
