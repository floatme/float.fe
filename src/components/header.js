import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import logo from "../images/logo-1.png"
import avatar from "../images/dashboard-avatar.jpg"
import { DashBoardPageTypeEnum } from "./admin/dashboard/enums/DashboardPageTypeEnum"
import { GetUserInfo } from "../components/login/lib/cookies"
import { Modal, Button } from 'antd';
import { LoginStatusEnum } from "./login/enums/LoginStatusEnum";
import Helmet from "react-helmet"

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: "none",
            menuOpen: false
        };
        this.showModal = this.showModal.bind(this);
    }

    cookieUserInfo = GetUserInfo();

    showModal() {
        console.log('Show modal');
        this.setState({
            modalVisible: "block",
        });
    };

    handleOk = e => {
        console.log(e);
        this.setState({
            modalVisible: "none",
        });
    };

    handleCancel = e => {
        console.log(e);
        this.setState({
            modalVisible: "none",
        });
    };

    toggleMobileMenu() {
        if (this.state.menuOpen === true) {
            this.setState({
                menuOpen: false,
            });
        } else {
            this.setState({
                menuOpen: true,
            });
        }
    }

    render() {
        const { headerContainerClass, headerClass, displaySignUp, logoutFn, userInfo } = this.props;
        return (
            <>
                {this.state.menuOpen === true ?
                    <Helmet>
                        <html className="touch mm-opened mm-blocking mm-background mm-opening"></html>
                    </Helmet> : ""}

                <div className="tab-content" id="tab2" style={{ display: this.state.modalVisible }}>

                    <form method="post" className="register">

                        <p className="form-row form-row-wide">
                            <label htmlFor="username2">Username:
                                <i className="im im-icon-Male"></i>
                                <input type="text" className="input-text" name="username" id="username2" value="" />
                            </label>
                        </p>

                        <p className="form-row form-row-wide">
                            <label htmlFor="email2">Email Address:
                                <i className="im im-icon-Mail"></i>
                                <input type="text" className="input-text" name="email" id="email2" value="" />
                            </label>
                        </p>

                        <p className="form-row form-row-wide">
                            <label htmlFor="password1">Password:
                                <i className="im im-icon-Lock-2"></i>
                                <input className="input-text" type="password" name="password1" id="password1" />
                            </label>
                        </p>

                        <p className="form-row form-row-wide">
                            <label htmlFor="password2">Repeat Password:
                                <i className="im im-icon-Lock-2"></i>
                                <input className="input-text" type="password" name="password2" id="password2" />
                            </label>
                        </p>

                        <input type="submit" className="button border fw margin-top-10" name="register"
                            value="Register" />

                    </form>
                </div>

                <header id="header-container" className={headerContainerClass}>

                    {/* <!-- Header --> */}
                    <div id="header" className={headerClass}>
                        <div className="container">

                            {/* <!-- Left Side Content --> */}
                            <div className="left-side">

                                {/* <!-- Logo --> */}
                                <div id="logo">
                                    <a href="/">
                                        <img src={logo} //data-sticky-logo="images/logo-2.png"
                                            alt="Float" />
                                    </a>
                                </div>

                                {/* <!-- Mobile Navigation --> */}
                                <div className="mmenu-trigger">
                                    <button onClick={() => this.toggleMobileMenu()} className="hamburger hamburger--collapse" type="button">
                                        <span className="hamburger-box">
                                            <span className="hamburger-inner"></span>
                                        </span>
                                    </button>
                                </div>

                                {/* <!-- Main Navigation --> */}
                                {this.state.menuOpen === false ? (
                                    <nav id="navigation" className="style-1">
                                        <ul id="responsive">

                                            <li><a className="current" href="../">Home</a></li>

                                            <li><a href="/listings">Listings</a></li>
                                            <li><a href={`/user?userId=${userInfo.userId}`}>Profile</a></li>
                                            <li><a
                                                href={`/admin/dashboard/?page=${DashBoardPageTypeEnum.HOME}`}>Dashboard</a>
                                            </li>


                                        </ul>
                                    </nav>
                                ) :
                                    <nav style={{ "z-index": "9999" }} className="mmenu-init mm-menu mm-offcanvas mm-opened" id="mm-0">
                                        <div className="mm-panels">
                                            <ul className="mm-listview"><li>
                                                <a className="current" href="../">Home</a></li>
                                                <li><a href="/listings">Listings</a></li>
                                                <li><a href={`/user?userId=${userInfo.userId}`}>Profile</a></li>
                                                <li><a href={`/admin/dashboard/?page=${DashBoardPageTypeEnum.HOME}`}>Dashboard</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </nav>
                                }

                                <div className="clearfix"></div>
                                {/* <!-- Main Navigation / End --> */}

                            </div>
                            {/* <!-- Left Side Content / End --> */}
                            {/* <!-- Right Side Content / End --> */}
                            <div className="right-side">
                                <div className="header-widget">
                                    {/* <!-- User Menu --> */}
                                    {this.cookieUserInfo.status === LoginStatusEnum.NONE
                                        ? <a onClick={() => this.showModal}
                                            className="sign-in popup-with-zoom-anim"
                                            style={{ cursor: 'pointer' }}>
                                            <i className="sl sl-icon-login" /> Sign In</a>
                                        : (
                                            <div className="user-menu">
                                                <div className="user-name"><span><img
                                                    src={this.cookieUserInfo.profilePicUrl === undefined ? userInfo.profilePicUrl : this.cookieUserInfo.profilePicUrl}
                                                    alt="" /></span>Hi, {this.cookieUserInfo.firstName === undefined ? "" : this.cookieUserInfo.firstName}!
                                                </div>
                                                <ul>
                                                    <li><a href="/admin/dashboard">
                                                        <i className="sl sl-icon-settings"></i> Dashboard</a>
                                                    </li>
                                                    <li><a href="/admin/messages">
                                                        <i className="sl sl-icon-envelope-open"></i> Messages</a></li>
                                                    <li><a href="/admin/bookings">
                                                        <i className="fa fa-calendar-check-o"></i> Bookings</a></li>
                                                    <li><a onClick={() => {
                                                        logoutFn()
                                                    }}><i className="sl sl-icon-power"></i> Logout</a></li>
                                                </ul>
                                            </div>
                                        )
                                    }

                                    {/* <a href="/dashboard/add" className="button border with-icon"></a> */}
                                </div>
                            </div>
                            {/* <!-- Right Side Content / End --> */}

                            {displaySignUp === "true" && (
                                <>

                                    {/* <!-- Sign In Popup --> */}
                                    <div id="sign-in-dialog" className="zoom-anim-dialog mfp-hide">

                                        <div className="small-dialog-header">
                                            <h3>Sign In</h3>
                                        </div>

                                        {/* <!--Tabs --> */}
                                        <div className="sign-in-form style-1">

                                            <ul className="tabs-nav">
                                                <li className=""><a href="#tab1">Log In</a></li>
                                                <li><a href="#tab2">Register</a></li>
                                            </ul>

                                            <div className="tabs-container alt">

                                                {/* <!-- Login --> */}
                                                <div className="tab-content" id="tab1" style={{ display: "none" }}>
                                                    <form method="post" className="login">

                                                        <p className="form-row form-row-wide">
                                                            <label for="username">Username:
                                                                <i className="im im-icon-Male"></i>
                                                                <input type="text" className="input-text"
                                                                    name="username"
                                                                    id="username" value="" />
                                                            </label>
                                                        </p>

                                                        <p className="form-row form-row-wide">
                                                            <label for="password">Password:
                                                                <i className="im im-icon-Lock-2"></i>
                                                                <input className="input-text" type="password"
                                                                    name="password" id="password" />
                                                            </label>
                                                            <span className="lost_password">
                                                                <a href="#">Lost Your Password?</a>
                                                            </span>
                                                        </p>

                                                        <div className="form-row">
                                                            <input type="submit" className="button border margin-top-5"
                                                                name="login" value="Login" />
                                                            <div className="checkboxes margin-top-10">
                                                                <input id="remember-me" type="checkbox" name="check" />
                                                                <label for="remember-me">Remember Me</label>
                                                            </div>
                                                        </div>

                                                    </form>
                                                </div>

                                                {/* <!-- Register --> */}
                                                <div className="tab-content" id="tab2" style={{ display: "none" }}>

                                                    <form method="post" className="register">

                                                        <p className="form-row form-row-wide">
                                                            <label for="username2">Username:
                                                                <i className="im im-icon-Male"></i>
                                                                <input type="text" className="input-text"
                                                                    name="username"
                                                                    id="username2" value="" />
                                                            </label>
                                                        </p>

                                                        <p className="form-row form-row-wide">
                                                            <label for="email2">Email Address:
                                                                <i className="im im-icon-Mail"></i>
                                                                <input type="text" className="input-text" name="email"
                                                                    id="email2" value="" />
                                                            </label>
                                                        </p>

                                                        <p className="form-row form-row-wide">
                                                            <label for="password1">Password:
                                                                <i className="im im-icon-Lock-2"></i>
                                                                <input className="input-text" type="password"
                                                                    name="password1" id="password1" />
                                                            </label>
                                                        </p>

                                                        <p className="form-row form-row-wide">
                                                            <label for="password2">Repeat Password:
                                                                <i className="im im-icon-Lock-2"></i>
                                                                <input className="input-text" type="password"
                                                                    name="password2" id="password2" />
                                                            </label>
                                                        </p>

                                                        <input type="submit" className="button border fw margin-top-10"
                                                            name="register" value="Register" />

                                                    </form>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    {/* <!-- Sign In Popup / End --> */}
                                </>
                            )}

                        </div>
                    </div>
                    {/* <!-- Header / End --> */}

                </header>
                <div class="clearfix"></div>
            </>
        )
    }
}

Header.propTypes = {
    siteTitle: PropTypes.string,
};

Header.defaultProps = {
    siteTitle: ``,
};

export default Header
