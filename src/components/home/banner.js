import React from "react"
import { Link } from "gatsby"
import MainSearch from "./main-search.js"
import axios from "axios";
import { GET_FEATURED_category } from "../../api/endpoints";
import header from "../../images/header.png"

let backgroundHeader = {
    backgroundImage: `url(${header})`
}

export default class Banner extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: []
        };
        this.GetCategories = this.GetCategories.bind(this);
    }

    componentDidMount() {
        this.GetCategories();
    }

    GetCategories() {
        axios.get(GET_FEATURED_category).then(response => {
            this.setState({ categories: response.data })
        });
    };

    render() {
        return (
            <>
                <div
                    className="main-search-container centered"
                    style={backgroundHeader}
                >
                    <div className="main-search-inner">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12">
                                    <h2>
                                        Rent stuff:
                                        {/* <!-- Typed words can be configured in script settings at the bottom of this HTML file --> */}
                                        <span className="typed-words"></span>
                                    </h2>
                                    <h4>Rent stuff you need, only when you need them</h4>
                                    <MainSearch css="main-search-input" redirectOnSearch={true} />
                                </div>
                            </div>

                            {/* <!-- Features Categories --> */}
                            <div className="row">
                                <div className="col-md-12">
                                    <h5 className="highlighted-categories-headline">
                                        Or browse featured categories:
                                    </h5>

                                    <div className="highlighted-categories">
                                        {this.state.categories.map(cat => {
                                            return <a
                                                href={"/listings/" + cat.id}
                                                className="highlighted-category"
                                            >
                                                <i className="im im-icon-Camera"></i>
                                                <h4>{cat.name}</h4>
                                            </a>
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
