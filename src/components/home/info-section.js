import React from "react"
import PropTypes from "prop-types"

const InfoSection = () => {
  return (
    <>
      <section
        className="fullwidth padding-top-75 padding-bottom-70"
        data-background-color="#f9f9f9"
      >
        <div className="container">
          <div className="row">
            <div className="col-md-8 col-md-offset-2">
              <h3 className="headline centered headline-extra-spacing">
                <strong className="headline-with-separator">
                  How does it work?{" "}
                </strong>
                <span className="margin-top-25">

                </span>
              </h3>
            </div>
          </div>

          <div className="row icons-container">
            {/* <!-- Stage --> */}
            <div className="col-md-4">
              <div className="icon-box-2 with-line">
                <i className="im im-icon-Map2"></i>
                <h3>Search for an item</h3>
                <p>
                  Search for the item you want to rent, select the one you want,
                  book it and pay for it and voila! It is yours to use.
                </p>
              </div>
            </div>

            {/* <!-- Stage --> */}
            <div className="col-md-4">
              <div className="icon-box-2">
                <i className="im im-icon-Mail-withAtSign"></i>

                <h3>Book item online</h3>
                <p>
                  Book the item you like, with your desired duration and
                  delivery options, seal your booking by paying online.
                </p>
              </div>
            </div>

            {/* <!-- Stage --> */}
            <div className="col-md-4">
              <div className="icon-box-2 with-line">
                <i className="im im-icon-Checked-User"></i>
                <h3>Connect with owner</h3>
                <p>
                  Connect with the owner of the item to arrange delivery and or
                  collection of the item.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

InfoSection.propTypes = {}

export default InfoSection
