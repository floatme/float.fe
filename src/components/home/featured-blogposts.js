import React from "react"
import PropTypes from "prop-types"

const FeaturedBlogPosts = () => {
  return (
    <>
      <section
        className="fullwidth margin-top-0 padding-top-75 padding-bottom-75"
        data-background-color="#fff"
      >
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h3 className="headline centered margin-bottom-55">
                <strong className="headline-with-separator">
                  From The Community
                </strong>
              </h3>
            </div>
          </div>

          <div className="row">
            {/* <!-- Blog Post Item --> */}
            <div className="col-md-4">
              <a
                href="pages-blog-post.html"
                className="blog-compact-item-container"
              >
                <div className="blog-compact-item">
                  <img src="images/blog-compact-post-01.jpg" alt="" />
                  <span className="blog-item-tag">Tips</span>
                  <div className="blog-compact-item-content">
                    <ul className="blog-post-tags">
                      <li>22 August 2019</li>
                    </ul>
                    <h3>Cameras for All Budgets</h3>
                    <p>
                      Sed sed tristique nibh iam porta volutpat finibus. Donec
                      in aliquet urneget mattis lorem. Pellentesque
                      pellentesque.
                    </p>
                  </div>
                </div>
              </a>
            </div>
            {/* <!-- Blog post Item / End --> */}

            {/* <!-- Blog Post Item --> */}
            <div className="col-md-4">
              <a
                href="pages-blog-post.html"
                className="blog-compact-item-container"
              >
                <div className="blog-compact-item">
                  <img src="images/blog-compact-post-02.jpg" alt="" />
                  <span className="blog-item-tag">Tips</span>
                  <div className="blog-compact-item-content">
                    <ul className="blog-post-tags">
                      <li>18 August 2019</li>
                    </ul>
                    <h3>Student hacks to saving money</h3>
                    <p>
                      Sed sed tristique nibh iam porta volutpat finibus. Donec
                      in aliquet urneget mattis lorem. Pellentesque
                      pellentesque.
                    </p>
                  </div>
                </div>
              </a>
            </div>
            {/* <!-- Blog post Item / End --> */}

            {/* <!-- Blog Post Item --> */}
            <div className="col-md-4">
              <a
                href="pages-blog-post.html"
                className="blog-compact-item-container"
              >
                <div className="blog-compact-item">
                  <img src="images/blog-compact-post-03.jpg" alt="" />
                  <span className="blog-item-tag">Tips</span>
                  <div className="blog-compact-item-content">
                    <ul className="blog-post-tags">
                      <li>10 August 2019</li>
                    </ul>
                    <h3>Make money from renting stuff</h3>
                    <p>
                      Sed sed tristique nibh iam porta volutpat finibus. Donec
                      in aliquet urneget mattis lorem. Pellentesque
                      pellentesque.
                    </p>
                  </div>
                </div>
              </a>
            </div>
            {/* <!-- Blog post Item / End --> */}

            <div className="col-md-12 centered-content">
              {/*<a href="pages-blog.html" className="button border margin-top-10">
                View Blog
              </a>*/}
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

FeaturedBlogPosts.propTypes = {}

export default FeaturedBlogPosts
