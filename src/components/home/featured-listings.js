import React from "react";
import axios from "axios";
import { GET_top_listings } from "../../api/endpoints.js";
import { Carousel } from 'antd';
export default class FeaturedListings extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      featured: []
    };
  }

  componentDidMount() {
    axios.get(GET_top_listings).then(featured_response => {
      this.setState({ featured: featured_response.data });
    })
  }

  render() {
    return (
      <>
        <section
          className="fullwidth margin-top-65 padding-top-75 padding-bottom-70"
          data-background-color="#f8f8f8"
        >
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <h3 className="headline centered margin-bottom-45">
                  <strong className="headline-with-separator">
                    Most Rented Items
                    </strong>
                  <span>Discover Top-Rated Rented Items</span>
                </h3>
              </div>

              <div className="col-md-12">
                <div className="">
                  <Carousel autoplay>
                    {/* <!-- Listing Item --> */}
                    {
                      this.state.featured.map(item => {
                        return <div className="carousel-item">
                          <a
                            href="listings-single-page.html"
                            className="listing-item-container"
                          >
                            <div className="listing-item">
                              <img src={item.mainImage} alt="" />

                              <div className="listing-badge now-open">
                                {item.promoText}
                              </div>

                              <div className="listing-item-content">
                                <span className="tag">{item.category.name}</span>
                                <h3>
                                  {item.title} <i className="verified-icon"></i>
                                </h3>
                                <span>R{item.pricePerDay} per day</span>
                              </div>
                              <span className="like-icon"></span>
                            </div>
                            <div className="star-rating" data-rating="4.5">
                              <div className="rating-counter">({item.reviewCount} reviews)</div>
                            </div>
                          </a>
                        </div>
                      })
                    }
                  </Carousel>
                </div>
              </div>
            </div>
          </div>
        </section>
      </>
    )
  }
}

