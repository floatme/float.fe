import React from "react"
import axios from "axios";
import { GET_category } from "../../api/endpoints";
import { _allCategories } from "../../mocks"
import LocationSearchInput from "../shared/location-search-input";
import { Link } from "gatsby";
import { AutoComplete } from "antd";
import Autocomplete from "react-autocomplete";
import AutoCompleteContainer from "../shared/autocomplete";
import { _suburbs, _cities } from "../../mocks";

export default class MainSearch extends React.Component {

    //const {css, onClickFn} = props
    constructor(props) {
        super(props);
        this.state = {
            categories: _allCategories,
            category: "",
            term: "",
            location: ""
        };
        // this.GetCategories = this.GetCategories.bind(this);
        this.handleAutoCompleteChange = this.handleAutoCompleteChange.bind(this);
    }


    componentDidMount() {
        //  this.GetCategories();
    }

    // GetCategories() {
    //     axios.get(GET_category).then(response => {
    //         this.setState({ categories: response.data })
    //     });
    // }

    handleInputChange = event => {
        if (event !== undefined) {
            const target = event.target
            const value = target.type === 'checkbox' ? target.checked : target.value;
            const name = target.name
            this.setState({
                [name]: value
            })

        }

        // this.props.updateParent(this.state.term, this.state.location, this.state.category)
    }

    handleAutoCompleteChange(value) {
        console.log("Updating location to ", value)
        this.setState({
            location: value
        })
    }


    render() {
        const suburbs = _suburbs.map(suburb => { return suburb.suburb });

        const allLocations = [...suburbs, ..._cities]
        console.log("Current state", this.state)
        return (
            <div className={this.props.css}>
                <div className="main-search-input-item">
                    <input type="text" onChange={this.handleInputChange} name="term" placeholder="What are you looking for?" value={this.state.term} />
                </div>
                <div className="main-search-input-item location">
                    <div id="autocomplete-container">

                        <AutoCompleteContainer
                            value={this.state.location}
                            items={allLocations}
                            onChangeFn={this.handleAutoCompleteChange}
                        />
                        {/* <input id="autocomplete-input" type="text" onChange={this.handleInputChange} name="location" placeholder="Location" /> */}
                        {/* <LocationSearchInput value={this.state.term} inputCss="" geoCode={false} changeLocation={this.changeLocation} /> */}
                    </div>
                    <a href="#">
                        <i className="fa fa-map-marker"></i>
                    </a>
                </div>
                <div className="main-search-input-item">
                    <select data-placeholder="All Categories" className="chosen-select" onChange={this.handleInputChange} name="category">
                        <option>All Categories</option>
                        {/* {this.state.categories.map(category => { */}
                        {_allCategories.map(category => {
                            return <option value={category.id}>{category.name}</option>
                        })}
                    </select>
                </div>
                {this.props.redirectOnSearch === false ?
                    <button className="button"
                        onClick={this.props.searchFn === undefined ?
                            () => { console.log("Validation error on search.") } :
                            () => this.props.searchFn(this.state.term, this.state.location, this.state.category)}>Search</button>
                    :
                    <Link
                        className="button"
                        to={`/listings/`}
                        state={{ "initSearch": { term: this.state.term, location: this.state.location, category: this.state.category } }}
                    >
                        Search
                </Link>}


            </div >
        )
    }
}
