import React from "react"
import axios from "axios";
import { GET_TOP_category } from "../../api/endpoints";
import { Link } from "gatsby";

import sports from "../../images/svgs/undraw_goal_0v5v.svg"
import electronics from "../../images/svgs/undraw_gaming_6oy3.svg"
import gardening from "../../images/svgs/undraw_for_sale_viax.svg"
import events from "../../images/svgs/undraw_events_2p66.svg"


const categoryImageArray = {
    "home-sports": sports,
    "home-electronics": electronics,
    "home-gardening": gardening,
    "home-events": events
};

const _TOP_COUNT = 6;
export default class Categories extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: []
        };
        this.GetCategories = this.GetCategories.bind(this);
    }

    componentDidMount() {
        this.GetCategories();
    }

    GetCategories() {
        axios.get(GET_TOP_category + _TOP_COUNT).then(response => {
            this.setState({ categories: response.data })
        });
    };

    render() {

        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h3 className="headline centered margin-top-75">
                            <strong className="headline-with-separator">
                                Popular Categories
                            </strong>
                        </h3>
                    </div>

                    <div className="col-md-12">
                        <div className="categories-boxes-container margin-top-5 margin-bottom-30">
                            {this.state.categories.map(cat => {
                                console.log("CATEGORY", cat)
                                const categoryCss = `category-small-box home-category`
                                return (
                                    <Link
                                        className={categoryCss}
                                        style={{ backgroundImage: `url(${categoryImageArray[cat.subCategory.icon]})` }}
                                        to={`/listings/`}
                                        state={{ "initSearch": { term: undefined, location: undefined, category: cat.subCategory.id } }}
                                    > <i className={cat.subCategory.icon}></i>
                                        <h4 className=" category-headline">{cat.subCategory.name}</h4>
                                        <span className="category-box-counter">{cat.itemCount}</span></Link>
                                )
                                // <a
                                //     href={"/listings/" + cat.id}
                                //     className="category-small-box"
                                // >
                                //     <i className={cat.icon}></i>
                                //     <h4>{cat.name}</h4>
                                //     <span className="category-box-counter">0</span>
                                // </a>
                            })}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

// <Link
//     className="button"
//     to={`/listings/`}
//     state={{ "initSearch": { term: this.state.term, location: this.state.location, category: this.state.category } }}
// >
//     Search
// </Link>



