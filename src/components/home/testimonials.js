import React from "react"
import PropTypes from "prop-types"

const Testimonials = () => {
  return (
    <>
      <section
        className="fullwidth padding-top-75 padding-bottom-30"
        data-background-color="#fff"
      >
        {/* <!-- Info Section --> */}
        <div className="container">
          <div className="row">
            <div className="col-md-8 col-md-offset-2">
              <h3 className="headline centered headline-extra-spacing">
                <strong className="headline-with-separator">
                  What Our Users Say
                </strong>
                <span className="margin-top-25">
                  We collect reviews from our floaters so you can get an honest
                  opinion of what their experience with Float is really like!
                </span>
              </h3>
            </div>
          </div>
        </div>
        {/* <!-- Info Section / End --> */}

        {/* <!-- Categories Carousel --> */}
        <div className="fullwidth-carousel-container margin-top-20 no-dots">
          <div className="testimonial-carousel testimonials">
            {/* <!-- Item --> */}
            <div className="fw-carousel-review">
              <div className="testimonial-box">
                <div className="testimonial">
                  I've listed so many of my stuff on float that I never thought
                  I'd make money from. I just love how quick and easy it is, and
                  how they make sure my stuff ends up in safe hands. This is
                  honestly the best thing after uber!{" "}
                </div>
              </div>
              <div className="testimonial-author">
                <img src="images/happy-client-01.jpg" alt="" />
                <h4>
                  Sello Kgoete <span>2nd hand Shop Owner</span>
                </h4>
              </div>
            </div>

            {/* <!-- Item --> */}
            <div className="fw-carousel-review">
              <div className="testimonial-box">
                <div className="testimonial">
                  OMG this is the best thing ever invented for my varsity
                  experience! I can now save so much of my pocket money buy
                  renting stuff I need on demand instead of buying stuff.
                </div>
              </div>
              <div className="testimonial-author">
                <img src="images/happy-client-02.jpg" alt="" />
                <h4>
                  Tandeka Bheki <span>Student</span>
                </h4>
              </div>
            </div>

            {/* <!-- Item --> */}
            <div className="fw-carousel-review">
              <div className="testimonial-box">
                <div className="testimonial">
                  I usually need to create so much content for social media and
                  I love how I can now rent out photography equipments with such
                  convenience! I love it{" "}
                </div>
              </div>
              <div className="testimonial-author">
                <img src="images/happy-client-03.jpg" alt="" />
                <h4>
                  Becky Paden <span>Blogger & photographer</span>
                </h4>
              </div>
            </div>
          </div>
        </div>
        {/* <!-- Categories Carousel / End --> */}
      </section>
    </>
  )
}

Testimonials.propTypes = {}

export default Testimonials
