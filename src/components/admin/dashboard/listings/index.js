import React, { Component } from "react"
import axios from 'axios';
import { Link } from "gatsby"
import Listing from "./listing";
import { GET_user_listings } from "../../../../api/endpoints";
import { mockUserListings } from "../../../../mocks";
import { GetUserId } from "../../../../components/login/lib/cookies"
import { MapUserListingFromResponse } from "../../../../api/mapper"
import Loader from "../../../../components/shared/loader"

const useMock = false;


class Listings extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: true,
			error: false,
			listings: [],
			userId: GetUserId()
		}
	}

	componentDidMount() {
		if (useMock) {
			this.setState({
				loading: false,
				listings: mockUserListings,
			})
		} else {
			this.GetListings()
		}
	}


	GetListings() {
		this.setState({ loading: true, listings: [] })
		axios
			.get(`${GET_user_listings}${this.state.userId}`)
			.then(response => {
				console.log(`[INFO] Retrieved listings for userId: ${this.state.userId}, data: `, response.data)
				const payload = MapUserListingFromResponse(response.data)
				console.log("PAYLOAD", payload)
				this.setState({
					loading: false,
					listings: payload,
				})
			})
			.catch(error => {
				console.log(`[ERROR] Getting listings for userId: ${this.state.userId}, error: `, error)
				this.setState({ loading: false, error })
			})
	}

	render() {
		return (
			<>

				{/* <!-- Listings --> */}
				<div class="col-lg-12 col-md-12">
					<div class="dashboard-list-box margin-top-0">
						<h4>Active Listings</h4>
						<ul>

							{this.state.listings.map(listing => (
								<>
									<Listing
										key={listing.listingId}
										listingId={listing.listingId}
										status={listing.status}
										listingUrl={`/detail/?id=${listing.listingId}`}
										title={listing.title}
										description={listing.description}
										rating={listing.rating}
										reviewCount={listing.reviewCount}
										mainImageUrl={listing.mainImageUrl}
										changePageFn={this.props.changePageFn}
									/>
								</>
							))}

						</ul>
					</div>
				</div>

				<Loader showLoader={this.state.loading} />
			</>
		)
	}

}

Listings.propTypes = {}

export default Listings
