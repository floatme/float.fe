import React, { Component } from "react"
import PropTypes from "prop-types"
import axios from 'axios';

import { GET_ENABLE_listing, GET_DISABLE_listing, GET_DELETE_listing } from "../../../../api/endpoints";
import { ListingStatusEnum } from "../enums/ListingStatusEnum";
import { GetUserId, GetJwToken } from "../../../login/lib/cookies";

import { DashBoardPageTypeEnum } from "../enums/DashboardPageTypeEnum"
import { Link } from "gatsby";
import Loader from "../../../shared/loader";
import { DotLoader } from "react-spinners";



class Listing extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			newStatus: undefined
		}
	}

	componentDidMount() { }
	render() {
		const {
			status,
			listingUrl,
			title,
			description,
			rating,
			reviewCount,
			mainImageUrl,
			listingId,
			changePageFn,
		} = this.props

		const userId = GetUserId();

		const listingStatusAndEndpoints = {
			[ListingStatusEnum.ENABLED]: { endpoint: GET_ENABLE_listing(userId, listingId) },
			[ListingStatusEnum.DISABLED]: { endpoint: GET_DISABLE_listing(userId, listingId) },
			[ListingStatusEnum.DELETED]: { endpoint: GET_DELETE_listing(userId, listingId) },

		}

		const toggleListing = (listingId, status) => {

			this.setState({ loading: true })
			axios
				.get(listingStatusAndEndpoints[status].endpoint)
				.then(response => {
					if (response.data === "UN_AUTH") console.log("UN_AUTH for userId:" + userId)
					if (response.data === "INVALID") console.log("INVALID for listing:" + listingId)
					if (response.data === "BOOKING_ACTIVE") console.log("BOOKING_ACTIVE for listing:" + listingId)
					if (response.data === ListingStatusEnum.ENABLED || ListingStatusEnum.DISABLED || ListingStatusEnum.DELETED) {
						this.setState({ newStatus: response.data })
					}
					this.setState({ loading: false })
				})
				.catch(error => {
					console.log(`[ERROR] updating listingId: ${listingId} ,with status: ${status} ,  error: `, error)
					this.setState({ loading: false, error: error })
				})
		}
		console.log("status", status)
		console.log("state", this.state)

		console.log("state", this.props)

		return (
			<>

				<li className={(((this.state.newStatus === undefined && status === ListingStatusEnum.ENABLED) || this.state.newStatus === ListingStatusEnum.ENABLED) ? "" : "disabled-list-item")}>
					<div className="list-box-listing">
						<div className="list-box-listing-img"><a href="#"><img src={mainImageUrl} alt="" /></a></div>
						<div className="list-box-listing-content">
							<div className="inner">
								<h3><a href={listingUrl}>{title}</a></h3>
								<span>{description}</span>
								<div className="star-rating" data-rating={rating}>
									<div className="rating-counter">({reviewCount === "" ? "0" : reviewCount} reviews)</div>
								</div>
							</div>
						</div>
					</div>
					<div class="buttons-to-right">
						<Link
							to={`/admin/dashboard/?id=${listingId}`}
							state={{ listingId: listingId }}
							onClick={() => changePageFn(DashBoardPageTypeEnum.ADD_LISTING)}
							className={"button gray"}
						>
							<i className="sl sl-icon-note"></i> Edit
						</Link>
						{this.state.newStatus === undefined ? <>

							{status == ListingStatusEnum.ENABLED ? <>
								<a onClick={() => toggleListing(listingId, ListingStatusEnum.DISABLED)} className="button gray"><i className="sl sl-icon-close"></i> Disable</a>
							</> : ""}
							{status == ListingStatusEnum.DISABLED ? <>
								<a onClick={() => toggleListing(listingId, ListingStatusEnum.ENABLED)} className="button gray"><i className="sl sl-icon-check"></i> Enable</a>
								<a onClick={() => toggleListing(listingId, ListingStatusEnum.DELETED)} className="button gray"><i className="sl sl-icon-close"></i> Delete</a>
							</> : ""}
						</> : <>

								{this.state.newStatus == ListingStatusEnum.ENABLED ? <>
									<a onClick={() => toggleListing(listingId, ListingStatusEnum.DISABLED)} className="button gray"><i className="sl sl-icon-close"></i> Disable</a>
								</> : ""}
								{this.state.newStatus == ListingStatusEnum.DISABLED ? <>
									<a onClick={() => toggleListing(listingId, ListingStatusEnum.ENABLED)} className="button gray"><i className="sl sl-icon-check"></i> Enable</a>
									<a onClick={() => toggleListing(listingId, ListingStatusEnum.DELETED)} className="button gray"><i className="sl sl-icon-close"></i> Delete</a>
								</> : ""}
							</>
						}
					</div>
					<div className="listing-item-spinner">
						<DotLoader
							css={"override loader"}
							sizeUnit={"px"}
							size={90}
							color={'#50E3C2'}
							loading={this.state.loading} />
					</div>

				</li>

			</>
		)
	}
}

Listing.propTypes = {}

export default Listing
