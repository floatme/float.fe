import React, { Component } from "react"
import axios from 'axios';
import { Link } from "gatsby"
import Booking from "./booking";
import { GET_lender_bookings } from "../../../../api/endpoints";
import { GetUserId } from "../../../../components/login/lib/cookies"
import { mockBookings } from "../../../../mocks";
import { MapUserBookingFromResponse } from "../../../../api/mapper"
import Loader from "../../../shared/loader";
import { BookingStatusEnum } from "../enums/BookingStatusEnum";

const useMock = false;

class LenderBookings extends Component {

    state = {
        loading: true,
        error: false,
        allBookings: [],
        bookings: [],
        userId: GetUserId(),
        listings: []
    }

    componentDidMount() {
        if (useMock === true) {
            this.setState({
                loading: false,
                bookings: mockBookings,
            })
        } else {
            this.GetBookings(this.state.userId)
        }
    }
    handleFilterByStatus = event => {
        if (event !== undefined) {
            const target = event.target
            const value = target.value;
            console.log("FILTERING", target.value)
            if (value === "all") {
                const allListings = this.state.allListings;
                const allBookings = this.state.allBookings;

                console.log("FILTERING ALL", this.state)
                this.setState({
                    ...this.state,
                    loading: false,
                    bookings: allBookings,
                    listings: allListings
                });
            } else {
                const filteredBookings = this.state.allBookings.filter(booking => {
                    return booking.status === value
                });
                console.log("FILTERED BOOKINGS", filteredBookings)
                this.setState({
                    ...this.state,
                    loading: false,
                    bookings: filteredBookings,
                    //  listings: filteredListings
                });
            }
        }
    }
    handleFilterByListingId = event => {
        if (event !== undefined) {
            const target = event.target
            const value = target.value;
            console.log("FILTERING", target.value)
            if (value === "all") {
                const allListings = this.state.allListings;
                const allBookings = this.state.allBookings;

                console.log("FILTERING ALL", this.state)
                this.setState({
                    ...this.state,
                    loading: false,
                    bookings: allBookings,
                    listings: allListings
                });
            } else {
                const filteredBookings = this.state.allBookings.filter(booking => {
                    return booking.listing.title === value
                });
                console.log("FILTERED BOOKINGS", filteredBookings)
                const filteredListings = this.state.allListings.filter(listing => {
                    return listing.title !== value
                });

                this.setState({
                    ...this.state,
                    loading: false,
                    bookings: filteredBookings,
                    //  listings: filteredListings
                });
            }
        }
    }
    GetBookings() {
        this.setState({ loading: true, bookings: [] })
        axios
            .get(`${GET_lender_bookings}${this.state.userId}`)
            .then(response => {
                console.log(`[INFO] Retrieved bookings for userId: ${this.state.userId}, data: `, response.data)
                const payload = MapUserBookingFromResponse(response.data);
                console.log("PAYLOADD", payload)
                this.setState({
                    loading: false,
                    bookings: payload.bookings,
                    listings: payload.listings,
                    allBookings: payload.bookings,
                    allListings: payload.listings,
                })
            })
            .catch(error => {
                console.log(`[ERROR] Getting booking for userId: ${this.state.userId}, error: `, error)
                this.setState({ loading: false, error })
            })
    }

    render() {
        return (
            <>

                {/* <!-- Listings --> */}
                <div class="col-lg-12 col-md-12">
                    <div class="dashboard-list-box margin-top-0">

                        {/* <!-- Booking Requests Filters  --> */}
                        <div class="booking-requests-filter">

                            <div className="sort-by">
                                <div className="sort-by-select">
                                    <select onChange={this.handleFilterByStatus} data-placeholder="Default order" class="chosen-select-no-single">
                                        <option value="all">All booking statuses</option>
                                        {Object.values(BookingStatusEnum).map(state => {
                                            return <option value={state}>{state.toLowerCase().charAt(0).toUpperCase() + state.toLowerCase().slice(1)}</option>
                                        })}
                                        {/* <option>Burger House</option>
                                        <option>Tom's Restaurant</option>
                                        <option>Hotel Govendor</option> */}
                                    </select>
                                </div>
                            </div>

                            <div className="sort-by">
                                <div className="sort-by-select">
                                    <select onChange={this.handleFilterByListingId} data-placeholder="Default order" class="chosen-select-no-single">
                                        <option value="all">All Listings</option>
                                        {this.state.listings.map(listing => {
                                            return <option value={listing.id}>{listing.title}</option>
                                        })}
                                        {/* <option>Burger House</option>
                                        <option>Tom's Restaurant</option>
                                        <option>Hotel Govendor</option> */}
                                    </select>
                                </div>
                            </div>

                            {/* <!-- Date Range --> */}
                            <div id="booking-date-range">
                                <span></span>
                            </div>
                        </div>

                        {/* <!-- Reply to review popup --> */}
                        <div id="small-dialog" class="zoom-anim-dialog mfp-hide">
                            <div class="small-dialog-header">
                                <h3>Send Message</h3>
                            </div>
                            <div class="message-reply margin-top-0">
                                <textarea cols="40" rows="3" placeholder="Your Message to Kathy"></textarea>
                                <button class="button">Send</button>
                            </div>
                        </div>

                        <h4>Booking Requests</h4>
                        <ul>
                            {this.state.bookings !== undefined ? this.state.bookings.map(booking => (<>
                                <Booking
                                    bookingId={booking.bookingId}
                                    profilePicUrl={booking.profilePicUrl}
                                    title={booking.title}
                                    timePeriod={booking.timePeriod}
                                    price={booking.price}
                                    username={booking.username}
                                    email={booking.email}
                                    phone={booking.phone}
                                    paid={booking.paid}
                                    status={booking.status}
                                    confirmedDate={booking.confirmedDate}
                                    requestDate={booking.requestDate}
                                    lastModifiedDate={booking.lastModifiedDate}
                                />
                            </>)) : ""}

                        </ul>
                    </div>
                </div>

                <Loader showLoader={this.state.loading} />
            </>
        )
    }

}

LenderBookings.propTypes = {}

export default LenderBookings
