import React, { Component } from "react"
import PropTypes from "prop-types"
import axios from 'axios';

import { GET_cancel_booking, POST_dismiss_booking_notification } from "../../../../api/endpoints";
import { BookingStatusEnum } from "../enums/BookingStatusEnum";
import { GetUserId, GetJwToken } from "../../../login/lib/cookies";
import { Link } from "gatsby";
import { DashBoardPageTypeEnum } from "../enums/DashboardPageTypeEnum";

const bookingStatusesAndEndpoints = {
	[BookingStatusEnum.REJECTED]: { class: "pending-booking" },
	[BookingStatusEnum.APPROVED]: { class: "approved-booking" },
	[BookingStatusEnum.CANCELLED]: { class: "canceled-booking", endpoint: GET_cancel_booking }
}


class Booking extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			newStatus: undefined
		}
	}
	componentDidMount() { }
	render() {
		const {
			status,
			bookingId,
			listingImageUrl,
			title,
			timePeriod,
			price,
			username,
			email,
			phone,
			paid,
			confirmedDate,
			requestDate,
			lastModifiedDate,
			changePageFn
		} = this.props;

		const userId = GetUserId();

		// console.log("Booking status: ", bookingStatusesAndEndpoints[status])
		const updateBooking = (bookingId, status) => {

			this.setState({ loading: true })
			axios
				.get(`${bookingStatusesAndEndpoints[status].endpoint}${bookingId}&userId=${userId}`)
				.then(response => {
					if (response.data === "UN_AUTH") console.log("UN_AUTH for userId:" + userId)
					if (response.data === "INVALID") console.log("INVALID for listing:" + bookingId)
					if (response.data === "BOOKING_ACTIVE") console.log("BOOKING_ACTIVE for listing:" + bookingId)
					if (response.data === BookingStatusEnum.REJECTED || BookingStatusEnum.APPROVED || BookingStatusEnum.CANCELLED) {
						this.setState({ newStatus: response.data })
					}
					console.log(`[INFO] Updated bookingId: ", bookingId ${bookingId}, data: `, response.data)
					this.setState({ loading: false })
				})
				.catch(error => {
					console.log(`[ERROR] updating bookingId: ${bookingId} , error: `, error)
					this.setState({ loading: false, error: error })
				})
		};
		const cssState = this.state.newStatus === undefined ? bookingStatusesAndEndpoints[status] : bookingStatusesAndEndpoints[this.state.newStatus];

		return (
			<>
				{/* <Link
					to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.BOOKING}`}
					state={{ bookingId: bookingId }}> */}
				<li className={bookingStatusesAndEndpoints[status] !== undefined ? bookingStatusesAndEndpoints[status].class : "pending-booking"}>
					<div className="list-box-listing bookings">
						<div className="list-box-listing-img"><img src={listingImageUrl} alt="" /></div>
						<div className="list-box-listing-content">
							<div className="inner">
								<h3>{title}
									<span className="booking-status pending">
										{this.state.newStatus === undefined ? status : this.state.newStatus}
									</span>
									<span className={!paid ? "booking-status unpaid" : ""}>
										{!paid ? "Unpaid" : " Confirmed"}
									</span>
								</h3>

								<div className="inner-booking-list">
									<h5>Booking Date:</h5>
									<ul className="booking-list">
										<li className="">{timePeriod}</li>
									</ul>
								</div>

								<div className="inner-booking-list">
									<h5>Booking Details:</h5>
									<ul className="booking-list">
										<li className="highlighted"> Requested: {requestDate}</li>
										{status === (BookingStatusEnum.REJECTED || BookingStatusEnum.CANCELLED) ? <li className="highlighted"> Last Modified: {lastModifiedDate} </li> : ""}
										{confirmedDate !== "" ? <li className="highlighted"> Confirmed: {confirmedDate} </li> : ""}
									</ul>
								</div>

								<div className="inner-booking-list">
									<h5>Price:</h5>
									<ul className="booking-list">
										<li className="">{price}</li>
									</ul>
								</div>

								<div className="inner-booking-list">
									<h5>Client:</h5>
									<ul className="booking-list">
										<li>{username}</li>
										<li>{email}</li>
										<li>{phone}</li>
									</ul>
								</div>

								{/* <a href="#small-dialog" className="rate-review popup-with-zoom-anim"><i className="sl sl-icon-envelope-open"></i> Send Message</a> */}
								<a onClick={() => changePageFn(DashBoardPageTypeEnum.BOOKING)} href={`/admin/dashboard/?page=${DashBoardPageTypeEnum.BOOKING}&bookingId=${bookingId}`} className="rate-review popup-with-zoom-anim">View</a>
								{/* <a onClick={() => changePageFn(DashBoardPageTypeEnum.BOOKING)} href={`/admin/dashboard/?page=${DashBoardPageTypeEnum.BOOKING}`} className="rate-review popup-with-zoom-anim">View</a> */}
								{/* 
								<Link
									to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.BOOKING}&bookingId=${bookingId}`}
									onClick={() => changePageFn(DashBoardPageTypeEnum.BOOKING)}
									state={{ "bookingId": bookingId }}
									className="rate-review popup-with-zoom-anim">
									<i className="sl sl-icon-eye-open"></i>
									View New
								</Link> */}

							</div>
						</div>
					</div>
					<div className="buttons-to-right">
						{this.state.newStatus === undefined ?
							<>
								{status == BookingStatusEnum.APPROVED ?
									<a onClick={() => updateBooking(bookingId, BookingStatusEnum.APPROVED)} className="button gray reject"><i className="sl sl-icon-close"></i> Cancel</a>
									: ""}

								{status == BookingStatusEnum.REJECTED ?
									<a onClick={() => updateBooking(bookingId, BookingStatusEnum.APPROVED)} className="button gray approve"><i className="sl sl-icon-check"></i> Approve</a>
									: ""}

								{status == BookingStatusEnum.PENDING ?
									<>
										<a onClick={() => updateBooking(bookingId, BookingStatusEnum.REJECTED)} className="button gray reject"><i className="sl sl-icon-close"></i> Reject</a>
										<a onClick={() => updateBooking(bookingId, BookingStatusEnum.APPROVED)} className="button gray approve"><i className="sl sl-icon-check"></i> Approve</a>
									</>
									: ""}
							</> : <>
								{this.state.newStatus == BookingStatusEnum.APPROVED ?
									<a onClick={() => updateBooking(bookingId, BookingStatusEnum.APPROVED)} className="button gray reject"><i className="sl sl-icon-close"></i> Cancel</a>
									: ""}

								{this.state.newStatus == BookingStatusEnum.REJECTED ?
									<a onClick={() => updateBooking(bookingId, BookingStatusEnum.APPROVED)} className="button gray approve"><i className="sl sl-icon-check"></i> Approve</a>
									: ""}

								{this.state.newStatus == BookingStatusEnum.PENDING ?
									<>
										<a onClick={() => updateBooking(bookingId, BookingStatusEnum.REJECTED)} className="button gray reject"><i className="sl sl-icon-close"></i> Reject</a>
										<a onClick={() => updateBooking(bookingId, BookingStatusEnum.APPROVED)} className="button gray approve"><i className="sl sl-icon-check"></i> Approve</a>
									</>
									: ""}
							</>}
					</div>
				</li>

				{/* </Link> */}


			</>
		)
	}



}

Booking.propTypes = {}

export default Booking
