import React, { Component } from "react"
import axios from 'axios';
import { Link } from "gatsby"
import Booking from "../client-bookings/booking";
import { GET_bookmarks, GET_remove_bookmark } from "../../../../api/endpoints";
import { GetUserId } from "../../../login/lib/cookies"
import { mockBookings } from "../../../../mocks";
import { MapUserBookingFromResponse } from "../../../../api/mapper"
import Loader from "../../../shared/loader";
import { BookingStatusEnum } from "../enums/BookingStatusEnum"
import Bookmark from "./bookmark";

const useMock = false;

class Bookmarks extends Component {

    state = {
        loading: true,
        bookmarks: [],
        userId: GetUserId()
    }

    componentDidMount() {
        if (useMock === true) {
            this.setState({
                loading: false,
                bookmarks: []
            })
        } else {
            this.GetBookmarks(this.state.userId)
        }
    }


    GetBookmarks() {
        this.setState({ loading: true, bookmarks: [] })
        axios
            .get(`${GET_bookmarks}${this.state.userId}`)
            .then(response => {
                console.log(`[INFO] Retrieved bookmarks for userId: ${this.state.userId}, data: `, response.data)
                const payload = response.data;

                this.setState({
                    loading: false,
                    bookmarks: payload
                })

                console.log("SET STATE", this.state)
            })
            .catch(error => {
                console.log(`[ERROR] Getting bookmarks for userId: ${this.state.userId}, error: `, error)
                this.setState({ loading: false, error })
            })
    }



    removeBookmarkFn = (listingId) => {

        const filteredBookmarks = this.state.bookmarks.filter(bookmark => {
            return bookmark.listingId !== listingId
        });
        console.log("FILTERED BOOKINGS", filteredBookmarks)

        axios
            .get(GET_remove_bookmark(GetUserId(), listingId))
            .then(response => {
                this.setState({
                    ...this.state,
                    loading: false,
                    bookmarks: filteredBookmarks,
                    //  listings: filteredListings
                });

            })
            .catch(error => {
                console.log("Bad response", error)
                this.setState({ loading: false, error })
            })



    }


    render() {
        return (
            <>
                <div class="row">

                    {/* <!-- Listings --> */}
                    <div class="col-lg-12 col-md-12">
                        <div class="dashboard-list-box margin-top-0">
                            <h4>Bookmarked Listings</h4>
                            <ul>

                                {this.state.bookmarks !== undefined ? this.state.bookmarks.map(bookmark => (<>
                                    <Bookmark
                                        removeBookmarkFn={this.removeBookmarkFn}
                                        listingTitle={bookmark.listingTitle}
                                        listingLocation={bookmark.listingLocation}
                                        listingRating={bookmark.listingRating}
                                        listingPic={bookmark.listingPic}
                                        listingId={bookmark.listingId} />
                                </>)) : ""}
                            </ul>
                        </div>
                    </div>
                </div>
                <Loader showLoader={this.state.loading} />
            </>
        )
    }

}

Bookmarks.propTypes = {}

export default Bookmarks
