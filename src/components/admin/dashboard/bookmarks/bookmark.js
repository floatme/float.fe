import React, { Component } from "react"
import PropTypes from "prop-types"
import axios from 'axios';

import { GET_cancel_booking, POST_dismiss_booking_notification } from "../../../../api/endpoints";
import { BookingStatusEnum } from "../enums/BookingStatusEnum";
import { GetUserId, GetJwToken } from "../../../login/lib/cookies";
import { Link } from "gatsby";
import { DashBoardPageTypeEnum } from "../enums/DashboardPageTypeEnum";

const bookingStatusesAndEndpoints = {
	[BookingStatusEnum.REJECTED]: { class: "pending-booking" },
	[BookingStatusEnum.APPROVED]: { class: "approved-booking" },
	[BookingStatusEnum.CANCELLED]: { class: "canceled-booking", endpoint: GET_cancel_booking }
}


class Bookmark extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			newStatus: undefined
		}
	}
	componentDidMount() { }
	render() {
		const {
			removeBookmarkFn,
			listingId,
			listingTitle,
			listingLocation,
			listingRating,
			listingPic
		} = this.props;

		console.log("PROPS", this.props)

		const userId = GetUserId();

		// console.log("Booking status: ", bookingStatusesAndEndpoints[status])

		return (
			<li>
				<Link
					to={`/detail?id=${listingId}`}
					state={{ id: listingId }}>
					<div class="list-box-listing">
						<div class="list-box-listing-img"><a href="#"><img src={listingPic} alt="" /></a></div>
						<div class="list-box-listing-content">
							<div class="inner">
								<h3>{listingTitle}</h3>
								<span>{listingLocation}</span>
								<div class="star-rating" data-rating={listingRating}>
									{/* <div class="rating-counter">(23 reviews)</div> */}
								</div>
							</div>
						</div>
					</div>
					<div class="buttons-to-right">
						<a href="#" onClick={() => removeBookmarkFn(listingId)} class="button gray"><i class="sl sl-icon-close"></i> Delete</a>
					</div>

				</Link>

			</li>
		)
	}



}

Bookmark.propTypes = {}

export default Bookmark
