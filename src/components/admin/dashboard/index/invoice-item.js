import React from "react"
import PropTypes from "prop-types"
const InvoiceItem = props => {
	const {
		title,
		paid,
		orderNo,
		date,
		status,
		invoiceLink
	} = props


	return (
		<>
			<li><i className="list-box-icon sl sl-icon-doc"></i>
				<strong>{title}</strong>
				<ul>
					{paid === true ?
						<li className="unpaid">Unpaid</li> :
						<li className="paid">Paid</li>
					}
					<li>Order: {orderNo}</li>
					<li>Date: {date}</li>
				</ul>
				<div className="buttons-to-right">
					<a href={invoiceLink} className="button gray">View Invoice</a>
				</div>
			</li>

		</>
	)
}

InvoiceItem.propTypes = {}

export default InvoiceItem
