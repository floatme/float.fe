import React from "react"
import PropTypes from "prop-types"
import { POST_dismiss_booking_notification } from "../../../../api/endpoints";
import axios from 'axios';

const ActivityItem = props => {
	const {
		subject,
		activity,
		iconClass,
		reviewRating,
		listingUrl,
		listing,
		userId,
		id,
		dismissFn
	} = props
	console.log("PROPS HER", props)
	return (
		<>
			<li>
				<i className={`list-box-icon sl ${iconClass}`}></i> {subject}
				{reviewRating !== "" ?
					<><div class="numerical-rating" data-rating={reviewRating}></div>
						{" "}{activity}{" on "}
					</> : ""}
				<strong>
					<a href={`./listing/${listingUrl}`}> {listing} </a></strong>
				{reviewRating === "" ? <>{activity}</> : ""}
				<a onClick={() => dismissFn(id)} class="close-list-item"><i class="fa fa-close"></i></a>
				{/* <button onClick={() => dismissFn(id)}></button> */}
			</li>

		</>
	)
}

ActivityItem.propTypes = {}

export default ActivityItem
