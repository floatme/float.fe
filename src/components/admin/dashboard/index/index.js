import React, { Component } from "react"
import axios from 'axios';

import StatBox from "./stat-box";
import ActivityItem from "./activity-item";
import InvoiceItem from "./invoice-item";

import { GetUserId } from "../../../login/lib/cookies"
import { GET_profile_summary, GET_category } from "../../../../api/endpoints";
import { mockProfileSummary } from "../../../../mocks";
import Loader from "../../../shared/loader";
const useMock = false;

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			error: false,
			profileSummary: {
				activities: [],
				invoices: [],
				stats: [],
				notifications: []
			},
			userId: GetUserId(),
			categories: []
		}
	}

	componentDidMount() {
		console.log("DASH PROPS INIT", this.props)
		if (useMock) {
			this.setState({
				loading: false,
				profileSummary: mockProfileSummary,
			})
		} else {
			console.log("CURRENT STATE PROFILE", this.state)
			this.GetProfileSummary()
		}
	}

	bookingDismissFn(id) {
		const filteredActivities = this.state.profileSummary.activities.filter(booking => {
			return booking.id !== id
		});
		console.log("dismiss", id)
		console.log("state", this.state)
		console.log("filter", filteredActivities)
		this.setState({
			loading: false,
			profileSummary: {
				...this.state.profileSummary, activities: filteredActivities
			},
		})
	}

	GetProfileSummary() {
		this.setState({ loading: true })
		axios
			.get(`${GET_profile_summary}${this.state.userId}`)
			.then(response => {
				console.log(`[INFO] Retrieved user profile for userId: ${this.state.userId}, data: `, response.data)
				const payload = response.data
				this.setState({
					loading: false,
					profileSummary: payload,
				})
				console.log(`[INFO] State after response data: `, this.state)
			})
			.catch(error => {
				console.log(`[ERROR] Getting booking for userId: ${this.state.userId}, error: `, error)
				this.setState({ loading: false, error })
			})
	}

	render() {
		return (
			<>

				{/* <!-- Notification --> */}
				<div class="row">
					<div class="col-md-12">
						{this.state.profileSummary.notifications.map(notification => (<>
							<div class="notification success closeable margin-bottom-30">
								<p>{notification.subject} {notification.activity}<strong>{notification.listing}</strong> </p>
								<a class="close" href="#"></a>
							</div>
						</>))}
					</div>
				</div>

				{/* <!-- Content --> */}
				<div class="row">
					{this.state.profileSummary.stats.map(stat => (<>
						<StatBox
							title={stat.title}
							number={stat.number}
							iconClass={stat.iconClass}
							color={stat.color}
						/>
					</>))}

				</div>

				{/* <!-- Recent Activity --> */}
				<div class="col-lg-6 col-md-12">
					<div class="dashboard-list-box with-icons margin-top-20">
						<h4>Recent Activities</h4>
						<ul>

							{this.state.profileSummary.activities.map(activity => (<>
								<ActivityItem
									subject={activity.subject}
									activity={activity.activity}
									iconClass={activity.iconClass}
									reviewRating={activity.reviewRating}
									listingUrl={activity.listingUrl}
									listing={activity.listing}
									dismissFn={this.bookingDismissFn}
								/>
							</>))}
						</ul>
					</div>
				</div>

				{/* <!-- Invoices --> */}
				<div class="col-lg-6 col-md-12">
					<div class="dashboard-list-box invoices with-icons margin-top-20">
						<h4>Invoices</h4>
						<ul>
							{this.state.profileSummary.invoices.map(invoice => (<>
								<InvoiceItem
									title={invoice.title}
									paid={invoice.paid}
									orderNo={invoice.orderNo}
									date={invoice.date}
									status={invoice.status}
									invoiceLink={invoice.invoiceLink}
								/>

							</>))}
						</ul>
					</div>
				</div>
				<Loader showLoader={this.state.loading} />
			</>
		)
	}

}

Home.propTypes = {}

export default Home
