import React from "react"
import PropTypes from "prop-types"
const StatBox = props => {
	const {
		title,
		number,
		iconClass,
		color
	} = props
	console.log("STAT props", props)

	return (
		<>
			{/* <!-- Item --> */}
			<div className="col-lg-3 col-md-6">
				<div className={`dashboard-stat color-${color}`}>
					<div className="dashboard-stat-content"><h4>{number}</h4> <span>{title}</span></div>
					<div className="dashboard-stat-icon"><i className={`im ${iconClass}`}></i></div>
				</div>
			</div>

		</>
	)
}

StatBox.propTypes = {}

export default StatBox
