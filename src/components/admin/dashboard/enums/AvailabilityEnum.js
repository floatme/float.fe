export const AvailabilityEnum = Object.freeze({
    WRONG_DATE: "WRONG_DATE",
    TOO_LONG: "TOO_LONG",
    UNAVAILABLE: "UNAVAILABLE",
    AVAILABLE: "AVAILABLE"
})

export const AvailabilityValidation = Object.freeze({
    WRONG_DATE: "Invalid dates selected.",
    TOO_LONG: "Duration is over 30 days",
    UNAVAILABLE: "Listing is unavailable for dates",
    AVAILABLE: "Listing is available for dates"
})