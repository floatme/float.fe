export const ListingStatusEnum = Object.freeze({
    ENABLED: "ENABLED",
    DISABLED: "DISABLED",
    PENDING_DELETION: "PENDING_DELETION",
    DELETED: "DELETED"
})