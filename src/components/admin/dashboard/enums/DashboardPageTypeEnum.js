
import { GetUserInfo } from "../../../login/lib/cookies"
export const DashBoardPageTypeEnum = Object.freeze({

    ADD_LISTING: "ADD_LISTING",
    PROFILE: "PROFILE",
    BOOKINGS: "BOOKINGS",
    HOME: "HOME",
    LISTINGS: "LISTINGS",
    CLIENT_HOME: "CLIENT_HOME",
    CLIENT_BOOKINGS: "CLIENT_BOOKINGS",
    CLIENT_INVOICE: "CLIENT_INVOICES",
    BOOKING: "BOOKING",
    BOOKMARK: "BOOKMARK",
    REVIEWS: "REVIEWS",
    ADMIN: "ADMIN"
    // CHAT: "CHAT"

})

export const DashBoardPageTitles = Object.freeze({

    ADD_LISTING: "Add a new listing",
    PROFILE: "My profile",
    BOOKINGS: "My bookings",
    HOME: `Welcome Back ${GetUserInfo().firstName}!`,
    LISTINGS: "My Listings",
    CLIENT_BOOKINGS: "Your Bookings",
    CLIENT_INVOICE: "Invoice",
    BOOKING: "My bookings",
    BOOKMARK: "My bookmarks",
    REVIEWS: "My reviews",
    ADMIN: "Admin Dashboard"
})