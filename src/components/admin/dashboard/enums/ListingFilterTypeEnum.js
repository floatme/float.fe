export const ListingFilterTypeEnum = Object.freeze({
    NONE: "NONE",
    DISTANCE: "DISTANCE",
    CATEGORY: "CATEGORY",
    MINPRICE: "MINPRICE",
    MAXPRICE: "MAXPRICE",
    DELIVERY: "DELIVERY",
    COLLECTION: "COLLECTION"
})

export const ListingFilterOrderTypeEnum = Object.freeze({
    NONE: "NONE",
    NEWEST: "NEWEST",
    REVIEWCOUNT: "REVIEWCOUNT",
    VIEWCOUNT: "VIEWCOUNT",
    RATING: "RATING"
})