export const BookingStatusEnum = Object.freeze({
    REJECTED: "REJECTED",
    APPROVED: "APPROVED",
    CANCELLED: "CANCELLED",
    PENDING: "PENDING",
    ONGOING: "ONGOING",
    COMPLETE: "COMPLETE",
})