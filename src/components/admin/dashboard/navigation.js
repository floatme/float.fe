import { Link } from "gatsby"
import React from "react"
import PropTypes from "prop-types"
import { DashBoardPageTypeEnum } from "./enums/DashboardPageTypeEnum"
import { GetUserId, GetJwToken, SetUserId, SetJwToken, RemoveUserId, RemoveJwToken } from "../../../components/login/lib/cookies"

const Navigation = props => {
	const { changePageFn, pageType } = props
	const logoutFn = () => {
		RemoveUserId();
		RemoveJwToken();
	}
	return (
		<>
			{/* <!-- Navigation
	==================================================--> */}

			{/* <!-- Responsive Navigation Trigger--> */}
			<a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

			<div class="dashboard-nav">
				<div class="dashboard-nav-inner">


					<ul data-submenu-title="Admin dashboard">
						{pageType === DashBoardPageTypeEnum.ADMIN ?
							(<li className="active">
								{/* <a onClick={() => changePageFn(DashBoardPageTypeEnum.ADD_LISTING)}><i class="sl sl-icon-plus"></i> Add Listing</a> */}
								<Link
									to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.ADMIN}`}
									onClick={() => changePageFn(DashBoardPageTypeEnum.ADMIN)}
								><i className="sl sl-icon-home"></i> Home </Link>
							</li>) :
							(<li><Link
								to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.ADMIN}`}
								onClick={() => changePageFn(DashBoardPageTypeEnum.ADMIN)}
							><i className="sl sl-icon-home"></i>  Home</Link></li>)
						}
					</ul>

					<ul data-submenu-title="My dashboard">
						{pageType === DashBoardPageTypeEnum.CLIENT_HOME ?
							(<li className="active">
								{/* <a onClick={() => changePageFn(DashBoardPageTypeEnum.ADD_LISTING)}><i class="sl sl-icon-plus"></i> Add Listing</a> */}
								<Link
									to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.CLIENT_HOME}`}
									onClick={() => changePageFn(DashBoardPageTypeEnum.CLIENT_HOME)}
								><i className="sl sl-icon-home"></i> Home </Link>
							</li>) :
							(<li><Link
								to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.CLIENT_HOME}`}
								onClick={() => changePageFn(DashBoardPageTypeEnum.CLIENT_HOME)}
							><i className="sl sl-icon-home"></i>  Home</Link></li>)
						}
						{pageType === DashBoardPageTypeEnum.CLIENT_BOOKINGS ?
							(<li className="active">
								<Link
									to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.CLIENT_BOOKINGS}`}
									onClick={() => changePageFn(DashBoardPageTypeEnum.CLIENT_BOOKINGS)}
								><i className="fa fa-calendar-check-o"></i> Bookings</Link>
							</li>) :
							(<li><Link
								to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.CLIENT_BOOKINGS}`}
								onClick={() => changePageFn(DashBoardPageTypeEnum.CLIENT_BOOKINGS)}
							><i className="fa fa-calendar-check-o"></i> Bookings</Link></li>)
						}
						{pageType === DashBoardPageTypeEnum.BOOKMARK ?
							(<li className="active">
								<Link
									to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.BOOKMARK}`}
									onClick={() => changePageFn(DashBoardPageTypeEnum.BOOKMARK)}
								><i className="sl sl-icon-heart"></i> Bookmarks</Link>
							</li>) :
							(<li><Link
								to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.BOOKMARK}`}
								onClick={() => changePageFn(DashBoardPageTypeEnum.BOOKMARK)}
							><i className="sl sl-icon-heart"></i> Bookmarks</Link></li>)
						}
						{/* 
						{pageType === DashBoardPageTypeEnum.CLIENT_INVOICE ?
							(<li className="active">
								<Link
									to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.CLIENT_INVOICE}`}
									onClick={() => changePageFn(DashBoardPageTypeEnum.CLIENT_INVOICE)}
								><i className="fa fa-calendar-check-o"></i> Invoices</Link>
							</li>) :
							(<li><Link
								to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.CLIENT_INVOICE}`}
								onClick={() => changePageFn(DashBoardPageTypeEnum.CLIENT_INVOICE)}
							><i className="fa fa-calendar-check-o"></i> Invoices</Link></li>)
						} */}
					</ul>

					<ul data-submenu-title="My products">
						{pageType === DashBoardPageTypeEnum.HOME ?
							(<li className="active">

								{/* <a onClick={() => changePageFn(DashBoardPageTypeEnum.HOME)}><i class="sl sl-icon-settings"></i> Dashboard</a> */}
								<Link
									to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.HOME}`}
									onClick={() => changePageFn(DashBoardPageTypeEnum.HOME)}
									state={{ "id": DashBoardPageTypeEnum.HOME }}

								><i className="sl sl-icon-home"></i> Overview</Link>

							</li>) :
							(<li>
								<Link
									to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.HOME}`}
									onClick={() => changePageFn(DashBoardPageTypeEnum.HOME)}
								><i className="sl sl-icon-home"></i> Overview</Link>
							</li>)
						}
						{pageType === DashBoardPageTypeEnum.BOOKINGS ?
							(<li className="active">
								<Link
									to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.BOOKINGS}`}
									onClick={() => changePageFn(DashBoardPageTypeEnum.BOOKINGS)}
								><i className="sl sl-icon-wallet"></i> Requests</Link>
							</li>) :
							(<li><Link
								to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.BOOKINGS}`}
								onClick={() => changePageFn(DashBoardPageTypeEnum.BOOKINGS)}
							><i className="sl sl-icon-wallet"></i> Requests</Link></li>)
						}
						{pageType === DashBoardPageTypeEnum.LISTINGS ?
							(<li className="active">
								{/* <a onClick={() => changePageFn(DashBoardPageTypeEnum.LISTINGS)}><i class="sl sl-icon-layers"></i> My Listings</a> */}
								<Link
									to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.LISTINGS}`}
									onClick={() => changePageFn(DashBoardPageTypeEnum.LISTINGS)}
								><i className="sl sl-icon-layers"></i> My Listings</Link>
							</li>) :
							(<li>
								<Link
									to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.LISTINGS}`}
									onClick={() => changePageFn(DashBoardPageTypeEnum.LISTINGS)}
								><i className="sl sl-icon-layers"></i> My Listings</Link>
							</li>)
						}
						{pageType === DashBoardPageTypeEnum.REVIEWS ?
							(<li className="active">
								{/* <a onClick={() => changePageFn(DashBoardPageTypeEnum.LISTINGS)}><i class="sl sl-icon-layers"></i> My Listings</a> */}
								<Link
									to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.REVIEWS}`}
									onClick={() => changePageFn(DashBoardPageTypeEnum.REVIEWS)}
								><i className="sl sl-icon-star"></i> My Reviews</Link>
							</li>) :
							(<li>
								<Link
									to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.REVIEWS}`}
									onClick={() => changePageFn(DashBoardPageTypeEnum.REVIEWS)}
								><i className="sl sl-icon-star"></i> My Reviews</Link>
							</li>)
						}
						{pageType === DashBoardPageTypeEnum.ADD_LISTING ?
							(<li className="active">
								{/* <a onClick={() => changePageFn(DashBoardPageTypeEnum.ADD_LISTING)}><i class="sl sl-icon-plus"></i> Add Listing</a> */}
								<Link
									to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.ADD_LISTING}`}
									onClick={() => changePageFn(DashBoardPageTypeEnum.ADD_LISTING)}
								><i className="sl sl-icon-plus"></i>  Add Listing</Link>
							</li>) :
							(<li><Link
								to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.ADD_LISTING}`}
								onClick={() => changePageFn(DashBoardPageTypeEnum.ADD_LISTING)}
							><i className="sl sl-icon-plus"></i>  Add Listing</Link></li>)
						}
					</ul>
					<ul data-submenu-title="My account">
						{pageType === DashBoardPageTypeEnum.PROFILE ?
							(<li className="active">
								{/* <a onClick={() => changePageFn(DashBoardPageTypeEnum.PROFILE)}><i class="sl sl-icon-user"></i> My Profile</a> */}
								<Link
									to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.PROFILE}`}
									onClick={() => changePageFn(DashBoardPageTypeEnum.PROFILE)}
								><i className="sl sl-icon-user"></i>  My Profile</Link>
							</li>) :
							(<li><Link
								to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.PROFILE}`}
								onClick={() => changePageFn(DashBoardPageTypeEnum.PROFILE)}
							><i className="sl sl-icon-user"></i>  My Profile</Link></li>)
						}
						{/* <li><a onClick={() => changePageFn(DashBoardPageTypeEnum.PROFILE)}><i class="sl sl-icon-user"></i> My Profile</a></li> */}
						<li><a onClick={() => logoutFn()} href="../../"><i class="sl sl-icon-power"></i> Logout</a></li>
					</ul>

				</div>
			</div>
			{/* <!-- Navigation / End--> */}
		</>
	)
}

Navigation.propTypes = {}

export default Navigation
