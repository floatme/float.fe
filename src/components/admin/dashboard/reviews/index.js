import React, { Component } from "react"
import axios from 'axios';
import { GET_reviews } from "../../../../api/endpoints";
import { GetUserId } from "../../../login/lib/cookies"
import ReviewItem from "../../../detail/review/review-item";

const useMock = false;

class Reviews extends Component {

    state = {
        loading: true,
        reviews: [],
        userId: GetUserId()
    }

    componentDidMount() {
        if (useMock === true) {
            this.setState({
                loading: false,
                reviews: []
            })
        } else {
            this.GetReviews()
        }
    }


    GetReviews() {
        this.setState({ loading: true, reviews: [] })
        axios
            .get(`${GET_reviews}/null/${this.state.userId}`)
            .then(response => {
                console.log(`[INFO] Retrieved reviews for userId: ${this.state.userId}, data: `, response.data)
                const payload = response.data;

                this.setState({
                    loading: false,
                    reviews: payload
                })

                console.log("SET STATE", this.state)
            })
            .catch(error => {
                console.log(`[ERROR] Getting review for userId: ${this.state.userId}, error: `, error)
                this.setState({ loading: false, error })
            })
    }

    replyToReview = (listingId) => {

    }

    removeBookmarkFn = (listingId) => {

        // const filteredBookmarks = this.state.bookmarks.filter(bookmark => {
        //     return bookmark.listingId !== listingId
        // });
        // console.log("FILTERED BOOKINGS", filteredBookmarks)

        // axios
        //     .get(GET_remove_bookmark(GetUserId(), listingId))
        //     .then(response => {
        //         this.setState({
        //             ...this.state,
        //             loading: false,
        //             bookmarks: filteredBookmarks,
        //             //  listings: filteredListings
        //         });

        //     })
        //     .catch(error => {
        //         console.log("Bad response", error)
        //         this.setState({ loading: false, error })
        //     })
    }


    render() {
        return (
            <>
                {/* <!-- Listings --> */}
                <div class="col-lg-6 col-md-12">

                    <div class="dashboard-list-box margin-top-0">

                        {/* <!-- Sort by --> */}
                        <div class="sort-by">
                            <div class="sort-by-select">
                                <select data-placeholder="Default order" class="chosen-select-no-single">
                                    <option>All Listings</option>
                                    <option>Tom's Restaurant</option>
                                    <option>Sticky Band</option>
                                    <option>Hotel Govendor</option>
                                    <option>Burger House</option>
                                    <option>Airport</option>
                                    <option>Think Coffee</option>
                                </select>
                            </div>
                        </div>

                        <h4>Visitor Reviews</h4>

                        {/* <!-- Reply to review popup --> */}
                        <div id="small-dialog" class="zoom-anim-dialog mfp-hide">
                            <div class="small-dialog-header">
                                <h3>Reply to review</h3>
                            </div>
                            <div class="message-reply margin-top-0">
                                <textarea cols="40" rows="3"></textarea>
                                <button class="button">Reply</button>
                            </div>
                        </div>

                        <ul>

                            {this.state.reviews.map(review => (
                                <ReviewItem
                                    username={review.username}
                                    gravatar={review.userProfilePicUrl}
                                    userVerified={review.userVerified}
                                    dateCreated={review.timestamp}
                                    rating={review.rating}
                                    reviewText={review.reviewText}
                                    images={review.images}
                                    reviewUpCount={review.helpfulReviewCount}
                                    reviewId={review.id}
                                    userId={this.state.userId}
                                    showUpvote={false}
                                />
                            ))}
                        </ul>
                    </div>

                    {/* <!-- Pagination --> */}
                    <div class="clearfix"></div>
                    <div class="pagination-container margin-top-30 margin-bottom-0">
                        <nav class="pagination">
                            <ul>
                                <li><a href="#" class="current-page">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#"><i class="sl sl-icon-arrow-right"></i></a></li>
                            </ul>
                        </nav>
                    </div>
                    {/* <!-- Pagination / End --> */}

                </div>

                {/* <!-- Listings --> */}
                {/* <div class="col-lg-6 col-md-12">
                    <div class="dashboard-list-box margin-top-0">
                        <h4>Your Reviews</h4>
                        <ul>

                            <li>
                                <div class="comments listing-reviews">
                                    <ul>
                                        <li>
                                            <div class="avatar"><img src="images/reviews-avatar.jpg" alt="" /> </div>
                                            <div class="comment-content"><div class="arrow-comment"></div>
                                                <div class="comment-by">Your review <div class="comment-by-listing own-comment">on <a href="#">Tom's Restaurant</a></div> <span class="date">May 2019</span>
                                                    <div class="star-rating" data-rating="4.5"></div>
                                                </div>
                                                <p>Commodo est luctus eget. Proin in nunc laoreet justo volutpat blandit enim. Sem felis, ullamcorper vel aliquam non, varius eget justo. Duis quis nunc tellus sollicitudin mauris.</p>
                                                <a href="#" class="rate-review"><i class="sl sl-icon-note"></i> Edit</a>
                                            </div>

                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <div class="comments listing-reviews">
                                    <ul>
                                        <li>
                                            <div class="avatar"><img src="images/reviews-avatar.jpg" alt="" /> </div>
                                            <div class="comment-content"><div class="arrow-comment"></div>
                                                <div class="comment-by">Your review <div class="comment-by-listing own-comment">on <a href="#">Think Coffee</a></div> <span class="date">May 2019</span>
                                                    <div class="star-rating" data-rating="5"></div>
                                                </div>
                                                <p>Commodo est luctus eget. Proin in nunc laoreet justo volutpat blandit enim. Sem felis, ullamcorper vel aliquam non, varius eget justo. Duis quis nunc tellus sollicitudin mauris.</p>
                                                <a href="#" class="rate-review"><i class="sl sl-icon-note"></i> Edit</a>
                                            </div>

                                        </li>
                                    </ul>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div> */}

            </>
        )
    }

}

Reviews.propTypes = {}

export default Reviews
