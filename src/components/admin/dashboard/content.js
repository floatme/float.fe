import React from "react"
import AddListing from "./addListing";
import Profile from "./profile";
import { DashBoardPageTypeEnum } from "./enums/DashboardPageTypeEnum"
import { Link } from "gatsby"
import LenderBookings from "./lender-bookings";
import Home from "./index";
import Listings from "./listings";
import ClientHome from "./client-home";
import ClientBookings from "./client-bookings";
import BookingDetail from "./booking";
import Bookmarks from "./bookmarks";
import Reviews from "./reviews";
import AdminHome from "./admin";


const DashboardContent = props => {
	const { page, title, changePageFn, location } = props
	return (
		<>
			{/* <!-- Content ==================================================--> */}
			<div className="dashboard-content">

				{/* <!-- Titlebar--> */}
				<div id="titlebar">
					<div className="row">
						<div className="col-md-12">
							<h2>{title}</h2>
							{/* <!-- Breadcrumbs--> */}
							<nav id="breadcrumbs">
								<ul>
									<li>
										<Link
											to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.HOME}`}
											onClick={() => changePageFn(DashBoardPageTypeEnum.HOME)}>
											Home
										</Link>
									</li>
									<li>
										<Link
											to={`/admin/dashboard/?page=${DashBoardPageTypeEnum.HOME}`}
											onClick={() => changePageFn(DashBoardPageTypeEnum.HOME)}>
											Dashboard
										</Link>
									</li>
									<li>
										<Link
											to={`/admin/dashboard/?page=${page}`}
											onClick={() => changePageFn(page)}>
											{title}
										</Link>
									</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>

				<div className="row">


					{page === DashBoardPageTypeEnum.ADD_LISTING && (<>
						<AddListing />
					</>)}
					{page === DashBoardPageTypeEnum.PROFILE && (<>
						<Profile />
					</>)}

					{page === DashBoardPageTypeEnum.BOOKINGS && (<>
						<LenderBookings />
					</>)}

					{page === DashBoardPageTypeEnum.HOME && (<>
						<Home />
					</>)}

					{page === DashBoardPageTypeEnum.LISTINGS && (<>
						<Listings changePageFn={changePageFn} />
					</>)}

					{page === DashBoardPageTypeEnum.CLIENT_BOOKINGS && (<>
						<ClientBookings changePageFn={changePageFn} />
					</>)}

					{page === DashBoardPageTypeEnum.CLIENT_HOME && (<>
						<ClientHome changePageFn={changePageFn} />
					</>)}

					{page === DashBoardPageTypeEnum.CLIENT_INVOICE && (<>
						<Listings changePageFn={changePageFn} />
					</>)}

					{page === DashBoardPageTypeEnum.BOOKMARK && (<>
						<Bookmarks changePageFn={changePageFn} />
					</>)}

					{page === DashBoardPageTypeEnum.BOOKING && (<>
						<BookingDetail location={location} changePageFn={changePageFn} />
					</>)}

					{page === DashBoardPageTypeEnum.REVIEWS && (<>
						<Reviews location={location} changePageFn={changePageFn} />
					</>)}


					{page === DashBoardPageTypeEnum.ADMIN && (<>
						<AdminHome location={location} changePageFn={changePageFn} />
					</>)}


				</div>

			</div>
			{/* <!-- Content / End--> */}
		</>
	)
}

DashboardContent.propTypes = {}

export default DashboardContent
