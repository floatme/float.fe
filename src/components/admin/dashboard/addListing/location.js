import React from "react"
import PropTypes from "prop-types"
import { Map } from "../../../map"
import { _suburbs } from "../../../../mocks";
import LocationSearchInput from "../../../shared/location-search-input";

const getDistrictCities = () => {
	const distinct = [... new Set(_suburbs.map(x => x.city))]
	return distinct;
}


const getSuburbs = (city) => {
	const suburbs = _suburbs.filter(suburb => suburb.city === city)
	return suburbs;
}
const Location = props => {
	const { city, address, zipCode, suburb, handleChangeFn, handleLocationChange } = props
	const cities = getDistrictCities();
	const suburbs = (city === undefined || city === "") ? [] : getSuburbs(city)

	return (

		<>
			{/* <!-- Section--> */}
			<div className="add-listing-section margin-top-45">

				{/* <!-- Headline--> */}
				<div className="add-listing-headline">
					<h3><i className="sl sl-icon-location"></i> Location</h3>
				</div>

				<div className="submit-section">

					{/* <!-- Row--> */}
					<div className="row with-forms">

						{/* <!-- City--> */}
						{/* <div className="col-md-6">
							<h5>Province</h5>
							<input type="text" />
						</div> */}


						{/* <!-- City--> */}
						<div className="col-md-6">
							<h5>City</h5>
							<select required value={city} onChange={handleChangeFn} name="location" className="chosen-select-no-single" >
								<option value="">Select City</option>
								{cities.map(city => {
									return <option value={city}>{city}</option>
								})}
							</select>
						</div>

						{/* <!-- Suburb--> */}
						<div className="col-md-6">
							<h5>Suburb</h5>
							<select required value={suburb} onChange={handleChangeFn} name="suburb" className={suburbs.length !== 0 ? "chosen-select-no-single" : "chosen-select-no-single disabled-select"} >
								<option value="">Select Suburb</option>
								{suburbs.map(suburb => {
									return <option value={suburb.id}>{suburb.suburb}</option>
								})}
							</select>
						</div>



						{/* <!-- Zip-Code--> */}
						{/* <div className="col-md-6">
							<h5>Zip-Code</h5>
							<input value={zipCode} onChange={handleChangeFn} name="zipCode" type="text" />
						</div> */}

						{/* <!-- Address--> */}
						<div className="col-md-8">
							<h5>Address <span>* will not be displayed to other users</span></h5>

							{/* <input value={address} onChange={handleChangeFn} name="address" type="text" placeholder="e.g. 964 School Street" /> */}
							<LocationSearchInput changeLocation={handleLocationChange} />
						</div>


						{/* <!-- Zip-Code--> */}
						{/* <div className="col-md-6">
							<h5>Map</h5> */}
						{/* <Map /> */}
						{/* </div> */}
					</div>
					{/* <!-- Row / End--> */}

				</div>
			</div>
			{/* <!-- Section / End--> */}
		</>
	)
}

Location.propTypes = {}

export default Location
