import React from "react"
import PropTypes from "prop-types"

const Details = props => {
	const { description, phone, email, fb, tw, delivery, collection, terms, handleChangeFn } = props

	return (
		<>
			{/* <!-- Section--> */}
			<div className="add-listing-section margin-top-45">

				{/* <!-- Headline--> */}
				<div className="add-listing-headline">
					<h3><i className="sl sl-icon-docs"></i> Details</h3>
				</div>

				{/* <!-- Description--> */}
				<div className="form">
					<h5>Description</h5>
					<textarea required name="description" value={description} onChange={handleChangeFn} className="WYSIWYG" cols="40" rows="3" id="summary" spellcheck="true"></textarea>
				</div>

				<div className="form">
					<h5>Terms of use</h5>
					<textarea required name="terms" value={terms} onChange={handleChangeFn} className="WYSIWYG" cols="40" rows="3" id="summary" spellcheck="true"></textarea>
				</div>

				{/* <!-- Row--> */}
				<div className="row with-forms">

					{/* <!-- Phone--> */}
					<div className="col-md-4">
						<h5>Phone <span>(optional)</span></h5>
						<input required name="phone" value={phone} disabled readonly type="text" />
					</div>

					{/* <!-- Website--> */}
					{/* <div className="col-md-4">
						<h5>Website <span>(optional)</span></h5>
						<input type="text" />
					</div> */}

					{/* <!-- Email Address--> */}
					<div className="col-md-4">
						<h5>E-mail <span>(optional)</span></h5>
						<input required type="email" name="email" value={email} onChange={handleChangeFn} type="text" />
					</div>

				</div>
				{/* <!-- Row / End--> */}


				{/* <!-- Row--> */}
				<div className="row with-forms">

					{/* <!-- Phone--> */}
					<div className="col-md-4">
						<h5 className="fb-input"><i className="fa fa-facebook-square"></i> Facebook <span>(optional)</span></h5>
						<input name="fb" type="text" value={fb} readonly disabled placeholder="https://www.facebook.com/" />
					</div>

					{/* <!-- Website--> */}
					<div className="col-md-4">
						<h5 className="twitter-input"><i className="fa fa-twitter"></i> Twitter <span>(optional)</span></h5>
						<input name="tw" value={tw} readonly disabled type="text" placeholder="https://www.twitter.com/" />
					</div>

					{/* <!-- Email Address--> */}
					{/* <div className="col-md-4">
						<h5 className="gplus-input"><i className="fa fa-google-plus"></i> Google Plus <span>(optional)</span></h5>
						<input type="text" placeholder="https://plus.google.com" />
					</div> */}

				</div>
				{/* <!-- Row / End--> */}


				{/* <!-- Checkboxes--> */}
				<h5 className="margin-top-30 margin-bottom-10">Delivery / Collection</h5>
				<div className="checkboxes in-row margin-bottom-20">

					<input name="delivery" checked={delivery} onChange={handleChangeFn} id="check-a" type="checkbox" />
					<label for="check-a">Delivery</label>

					<input name="collection" checked={collection} onChange={handleChangeFn} id="check-b" type="checkbox" />
					<label for="check-b">Collection</label>

				</div>
				{/* <!-- Checkboxes / End--> */}

			</div>
			{/* <!-- Section / End--> */}

		</>
	)
}

Details.propTypes = {}

export default Details
