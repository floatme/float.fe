import React from "react"
import PropTypes from "prop-types"

const BasicInfo = props => {



	const { title, subCategoryId, keywords, handleChangeFn } = props

	return (
		<>
			{/* <!-- Section--> */}
			<div className="add-listing-section">

				{/* <!-- Headline--> */}
				<div className="add-listing-headline">
					<h3><i className="sl sl-icon-doc"></i> Basic Information</h3>
				</div>

				{/* <!-- Title--> */}
				<div className="row with-forms">
					<div className="col-md-12">
						<h5>Listing Title <i className="tip" data-tip-content="Name of your business"></i></h5>
						<input required name="title" className="search-field" type="text" value={title} onChange={handleChangeFn} />
					</div>
				</div>

				{/* <!-- Row--> */}
				<div className="row with-forms">

					{/* <!-- Status--> */}
					<div className="col-md-6">
						<h5>Category</h5>
						<select required name="subCategoryId" value={subCategoryId} onChange={handleChangeFn} className="chosen-select-no-single" >
							<option value="">Select Category</option>
							<option value="5dcc36d24157d00d783eb8b1">Sports Equipment</option>
						</select>
					</div>

					{/* <!-- Type--> */}
					<div className="col-md-6">
						<h5>Keywords <i className="tip" data-tip-content="Maximum of 15 keywords related with your business"></i></h5>
						<input required name="keywords" value={keywords} onChange={handleChangeFn} type="text" placeholder="Keywords should be separated by commas" />
					</div>

				</div>
				{/* <!-- Row / End--> */}

			</div>
			{/* <!-- Section / End--> */}

		</>
	)
}

BasicInfo.propTypes = {}

export default BasicInfo
