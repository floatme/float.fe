import React from "react"
import PropTypes from "prop-types"

const BookingSlots = props => {
	const { } = props

	return (
		<>
			<div className="add-listing-section margin-top-45">

				{/* <!-- Headline--> */}
				<div className="add-listing-headline">
					<h3><i className="sl sl-icon-clock"></i> Opening Hours</h3>
					{/* <!-- Switcher--> */}
					<label className="switch"><input type="checkbox" checked /><span className="slider round"></span></label>
				</div>

				{/* <!-- Switcher ON-OFF Content--> */}
				<div className="switcher-content">

					{/* <!-- Day--> */}
					<div className="row opening-day">
						<div className="col-md-2"><h5>Monday</h5></div>
						<div className="col-md-5">
							<select className="chosen-select" data-placeholder="Opening Time">
								<option label="Opening Time"></option>
								<option>Closed</option>
								<option>1 AM</option>
								<option>2 AM</option>
								<option>3 AM</option>
								<option>4 AM</option>
								<option>5 AM</option>
								<option>6 AM</option>
								<option>7 AM</option>
								<option>8 AM</option>
								<option>9 AM</option>
								<option>10 AM</option>
								<option>11 AM</option>
								<option>12 AM</option>
								<option>1 PM</option>
								<option>2 PM</option>
								<option>3 PM</option>
								<option>4 PM</option>
								<option>5 PM</option>
								<option>6 PM</option>
								<option>7 PM</option>
								<option>8 PM</option>
								<option>9 PM</option>
								<option>10 PM</option>
								<option>11 PM</option>
								<option>12 PM</option>
							</select>
						</div>
						<div className="col-md-5">
							<select className="chosen-select" data-placeholder="Closing Time">
								<option label="Closing Time"></option>
								<option>Closed</option>
								<option>1 AM</option>
								<option>2 AM</option>
								<option>3 AM</option>
								<option>4 AM</option>
								<option>5 AM</option>
								<option>6 AM</option>
								<option>7 AM</option>
								<option>8 AM</option>
								<option>9 AM</option>
								<option>10 AM</option>
								<option>11 AM</option>
								<option>12 AM</option>
								<option>1 PM</option>
								<option>2 PM</option>
								<option>3 PM</option>
								<option>4 PM</option>
								<option>5 PM</option>
								<option>6 PM</option>
								<option>7 PM</option>
								<option>8 PM</option>
								<option>9 PM</option>
								<option>10 PM</option>
								<option>11 PM</option>
								<option>12 PM</option>
							</select>
						</div>
					</div>
					{/* <!-- Day / End--> */}

					{/* <!-- Day--> */}
					<div className="row opening-day js-demo-hours">
						<div className="col-md-2"><h5>Tuesday</h5></div>
						<div className="col-md-5">
							<select className="chosen-select" data-placeholder="Opening Time">
								{/* <!-- Hours added via JS (this is only for demo purpose)--> */}
							</select>
						</div>
						<div className="col-md-5">
							<select className="chosen-select" data-placeholder="Closing Time">
								{/* <!-- Hours added via JS (this is only for demo purpose)--> */}
							</select>
						</div>
					</div>
					{/* <!-- Day / End--> */}

					{/* <!-- Day--> */}
					<div className="row opening-day js-demo-hours">
						<div className="col-md-2"><h5>Wednesday</h5></div>
						<div className="col-md-5">
							<select className="chosen-select" data-placeholder="Opening Time">
								{/* <!-- Hours added via JS (this is only for demo purpose)--> */}
							</select>
						</div>
						<div className="col-md-5">
							<select className="chosen-select" data-placeholder="Closing Time">
								{/* <!-- Hours added via JS (this is only for demo purpose)--> */}
							</select>
						</div>
					</div>
					{/* <!-- Day / End--> */}

					{/* <!-- Day--> */}
					<div className="row opening-day js-demo-hours">
						<div className="col-md-2"><h5>Thursday</h5></div>
						<div className="col-md-5">
							<select className="chosen-select" data-placeholder="Opening Time">
								{/* <!-- Hours added via JS (this is only for demo purpose)--> */}
							</select>
						</div>
						<div className="col-md-5">
							<select className="chosen-select" data-placeholder="Closing Time">
								{/* <!-- Hours added via JS (this is only for demo purpose)--> */}
							</select>
						</div>
					</div>
					{/* <!-- Day / End--> */}

					{/* <!-- Day--> */}
					<div className="row opening-day js-demo-hours">
						<div className="col-md-2"><h5>Friday</h5></div>
						<div className="col-md-5">
							<select className="chosen-select" data-placeholder="Opening Time">
								{/* <!-- Hours added via JS (this is only for demo purpose)--> */}
							</select>
						</div>
						<div className="col-md-5">
							<select className="chosen-select" data-placeholder="Closing Time">
								{/* <!-- Hours added via JS (this is only for demo purpose)--> */}
							</select>
						</div>
					</div>
					{/* <!-- Day / End--> */}

					{/* <!-- Day--> */}
					<div className="row opening-day js-demo-hours">
						<div className="col-md-2"><h5>Saturday</h5></div>
						<div className="col-md-5">
							<select className="chosen-select" data-placeholder="Opening Time">
								{/* <!-- Hours added via JS (this is only for demo purpose)--> */}
							</select>
						</div>
						<div className="col-md-5">
							<select className="chosen-select" data-placeholder="Closing Time">
								{/* <!-- Hours added via JS (this is only for demo purpose)--> */}
							</select>
						</div>
					</div>
					{/* <!-- Day / End--> */}

					{/* <!-- Day--> */}
					<div className="row opening-day js-demo-hours">
						<div className="col-md-2"><h5>Sunday</h5></div>
						<div className="col-md-5">
							<select className="chosen-select" data-placeholder="Opening Time">
								{/* <!-- Hours added via JS (this is only for demo purpose)--> */}
							</select>
						</div>
						<div className="col-md-5">
							<select className="chosen-select" data-placeholder="Closing Time">
								{/* <!-- Hours added via JS (this is only for demo purpose)--> */}
							</select>
						</div>
					</div>
					{/* <!-- Day / End--> */}

				</div>
				{/* <!-- Switcher ON-OFF Content / End--> */}

			</div>

		</>
	)
}

BookingSlots.propTypes = {}

export default BookingSlots
