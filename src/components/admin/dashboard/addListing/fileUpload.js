import React from "react"
import PropTypes from "prop-types"
import Dropzone from 'react-dropzone'

const FileUpload = props => {
	console.log("FILE PROPS", props)
	const { files, handleFileUpload, handleFileRemoval, images, cssClass, buttonText, iconClass, previewWidth } = props
	return (
		<>

			{/* <!-- Dropzone--> */}
			<div className="submit-section">
				<div className="photoUpload">
					<span><i className={iconClass}></i> {buttonText}</span>
					<input name="files" className={cssClass} type="file" multiple onChange={handleFileUpload} />
				</div>

				<div name="file-upload" className="dropzoness">
					{images.map(x =>
						<>
							<div key={x.url} className="file-preview">
								<a href="#" onClick={(e) => {
									e.preventDefault();
									handleFileRemoval(x.url)
								}}>
									<img style={{ width: previewWidth, height: previewWidth }} src={x.url}
										onClick={(e) => {
											handleFileRemoval(x.url)
										}}
									/>
									{x.url}
								</a>

							</div>
						</>
					)}

					{files.map(x =>
						<>
							<div key={x.name} className="file-preview">
								<a href="#" onClick={(e) => {
									e.preventDefault();
									handleFileRemoval(x.name)
								}}>
									<img style={{ width: previewWidth, height: previewWidth }} src={URL.createObjectURL(x)}
										onClick={(e) => {
											handleFileRemoval(x.name)
										}}
									/>
									{x.name}
								</a>

							</div>
						</>
					)}
				</div>
			</div>


			{/* <!-- Section / End--> */}

		</>
	)
}

FileUpload.propTypes = {}

export default FileUpload
