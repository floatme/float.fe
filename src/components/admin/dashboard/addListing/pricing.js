import React from "react"
import PropTypes from "prop-types"

const Pricing = props => {
	const { pricePerDay, title, handleChangeFn } = props

	return (
		<>
			{/* <!-- Section--> */}
			<div className="add-listing-section margin-top-45">

				{/* <!-- Headline--> */}
				<div className="add-listing-headline">
					<h3><i className="sl sl-icon-book-open"></i> Price per day</h3>
					{/* <!-- Switcher--> */}
					{/* <label className="switch"><input type="checkbox" checked /><span className="slider round"></span></label> */}
				</div>

				{/* <!-- Switcher ON-OFF Content--> */}
				{/* <div className="switcher-content"> */}

				<div className="row">
					<div className="col-md-12">
						<table id="pricing-list-container">
							<tr className="pricing-list-item pattern">
								<td>
									{/* <div className="fm-move"><i className="sl sl-icon-cursor-move"></i></div> */}
									<div name="title" className="fm-input pricing-name"><input type="text" placeholder="Title" value={title} disabled /></div>
									{/* <div className="fm-input pricing-ingredients"><input type="text" placeholder="Description" /></div> */}
									<div className="fm-input pricing-price">
										<input required type="number" name="pricePerDay" value={pricePerDay} onChange={handleChangeFn} />

									</div>
									{/* <div className="fm-close"><a className="delete" href="#"><i className="fa fa-remove"></i></a></div> */}
								</td>
							</tr>
						</table>
						{/* <a href="#" className="button add-pricing-list-item">Add Item</a> */}
						{/* <a href="#" className="button add-pricing-submenu">Add Category</a> */}
					</div>
				</div>

				{/* </div> */}
				{/* <!-- Switcher ON-OFF Content / End--> */}

			</div>
			{/* <!-- Section / End--> */}
		</>
	)
}

Pricing.propTypes = {}

export default Pricing
