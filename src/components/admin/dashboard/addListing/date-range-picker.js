import React from "react"
import PropTypes from "prop-types"
import { handleInputChange } from "../../../../functions/form-functions";


const DateRangePicker = props => {
    const { title, editable, startDate, endDate, index, removeDatePicker } = props
    const parentCss = editable === "false" ? "day-slots disabled-component" : "day-slots";

    return (

        <>

            {/* <!-- Single Day Slots--> */}
            <div className={parentCss}>
                <div className="day-slot-headline">
                    {title}
                    <input type="text" name={`title-${index}`} onChange={handleInputChange} placeholder="Title" />
                </div>
                {/* <!-- Slots Container--> */}
                <div className="slots-container">
                    {/* <!-- Single Slot--> */}
                    <div className={`single-slot ${index}`}>
                        <a id={`date-time-picker-${index}`} className="remove-slot" onClick={() => removeDatePicker(index)}><i className="fa fa-close"></i></a>
                        <div className="single-slot-left">
                            <div className="single-slot-time">Start Date<input type="date" name={`start-date-${index}`} value={startDate} onChange={handleInputChange} /></div>
                            <div className="single-slot-time">End Date<input type="date" name={`end-date-${index}`} value={endDate} onChange={handleInputChange} /></div>
                        </div>
                    </div>
                </div>
                {/* <!-- Slots Container / End--> */}
            </div>
            {/* <!-- Single Day Slots / End--> */}
        </>
    )
}

DateRangePicker.propTypes = {}

export default DateRangePicker
