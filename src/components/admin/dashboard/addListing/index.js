import React, { Component } from "react"
import { Link } from "gatsby"
import axios from 'axios';
import PropTypes from "prop-types"
import BasicInfo from "../../dashboard/addListing/basic-info";
import Location from "../../dashboard/addListing/location";
import Details from "../../dashboard/addListing/details";
import Pricing from "../../dashboard/addListing/pricing";
import FileUpload from "../../dashboard/addListing/fileUpload";
import { POST_add_listing, POST_add_image, GET_detail } from "../../../../api/endpoints";
import { DatePicker } from "antd";
import DateRangePicker from "./date-range-picker";
import { destroyDateTimePicker } from "../../../../functions/form-functions"
import { MapAddListingFromState, MapListingFromResponse, MapAddImageFromState } from "../../../../api/mapper"
import { GetUserId, GetUserInfo } from "../../../../components/login/lib/cookies"
import Loader from "../../../shared/loader";
import ModalLink from "../../../shared/modal-link";
// import { BrowserRouter, Route } from 'react-router-dom';

const defaultState = {
	listing: {
		title: "",
		category: "",
		keywords: "",
		city: "",
		address: "",
		zipCode: "",
		description: "",
		terms: "",
		phone: "",
		email: "",
		fb: "",
		tw: "",
		delivery: false,
		collection: false,
		pricePerDay: "",
		files: [],
		filePreviews: [],
		//additional components
		bookings: [
		],
		bookmarkCount: "0",
		dateCreated: "",
		gps: "",
		images: [],
		mainImage: "",
		ratings: {},
		reviews: [],
		socialMedia: {},
		subCategory: {},
		suburb: "",
		subCategoryId: "",
		ownerId: GetUserId(),
		owner: GetUserInfo(),
		timestamp: "",
		verified: true
	},
	isEditSet: false,
	isDefaultSet: false,
	listingId: "",
	loading: false,
	uploadImages: false
};


class AddListing extends Component {
	constructor(props) {
		super(props);
		this.state = defaultState
	}

	redirectToDetailPage(listingId) {
		window.location = `/detail/?id=${listingId}`;
	}

	handleInputChange = event => {
		if (event !== undefined) {
			const target = event.target
			const value = target.type === 'checkbox' ? target.checked : target.value;
			const name = target.name
			this.setState({
				listing: {
					...this.state.listing,
					[name]: value,
				}
			})
		}
	}

	handleLocationChange = (value) => {
		this.setState({
			listing: {
				...this.state.listing,
				address: value,
			}
		})
	}

	handleFileUpload = event => {

		const files = event.target.files;
		const filesArr = Array.prototype.slice.call(files);
		console.log("STATE HERE!!", this.state)
		//	const imageArray = filesArr !== [] ? filesArr : filesArr.map(x => this.mapImageUrl(x));
		this.setState({
			listing: {
				...this.state.listing,
				files: [...this.state.listing.files, ...filesArr],
				//	images: [...this.state.listing.images, ...imageArray],
			},
			uploadImages: true
		});
	}

	mapImageUrl(file) {
		return { url: URL.createObjectURL(file), title: file.name }
	}

	handleFileRemoval = (fileName) => {
		this.setState({ listing: { files: this.state.listing.files.filter(x => x.name !== fileName) } });
	}

	handleSubmit = (e) => {
		e.preventDefault();
		this.setState({ loading: true })
		console.log("ADD LISTING STATE", this.state.listing)
		const payload = MapAddListingFromState(this.state.listing)
		const config = {
			headers: { 'Access-Control-Allow-Origin': '*' }
		};
		axios.post(POST_add_listing, payload, config)
			.then(res => {
				const listingId = res.data.id;
				console.log("RESPONSE listingId", listingId);
				if (this.state.uploadImages === true) {
					console.log("Uploading images...")
					const imagePayload = MapAddImageFromState(this.state.listing.files);
					axios.post(`${POST_add_image}${listingId}`, imagePayload, { headers: { 'Content-Type': 'multipart/form-data' } })
						.then(res => {
							this.setState({ loading: false, uploadImages: false })
							this.redirectToDetailPage(listingId)
						});
				} else {
					this.setState({ loading: false, uploadImages: false })
				}

			}).catch(error => {
				// const imagePayload = MapAddImageFromState(this.state.listing.files);
				// axios.post(POST_add_image, imagePayload, { headers: { 'Content-Type': 'multipart/form-data' } });
				console.log(`[ERROR] Adding listing, error: `, error)
				// this.setState({ loading: false, error })
			})
	}

	removeDatePicker = (index) => {
		const newBookings = destroyDateTimePicker(index, this.state.listing.bookings);
		this.setState({ listing: { bookings: newBookings } })
	}

	addNewDatePicker = () => {
		const newBooking = { title: "New", startDate: "12/01/2019", endDate: "12/04/2019", editable: "true" };
		const newBookings = [...this.state.listing.bookings, newBooking]
		this.setState({ listing: { bookings: newBookings } })
	}

	extractParametersFromSearch(search) {
		return search.split('id=')[1];
	}

	GetDetail = listingId => {
		console.log("Getting detail ...")
		this.setState({ loading: true, listingId: listingId, isEditSet: true, isDefaultSet: false })
		axios
			.get(`${GET_detail}${listingId}`)
			.then(response => {

				console.log("Good response", response)
				let detail = MapListingFromResponse(response.data)
				detail.listingId = listingId
				console.log("Good response", detail)
				this.setState({
					listing: detail,
					loading: false
				})

				console.log
					("Set state", this.state)
			})
			.catch(error => {
				console.log("Bad response", error)
				this.setState({ loading: false, error })
			})
	}




	render() {
		const _listingId = this.extractParametersFromSearch(window.location.href);

		if (_listingId !== undefined) {
			if (this.state.isEditSet === false) {
				this.GetDetail(_listingId);
			}
		} else {
			if (this.state.isDefaultSet === false) {
				this.setState({ listing: defaultState.listing, isDefaultSet: true })
			}
		}

		return (
			<>	<form
				onSubmit={this.handleSubmit}
			// noValidate
			>
				<div className="col-lg-12">
					<div id="add-listing">

						<BasicInfo title={this.state.listing.title}
							subCategoryId={this.state.listing.subCategoryId}
							keywords={this.state.listing.keywords}
							handleChangeFn={this.handleInputChange} />
						<Details
							description={this.state.listing.description}
							terms={this.state.listing.terms}
							phone={this.state.listing.owner === undefined ? "" : this.state.listing.owner.cellPhone}
							email={this.state.listing.email}
							fb={this.state.listing.facebook === undefined ? "" : this.state.listing.owner.facebook}
							tw={this.state.listing.twitter === undefined ? "" : this.state.listing.owner.twitter}
							delivery={this.state.listing.delivery}
							collection={this.state.listing.collection}
							handleChangeFn={this.handleInputChange} />
						<Pricing title={this.state.listing.title} pricePerDay={this.state.listing.pricePerDay} handleChangeFn={this.handleInputChange} />

						<Location
							city={this.state.listing.location}
							address={this.state.listing.address}
							zipCode={this.state.listing.zipCode}
							suburb={this.state.listing.suburb}
							handleChangeFn={this.handleInputChange}
							handleLocationChange={this.handleLocationChange}
						/>
						<div className="add-listing-section margin-top-45">

							{/* <!-- Headline--> */}
							<div className="add-listing-headline">
								<h3><i className="sl sl-icon-picture"></i> Gallery</h3>
							</div>

							<FileUpload
								images={this.state.listing.images === undefined ? [] : this.state.listing.images}
								files={this.state.listing.files === undefined ? [] : this.state.listing.files}
								handleFileUpload={this.handleFileUpload}
								handleFileRemoval={this.handleFileRemoval}
								cssClass=""
								buttonText=""
								onDrop={this.onDrop}
								cssClass="upload"
								buttonText="Upload photo"
								iconClass="fa fa-upload"
								previewWidth="150px"
							/>
						</div>

						{/* <!-- Section--> */}
						<div className="add-listing-section margin-top-45">

							{/* <!-- Headline--> */}
							<div className="add-listing-headline">
								<h3><i className="fa fa-calendar-check-o"></i> Availability</h3>
								{/* <!-- Switcher--> */}
								{/* <label className="switch"><input type="checkbox" checked /><span className="slider round"></span></label> */}
							</div>

							{/* <!-- Switcher ON-OFF Content--> */}
							<div className="switcher-contentt">

								{/* <!-- Availablity Slots--> */}
								{/* <!-- Set data-clock-type="24hr" to enable 24 hours clock type--> */}
								<div className="availability-slots" data-clock-type="24hr">

									{this.state.listing.bookings !== undefined ? this.state.listing.bookings.map((booking, index) => (<>
										<DateRangePicker title={booking.title}
											editable={booking.editable}
											startDate="2017/01/01"
											endData="2017/01/01"
											index={index}
											removeDatePicker={this.removeDatePicker}
										/>
									</>)) : ""}
									<div className="day-slots">
										<div className="slots-container">
											<div className="single-slot-left">
												<a onClick={() => this.addNewDatePicker()} className="button margin-top-15">Add New</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div className="col-md-3 padding-20 margin-top-10 margin-bottom-30">
							{/* <Link
								className="button margin-top-15"
								to="../detail/"
								state={{
									previewState: this.state,
									modal: true
								}}
							>
								Preview
								</Link> */}
							<ModalLink
								isDetail={true}
								detailState={this.state}
								linkText="Preview"
								modalTitle={this.state.listing.title}
								className="button margin-top-15"
							/>

						</div>
						<div className="col-md-3 padding-20 margin-top-10 margin-bottom-30 ">
							<input type="submit" className="button margin-top-15" value="Save" />
						</div>

					</div>

				</div></form>
				<Loader showLoader={this.state.loading} />
			</>
		)
	}

}

AddListing.propTypes = {}

export default AddListing
