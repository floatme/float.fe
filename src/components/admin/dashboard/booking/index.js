import React, { Component } from "react"
import axios from 'axios';
// import { Link } from "gatsby"
// import Booking from "./booking";
import { GET_booking } from "../../../../api/endpoints";
import { GetUserId } from "../../../../components/login/lib/cookies"
import { mockBookings } from "../../../../mocks";
import { MapUserBookingFromResponse } from "../../../../api/mapper"
import Loader from "../../../shared/loader";
import { PulseLoader } from "react-spinners";
import NextStep from "./next-step";
import { extractParamFromLocation } from "../../../../functions/form-functions";
// import { BookingStatusEnum } from "../enums/BookingStatusEnum"

const isSeller = false;

class BookingDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            error: false,
            userId: GetUserId(),
            booking: {
                listing: {},
                borrower: {},
                lender: {},
                isPaid: false
            }
        };
    }

    componentDidMount() {
        const { location, id } = this.props;
        // const { search } = location;

        console.log("PROPS 1", this.props)
        console.log("PROPS 2", window.location.href)
        this.GetBookings();
    }


    GetBookings() {
        var bookingId = extractParamFromLocation(this.props.location, "bookingId");
        console.log("GETTING BOOKING for", this.props)
        axios
            .get(`${GET_booking}${bookingId}`)
            .then(response => {
                console.log(`[INFO] Retrieved bookings for userId: ${this.state.userId}, data: `, response.data)
                const payload = response.data;
                console.log("PAYLOADD", payload)
                debugger;
                this.setState({
                    loading: false,
                    booking: payload
                })

            })
            .catch(error => {
                console.log(`[ERROR] Getting booking for userId: ${this.state.userId}, error: `, error)
                this.setState({ loading: false, error })

            })
    }


    render() {
        const isLender = false;

        const requestBlock = {
            isActive: true,
            username: isLender === true ? "You " : `${this.state.booking.borrower.firstName} `,
            pic: this.state.booking.borrower.profilePicUrl,
            description: isLender === true ?
                `${this.state.booking.borrower.firstName} ${this.state.booking.borrower.lastName} requested a booking for ${this.state.booking.listing.title}` :
                `${this.state.booking.borrower.firstName} ${this.state.booking.borrower.lastName} requested a booking for  ${this.state.booking.listing.title}`,
            shortDescription: ` ${this.state.booking.borrower.firstName} ${this.state.booking.borrower.lastName} requested a booking for ${this.state.booking.listing.title}`,
            listingTitle: this.state.booking.listing.title,
            date: this.state.booking.requestDate,
            nextStep: {
                isActive: this.state.booking.status === 1,
                username: this.state.booking.lender.firstName,
                pic: this.state.booking.lender.profilePicUrl,
                shortDescription: `Waiting for ${this.state.booking.lender.firstName} to respond..`,
                description: this.state.booking.isPaid === true ? `You can prioritize your booking and pay now` : `Your booking is paid for and a priority request has been sent to ${this.state.booking.lender.firstName}`,
                date: this.state.booking.dateLastModified,
                listingTitle: this.state.booking.listing.title,
                username: this.state.booking.borrower.firstName,
                activity: " requested a booking ",
                actionText: "Pay Now",
                actionButtonFn: () => { },
                actionButtonActive: this.state.booking.status == 0
            }
        }
        const confirmBlock = {
            isActive: (this.state.booking.status > 1 && this.state.booking.status !== 2),
            username: isLender === true ? "You " : `${this.state.booking.borrower.firstName} `,
            pic: this.state.booking.borrower.profilePicUrl,
            description: isLender === true ?
                `Confirmed booking for ${this.state.booking.listing.title}, booking reference: ${this.state.booking.id}` :
                this.state.booking.isPaid === true ?
                    `Confirmed your booking for  ${this.state.booking.listing.title}` :
                    `Accepted your booking for  ${this.state.booking.listing.title}, you can now pay to reserve your booking.`,
            shortDescription: isLender === true ?
                ` You accepted a booking for ${this.state.booking.listing.title}` :
                ` ${this.state.booking.lender.firstName} accepted your booking for ${this.state.booking.listing.title}`
            ,
            listingTitle: this.state.booking.listing.title,
            date: this.state.booking.requestDate,
            nextStep: {
                isActive: this.state.booking.status === 1,
                username: this.state.booking.lender.firstName,
                pic: this.state.booking.lender.profilePicUrl,
                shortDescription: this.state.booking.isPaid === false ?
                    `You can prioritize your booking and pay now` :
                    `Your booking is paid for and a priority request has been sent to ${this.state.booking.lender.firstName}`,
                description: this.state.booking.isPaid === false ?
                    `You can prioritize your booking and pay now` :
                    `Your booking is paid for and a priority request has been sent to ${this.state.booking.lender.firstName}, your booking reference is ${this.state.booking.id}`,
                date: this.state.booking.dateLastModified,
                listingTitle: this.state.booking.listing.title,
                username: this.state.booking.lender.firstName,
                activity: " confirmed booking for ",
                actionText: "Pay Now",
                actionButtonFn: () => { },
                actionButtonActive: this.state.booking.isPaid === false
            }
        }

        const paidBlock = {
            isActive: this.state.booking.isPaid === true,
            username: this.state.booking.borrower.firstName,
            pic: this.state.booking.borrower.profilePicUrl,
            description: `${this.state.booking.borrower.firstName} paid for booking for  ${this.state.booking.listing.title} (${this.state.booking.id})`,
            shortDescription: ` ${this.state.booking.borrower.firstName} paid for booking`,
            listingTitle: this.state.booking.listing.title,
            date: this.state.booking.requestDate,
            nextStep: {
                isActive: this.state.booking.isPaid !== true,
                username: this.state.booking.lender.firstName,
                pic: this.state.booking.borrower.profilePicUrl,
                shortDescription: this.state.booking.delivery === true ? `Delivery is scheduled for ${this.state.booking.startDate}` : `Collection is scheduled for ${this.state.booking.startDate}`,
                description: `Your booking is paid for, ${this.state.booking.lender.firstName} will contact you 2 days before ${this.state.booking.startDate}  `,
                date: this.state.booking.dateLastModified,
                listingTitle: this.state.booking.listing.title,
                username: this.state.booking.borrower.firstName,
                activity: ` booking scheduled for ${this.state.booking.startDate} `,
                actionText: "Pay Now",
                actionButtonFn: () => { },
                actionButtonActive: false
            }
        }

        const cancelledBlock = {
            isActive: this.state.booking.status === 2,
            username: this.state.booking.lender.firstName,
            pic: this.state.booking.lender.profilePicUrl,
            description: `${this.state.booking.lender.firstName} canceled booking for  ${this.state.booking.listing.title} (${this.state.booking.id})`,
            shortDescription: ` ${this.state.booking.lender.firstName} canceled  booking`,
            listingTitle: this.state.booking.listing.title,
            date: this.state.booking.requestDate,
            nextStep: {
                isActive: true,
                username: this.state.booking.lender.firstName,
                pic: this.state.booking.borrower.profilePicUrl,
                shortDescription: `Unfortunately your booking was canceled`,
                description: `Unfortunately your booking was canceled, if you have paid for your booking you will recieve a full refund within 2 days. 
                If you have any more questions please email support@justfloat.app with booking reference ${this.state.booking.id}`,
                date: this.state.booking.dateLastModified,
                listingTitle: this.state.booking.listing.title,
                username: this.state.booking.lender.firstName,
                activity: ` booking canceled`,
                actionText: "Browse other listings",
                actionButtonFn: () => { },
                actionButtonActive: true
            }
        }



        let nextStepBlock = {};
        if (requestBlock.isActive === true) { nextStepBlock = requestBlock.nextStep; }
        if (confirmBlock.isActive === true) { nextStepBlock = confirmBlock.nextStep; }
        if (paidBlock.isActive === true) { nextStepBlock = paidBlock.nextStep; }
        if (cancelledBlock.isActive === true) { nextStepBlock = cancelledBlock.nextStep; }
        console.log("state", this.state.booking)
        console.log("tets", nextStepBlock)

        return (

            <>

                {/* <!-- Listings --> */}
                <div class="col-lg-6 col-md-12">

                    <div class="dashboard-list-box margin-top-0 no-border">
                        <h4>Timeline</h4>
                        <ul>
                            {requestBlock.isActive === true ? (
                                <li>
                                    <div class="comments listing-reviews">
                                        <ul>
                                            <li>
                                                <div class="avatar"><img src={requestBlock.pic} alt="" /></div>
                                                <div class="comment-content"><div class="arrow-comment"></div>
                                                    <div class="comment-by"> <div class="comment-by-listing">
                                                        <a href="#">{requestBlock.username} on {requestBlock.date} </a>
                                                    </div>
                                                        <span class="date">{requestBlock.requestDate} </span>
                                                        <div class="star-rating" data-rating="5"></div>
                                                    </div>
                                                    <p>{requestBlock.shortDescription}</p>

                                                    {/* <div class="review-images mfp-gallery-container">
                                                        <a href={requestBlock.pic} class="mfp-gallery"><img src={requestBlock.pic} alt="" /></a>
                                                    </div> */}
                                                    {/* <a href="#small-dialog" class="rate-review popup-with-zoom-anim"><i class="sl sl-icon-action-undo"></i> Reply to this review</a> */}
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            ) : ""}
                            {confirmBlock.isActive === true ? (
                                <>
                                    <li className="disabled-list">   <div style={{ marginLeft: "50%", maxWidth: "5px" }}>
                                        <PulseLoader
                                            css={"override loader"}
                                            sizeUnit={"px"}
                                            size={5}
                                            color={'#50E3C2'}
                                            loading={true} />
                                    </div>
                                    </li>
                                    <li>
                                        <div class="comments listing-reviews">
                                            <ul>
                                                <li>
                                                    <div class="avatar"><img src={confirmBlock.pic} alt="" /></div>
                                                    <div class="comment-content"><div class="arrow-comment"></div>
                                                        <div class="comment-by"> <div class="comment-by-listing">
                                                            <a href="#">{confirmBlock.username} on {confirmBlock.date} </a>
                                                        </div>
                                                            <span class="date">{confirmBlock.requestDate} </span>
                                                            <div class="star-rating" data-rating="5"></div>
                                                        </div>
                                                        <p>{confirmBlock.shortDescription}</p>

                                                        {/* <div class="review-images mfp-gallery-container">
                                                        <a href={requestBlock.pic} class="mfp-gallery"><img src={requestBlock.pic} alt="" /></a>
                                                    </div> */}
                                                        {/* <a href="#small-dialog" class="rate-review popup-with-zoom-anim"><i class="sl sl-icon-action-undo"></i> Reply to this review</a> */}
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </>
                            ) : ""}

                            {paidBlock.isActive === true ? (
                                <>
                                    <li className="disabled-list">  <div style={{ marginLeft: "50%", maxWidth: "5px" }}>
                                        <PulseLoader
                                            css={"override loader"}
                                            sizeUnit={"px"}
                                            size={5}
                                            color={'#50E3C2'}
                                            loading={true} />
                                    </div> </li>
                                    <li>
                                        <div class="comments listing-reviews">
                                            <ul>
                                                <li>
                                                    <div class="avatar"><img src={paidBlock.pic} alt="" /></div>
                                                    <div class="comment-content"><div class="arrow-comment"></div>
                                                        <div class="comment-by"> <div class="comment-by-listing">
                                                            <a href="#">{paidBlock.username} on {paidBlock.date} </a>
                                                        </div>
                                                            <span class="date">{paidBlock.requestDate} </span>
                                                            <div class="star-rating" data-rating="5"></div>
                                                        </div>
                                                        <p>{paidBlock.shortDescription}</p>

                                                        {/* <div class="review-images mfp-gallery-container">
                                                        <a href={requestBlock.pic} class="mfp-gallery"><img src={requestBlock.pic} alt="" /></a>
                                                    </div> */}
                                                        {/* <a href="#small-dialog" class="rate-review popup-with-zoom-anim"><i class="sl sl-icon-action-undo"></i> Reply to this review</a> */}
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </>
                            ) : ""}

                            {cancelledBlock.isActive === true ? (
                                <>
                                    <li className="disabled-list">  <div style={{ marginLeft: "50%", maxWidth: "5px" }}>
                                        <PulseLoader
                                            css={"override loader"}
                                            sizeUnit={"px"}
                                            size={5}
                                            color={'#50E3C2'}
                                            loading={true} />
                                    </div> </li>
                                    <li>
                                        <div class="comments listing-reviews">
                                            <ul>
                                                <li>
                                                    <div class="avatar"><img src={cancelledBlock.pic} alt="" /></div>
                                                    <div class="comment-content"><div class="arrow-comment"></div>
                                                        <div class="comment-by"> <div class="comment-by-listing">
                                                            <a href="#">{cancelledBlock.username} on {cancelledBlock.date} </a>
                                                        </div>
                                                            <span class="date">{cancelledBlock.requestDate} </span>
                                                            <div class="star-rating" data-rating="5"></div>
                                                        </div>
                                                        <p>{cancelledBlock.shortDescription}</p>

                                                        {/* <div class="review-images mfp-gallery-container">
                                                        <a href={requestBlock.pic} class="mfp-gallery"><img src={requestBlock.pic} alt="" /></a>
                                                    </div> */}
                                                        {/* <a href="#small-dialog" class="rate-review popup-with-zoom-anim"><i class="sl sl-icon-action-undo"></i> Reply to this review</a> */}
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </>
                            ) : ""}


                        </ul>
                        <div style={{ marginLeft: "50%", maxWidth: "7px" }}>
                            <PulseLoader
                                css={"override loader"}
                                sizeUnit={"px"}
                                size={7}
                                color={'#8750d5'}
                                loading={true} />
                        </div>
                        <ul>
                            {/* {nextStepBlock.isActive === true ? ( */}
                            <li>
                                <div class="comments listing-reviews">
                                    <ul>
                                        <li>
                                            <div class="avatar"><img src={nextStepBlock.pic} alt="" /></div>
                                            <div class="comment-content"><div class="arrow-comment"></div>
                                                <p>{nextStepBlock.shortDescription}</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            {/* ) : ""} */}
                        </ul>
                    </div>
                </div>

                {/* <!-- Listings --> */}
                <div class="col-lg-6 col-md-12">
                    <div class="dashboard-list-box margin-top-0">
                        <h4>Next Steps</h4>
                        <ul>

                            <NextStep
                                actionButtonActive={nextStepBlock.actionButtonActive}
                                actionButtonFn={() => { }}
                                actionText={nextStepBlock.actionText}
                                description={nextStepBlock.description}
                                listingTitle={nextStepBlock.listingTitle}
                                username={nextStepBlock.username}
                                activity={nextStepBlock.activity}
                            />

                        </ul>
                    </div>
                </div>


                <Loader showLoader={this.state.loading} />
            </>
        )
    }

}

BookingDetail.propTypes = {}

export default BookingDetail
