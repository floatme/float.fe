// pages/modal-example.js

import React from 'react'
import { Link } from 'gatsby'

const NextStep = (props) => {

    const { actionButtonActive, actionButtonFn, actionText, description, listingTitle, username, activity } = props;

    return (

        <li>
            <div class="comments listing-reviews">
                <ul>
                    <li>
                        <div class="avatar"><img src="images/reviews-avatar.jpg" alt="" /> </div>
                        <div class="comment-content"><div class="arrow-comment"></div>
                            <div class="comment-by"> <div class="comment-by-listing own-comment"> {username} {activity} <a href="#">{listingTitle}</a></div> <span class="date"></span>
                                <div class="star-rating" data-rating="4.5"></div>
                            </div>
                            <p>{description}</p>
                            {actionButtonActive === true ? (<a href="#" onClick={() => { actionButtonFn() }} class="rate-review"><i class="sl sl-icon-note"></i> {actionText} </a>) : ""}
                        </div>
                    </li>
                </ul>
            </div>
        </li>

    )
}
export default NextStep