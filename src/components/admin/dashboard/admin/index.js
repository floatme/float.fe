import React, { Component } from "react"
import axios from 'axios';

import StatBox from "../index/stat-box";
import ActivityItem from "../index/activity-item";
import InvoiceItem from "../index/invoice-item";

import { GetUserId } from "../../../../components/login/lib/cookies"
import { GET_client_profile_summary, GET_admin } from "../../../../api/endpoints";
import { mockProfileSummary } from "../../../../mocks";
import Loader from "../../../shared/loader";
const useMock = false;

class AdminHome extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: false,
            importedListings: [],
            userId: GetUserId(),
            categories: [],
            convertedListings: [],
            importedListings: [],
            activeListings: [],
            activeUsers: [],
            inActiveUsers: [],
            stats: [],
            recentBookings: [],
            recentListings: []
        }
    }

    componentDidMount() {
        if (useMock) {
            this.setState({
                loading: false
            })
        } else {
            console.log("CURRENT STATE PROFILE", this.state)
            this.GetImportedListings()
        }
    }

    GetImportedListings() {
        this.setState({ loading: true })
        axios
            .get(`${GET_admin}${this.state.userId}`)
            .then(response => {
                console.log(`[INFO] Retrieved imported listings for userId: ${this.state.userId}, data: `, response.data)
                const payload = response.data
                this.setState({
                    loading: false,
                    importedListings: payload.importedListings,
                    convertedListings: payload.convertedListings,
                    activeListings: payload.activeListings,
                    activeUsers: payload.activeUsers,
                    inActiveUsers: payload.inActiveUsers,
                    stats: payload.stats,
                    recentBookings: payload.recentBookings,
                    recentListings: payload.recentListings
                })
                console.log(`[INFO] State after response data: `, this.state)
            })
            .catch(error => {
                console.log(`[ERROR] Getting imported listings for userId: ${this.state.userId}, error: `, error)
                this.setState({ loading: false, error })
            })
    }


    render() {
        return (
            <>

                {/* <!-- Notification --> */}
                <div class="row">
                    <div class="col-md-12">
                        {/* {this.state.profileSummary.notifications.map(notification => (<>
                            <div class="notification success closeable margin-bottom-30">
                                <p>{notification.subject} {notification.activity}<strong>{notification.listing}</strong> </p>
                                <a class="close" href="#"></a>
                            </div>
                        </>))} */}
                    </div>
                </div>

                {/* <!-- Content --> */}
                <div class="row">
                    {this.state.stats.map(stat => (<>
                        <StatBox
                            title={stat.title}
                            number={stat.number}
                            iconClass={stat.iconClass}
                            color={stat.color}
                        />
                    </>))}
                </div>

                {/* <!-- Recent Activity --> */}
                <div class="col-lg-6 col-md-12">
                    <div class="dashboard-list-box with-icons margin-top-20">
                        <h4>Recent On-boards</h4>
                        <ul>

                            {this.state.recentListings.map(listing => (<>
                                <ActivityItem
                                    subject={listing.subject}
                                    activity={listing.activity}
                                    iconClass={listing.iconClass}
                                    reviewRating={listing.reviewRating}
                                    listingUrl={listing.listingUrl}
                                    listing={listing.listing}
                                //  dismissFn={this.bookingDismissFn}
                                />
                            </>))}
                        </ul>
                    </div>
                </div>

                {/* <!-- Invoices --> */}
                <div class="col-lg-6 col-md-12">
                    <div class="dashboard-list-box invoices with-icons margin-top-20">
                        <h4>Recent bookings</h4>
                        <ul>
                            {this.state.recentBookings.map(booking => (<>
                                <ActivityItem
                                    subject={booking.subject}
                                    activity={booking.activity}
                                    iconClass={booking.iconClass}
                                    reviewRating={booking.reviewRating}
                                    listingUrl={booking.listingUrl}
                                    listing={booking.listing}
                                //  dismissFn={this.bookingDismissFn}
                                />
                            </>))}
                        </ul>
                    </div>
                </div>
                <Loader showLoader={this.state.loading} />


            </>
        )
    }

}

AdminHome.propTypes = {}

export default AdminHome
