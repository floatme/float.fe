import React, { Component } from "react"
import axios from 'axios';
import { GetUserId, SetUserInfo, GetUserInfo } from "../../../../components/login/lib/cookies"
import { MapUserProfileFromResponse, MapStateToUserProfileRequest } from "../../../../api/mapper";
import { Link } from "gatsby"

import { POST_update_profilepic, POST_update_profile, GET_profile } from "../../../../api/endpoints";
import Loader from "../../../shared/loader";
import ModalLink from "../../../shared/modal-link";

class Profile extends Component {
	constructor(props) {
		super(props);
		this.state = {
			profilePicUrl: "",
			profilePic: "",
			firstName: "",
			surname: "",
			cellPhone: "",
			address: "",
			email: "",
			bio: "",
			twitter: "",
			fb: "",
			google: "",
			newProfilePic: {},
			password: { current: "", new: "", newMatch: "" },
			userId: GetUserId(),
			loading: false
		}
	}

	initUserInfo() {
		const cookieUserInfo = GetUserInfo();
		if (cookieUserInfo === undefined || this.props.modal === true) {
			this.setState({
				loading: true
			});
			axios.get(`${GET_profile}${this.props.modal !== true ? this.state.userId : this.props.userId}`)
				.then(res => {
					const userInfo = MapUserProfileFromResponse(res.data)
					console.log("USER INFO RESULT INIT", userInfo)

					this.props.modal !== true ?
						this.setState({
							...userInfo,
							loading: false
						}) :
						this.setState({
							...userInfo,
							loading: false
						});

					console.log("RESULT AFTER RESPONSE ", this.state)
				}).catch(error => {
					this.setState({
						loading: false
					});
					console.log("ERROR UPDATE PROFILE PIC", error)
				});
		} else {
			const cookieUserInfoJson = cookieUserInfo;
			this.setState({
				...cookieUserInfoJson
			});
			this.setState({
				loading: false
			});
		}
	}

	handleFileUpload = event => {
		this.setState({
			loading: true
		});
		console.log("FILES INCOMING", event.target.files);
		const file = event.target.files[0];

		if (file !== undefined) {
			const requestModel = new FormData();
			requestModel.append(file.name, file);
			this.setState({
				profilePicUrl: "",
				profilePic: this.mapImageUrl(file).url
			});

			axios.post(`${POST_update_profilepic}${this.state.userId}`, requestModel)
				.then(res => {
					console.log("RESULT FROM PROF PIC", res)
					SetUserInfo(MapUserProfileFromResponse(res.data));
					this.setState({
						loading: false
					});

				}).catch(error => {
					this.setState({
						loading: false
					});
					console.log("ERROR UPDATE PROFILE PIC", error)
				});
		} else {
			this.setState({
				loading: false
			});
			console.log("[ERROR] cannot file uploaded file")
		}

	}

	mapImageUrl(file) {
		return { url: URL.createObjectURL(file), title: file.name }
	}

	handleInputChange = event => {
		if (event !== undefined) {
			const target = event.target
			const value = target.type === 'checkbox' ? target.checked : target.value;
			const name = target.name
			this.setState({
				[name]: value,
			})
		}
	}

	changePasswordFn = () => {

	}

	handleSubmit = () => {
		console.log("STATE GOING", this.state)
		this.setState({
			loading: true
		});
		const request = MapStateToUserProfileRequest(this.state)
		axios.post(`${POST_update_profile}`, request)
			.then(res => {
				this.setState({
					loading: false
				});

			}).catch(error => {
				this.setState({
					loading: false
				});
				console.log("ERROR UPDATE PROFILE ", error)
			});
	}

	componentDidMount = () => {
		this.initUserInfo()
	}

	render() {
		console.log("RENDER STATE", this.state)
		return (
			<>

				{/* <!-- Profile --> */}
				<div className="col-lg-6 col-md-12">
					<div className="dashboard-list-box margin-top-0">
						<h4 className="gray">Profile Details</h4>
						<div className="dashboard-list-box-static">

							{/* <!-- Avatar --> */}
							<div className="edit-profile-photo">
								<img src={this.state.profilePicUrl === "" ? this.state.profilePic : this.state.profilePicUrl} alt="" />
								<div className="change-photo-btn">
									{this.props.modal !== true ? (
										<div className="photoUpload">
											<span><i className="fa fa-upload"></i> Upload Photo</span>
											<input type="file" className="upload" onChange={this.handleFileUpload} />
										</div>
									) : ""}
								</div>
							</div>

							{/* <!-- Details --> */}
							<div className="my-profile">

								<label> Name</label>
								<input name="firstName" value={this.state.firstName} onChange={this.handleInputChange} type="text" />

								<label>Phone</label>
								<input name="cellPhone" value={this.state.cellPhone} onChange={this.handleInputChange} type="text" />

								<label>Email</label>
								<input name="email" value={this.state.email} onChange={this.handleInputChange} type="text" />

								<label>Bio</label>
								<textarea name="bio" value={this.state.bio} onChange={this.handleInputChange} id="notes" cols="30" rows="10"></textarea>

								<label><i className="fa fa-twitter"></i> Twitter</label>
								<input name="tw" value={this.state.tw} onChange={this.handleInputChange} placeholder="https://www.twitter.com/" type="text" />

								<label><i className="fa fa-facebook-square"></i> Facebook</label>
								<input name="fb" value={this.state.fb} onChange={this.handleInputChange} placeholder="https://www.facebook.com/" type="text" />

								<label><i className="fa fa-google-plus"></i> Google+</label>
								<input name="google" value={this.state.google} onChange={this.handleInputChange} placeholder="https://www.google.com/" type="text" />
							</div>

							{this.props.modal !== true ? (<button onClick={this.handleSubmit} className="button margin-top-15">Save Changes</button>) : ""}

						</div>
					</div>
				</div>

				{this.props.modal !== true ? (
					<div className="col-lg-6 col-md-12">
						<div className="dashboard-list-box margin-top-0">
							<h4 className="gray">Change Password</h4>
							<div className="dashboard-list-box-static">

								{/* <!-- Change Password --> */}
								<div className="my-profile">
									<label className="margin-top-0">Current Password</label>
									<input name="password" value={this.state.password.current} type="password" />

									<label>New Password</label>
									<input name="new" value={this.state.password.new} onChange={this.handleInputChange} type="password" />

									<label>Confirm New Password</label>
									<input name="newMatch" value={this.state.password.newMatch} onChange={this.handleInputChange} type="password" />

									<button onClick={this.changePasswordFn} classNameName="button margin-top-15">Change Password</button>
								</div>

							</div>
						</div>
					</div>

				) : ""}
				<Loader showLoader={this.state.loading} />
			</>
		)
	}

}

Profile.propTypes = {}

export default Profile
