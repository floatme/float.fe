import React from "react"
import PropTypes from "prop-types"

export default function HTML(props) {
  return (
    <html {...props.htmlAttributes}>
      <head>
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        {props.headComponents}
        {/* <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeC3c3gl9CD4fOe7Y30uyIl3GFrLg98LY&libraries=places&callback=logInit"></script>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
        {function logInit() {
          console.log("maps loaded")

        }} */}
        {/* <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js" /> */}

      </head>
      <body {...props.bodyAttributes}>
        {props.preBodyComponents}
        <noscript key="noscript" id="gatsby-noscript">
          This app works best with JavaScript enabled.
        </noscript>
        <div
          key={`body`}
          id="___gatsby"
          dangerouslySetInnerHTML={{ __html: props.body }}
        />
        {props.postBodyComponents}
      </body>
      {/* <script
        type="text/javascript"
        src="../scripts/jquery-3.4.1.min.js"
      ></script>
      <script
        type="text/javascript"
        src="../scripts/jquery-migrate-3.1.0.min.js"
      ></script>
      <script type="text/javascript" src="../scripts/jquery-3.4.1.min.js"></script>
      <script type="text/javascript" src="../scripts/jquery-migrate-3.1.0.min.js"></script>
      <script type="text/javascript" src="../scripts/mmenu.min.js"></script>
      
      <script type="text/javascript" src="../scripts/slick.min.js"></script>
      <script type="text/javascript" src="../scripts/rangeslider.min.js"></script>
      <script type="text/javascript" src="../scripts/magnific-popup.min.js"></script>
      <script type="text/javascript" src="../scripts/waypoints.min.js"></script>
      <script type="text/javascript" src="../scripts/counterup.min.js"></script>
      <script type="text/javascript" src="../scripts/jquery-ui.min.js"></script>
      <script type="text/javascript" src="../scripts/tooltips.min.js"></script>
      <script type="text/javascript" src="../scripts/custom.js"></script> */}
      {/* <script type="text/javascript" src="../scripts/chosen.min.js"></script>   */}
    </html>
  )
}

HTML.propTypes = {
  htmlAttributes: PropTypes.object,
  headComponents: PropTypes.array,
  bodyAttributes: PropTypes.object,
  preBodyComponents: PropTypes.array,
  body: PropTypes.string,
  postBodyComponents: PropTypes.array,
}
