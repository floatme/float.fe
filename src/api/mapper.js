import {ListingStatusEnum} from "../components/admin/dashboard/enums/ListingStatusEnum";
import {LoginStatusEnum} from "../components/login/enums/LoginStatusEnum"
import {BookingStatusEnum} from "../components/admin/dashboard/enums/BookingStatusEnum";

export function MapAddListingFromState(state) {
    console.log("ADD LIST STATE", state)
    const requestModel = {
        "id": state.listingId === undefined ? null : state.listingId,
        "pricePerDay": state.pricePerDay || "",
        "title": state.title || "",
        "description": state.description || "",
        "keywords": state.keywords || "",
        "mainImage": state.mainImage || "",
        "location": state.location || "",
        "collection": state.collection || false,
        "delivery": state.delivery || false,
        "terms": state.terms || "",
        "subCategoryId": state.subCategoryId,
        "ownerId": state.ownerId,
        "suburb": state.suburb || "",
        "terms": state.terms || "",
        "phone": state.phone || "",

    }
    return requestModel;
}

export function MapAddReviewFromState(state, listingId, userId) {
    console.log("ADD LIST STATE", state)
    const requestModel = {
        valueForMoney: state.valueForMoney,
        service: state.service,
        quality: state.quality,
        availability: state.availability,
        reviewText: state.reviewText,
        name: state.name,
        email: state.email,
        listingId: listingId,
        userId: userId
    }
    return requestModel;
}

export function MapProfileResponseToState(response) {
    // stats: [
    //     {
    //       title: "Active Listings",
    //       number: "2",
    //       iconClass: "im-icon-Map2",
    //       color: "1"
    //     },
    // const stateModel = {
    //     stats: responseData.states === undefined ?
    //         [] :
    //         responseData.stats.map(
    //             stat => {
    //                 return {
    //                     title: stat.title,
    //                     number: stat.number,
    //                     ic
    //                 }
    //             }),
    // }
}
export function MapUserProfileFromResponse(response) {
    return {
        userId: response.id,
        role: response.role,
        firstName: response.firstName,
        lastName: response.lastName,
        address: response.address,
        cellPhone: response.cellPhone,
        isVerified: response.isVerified,
        isActive: response.isActive,
        profilePicUrl: response.profilePicUrl,
        lastLogin: response.lastLogin,
        token: response.token,
        bio: response.bio,
        email: response.email,
        tw: response.twitter,
        fb: response.facebook,
        google: response.google,
        status: LoginStatusEnum.SUCCESS,
        bookmarks: response.bookmarks
    };
}

export function MapStateToUserProfileRequest(state) {
    console.log("state MAP", state)
    const stateModel = {
        id: state.userId,
        role: state.role,
        firstName: state.firstName,
        lastName: state.lastName,
        address: state.address,
        cellPhone: state.cellPhone,
        isVerified: state.isVerified,
        isActive: state.isActive,
        profilePicUrl: state.profilePicUrl,
        lastLogin: state.lastLogin,
        token: state.token,
        bio: state.bio,
        email: state.email,
        twitter: state.tw,
        facebook: state.fb,
        google: state.google,
        status: LoginStatusEnum.SUCCESS
    }
    return stateModel;

}


export function MapAddImageFromState(files) {
    const requestModel = new FormData();
    for (var i = 0; i < files.length; i++) {
        requestModel.append(files[i].name, files[i]);
    }
    return requestModel;
}
export function MapListingFromResponse(responseData) {

    console.log("RESP MODEL", responseData)
    const stateModel = {
        title: responseData.title,
        keywords: responseData.keywords,
        category: responseData.category,
        subCategory: responseData.subCategory,
        subCategoryId: responseData.subCategory.id,
        ownerId: responseData.owner.id,
        location: responseData.location,
        suburb: responseData.suburb,
        description: responseData.description,
        phoneNumber: responseData.owner.cellPhone,
        listingId: responseData.id,
        email: responseData.owner.email,
        delivery: responseData.delivery,
        collection: responseData.collection,
        pricePerDay: responseData.pricePerDay,
        verified: responseData.owner.isVerified,
        bookings: responseData.currentBookings,
        mainImage: responseData.mainImage,
        dateCreated: responseData.dateCreated,
        files: [],
        bookingFriendlyDates: responseData.friendlyBookingDates === undefined ? [] : responseData.friendlyBookingDates.map(date => { return { startDate: date.startDate, days: date.days } }),
        images: responseData.images === undefined ? [] : responseData.images.map(imgResponse => { return { url: imgResponse.url, title: imgResponse.title } }),
        reviews: responseData.reviews,
        reviewSummary: responseData.reviewSummary,
        bookmarkCount: "",
        pricePerWeek: "",
        gps: "",
        terms: responseData.terms,
        ratings: {},
        isBookmarked: responseData.isBookmarked,
        owner: {
            address: responseData.owner.address,
            bio: responseData.owner.bio,
            cellPhone: responseData.owner.cellphone,
            email: responseData.owner.email,
            facebook: responseData.owner.facebook,
            firstName: responseData.owner.firstName,
            google: responseData.owner.google,
            userId: responseData.owner.id,
            lastLogin: responseData.owner.lastLogin,
            lastName: responseData.owner.lastName,
            profilePicUrl: responseData.owner.profilePicUrl,
            role: responseData.owner.role,
            token: responseData.owner.token,
            twitter: responseData.owner.twitter,
            isActive: responseData.owner.isActive,
            isVerified: responseData.owner.isVerified
        },
        socialMedia: {
            fb: responseData.owner.facebook,
            tw: responseData.owner.twitter,
            google: responseData.owner.google
        }
    }
    console.log("MAPPED STATE MODEL", stateModel)
    return stateModel;
}

export function MapHomeListingBySubCategoryResponse(subCategories) {

    var stateModel = subCategories.map(subCategory => {
        return {
            name: subCategory.SubCategory.Name,
            count: subCategory.Count,
            id: subCategory.SubCategory.Id,
            icon: subCategory.SubCategory.icon
        }
    })

    return stateModel
}

export function MapUserListingFromResponse(listings) {

    console.log("REQ MODEL", listings)
    var stateModel = listings.map(listing => {
        return {
            key: listing.id,
            listingId: listing.id,
            status: listing.isActive === true ? ListingStatusEnum.ENABLED : ListingStatusEnum.DISABLED,
            listingUrl: `/detail/${listing.listingId}`,
            title: listing.title,
            description: listing.description,
            rating: listing.rating,
            reviewCount: listing.reviewCount,
            mainImageUrl: listing.mainImage
        }
    })

    return stateModel
}

export function MapUserBookingFromResponse(response) {

    const bookings = response.bookings.map(booking => {
        return {
            // key: listing.Id,
            // listingId: listing.Id,
            // status: ListingStatusEnum.ENABLED,
            // listingUrl: `/detail/${listing.listingUrl}`,
            // title: listing.Title,
            // description: listing.Description,
            // rating: listing.Rating,
            // reviewCount: listing.ReviewCount,
            // mainImageUrl: listing.MainImageUrl
            // }

            //         Borrower: {Id: "5db01151d143786f5819d406", Role: null, FirstName: "Sfeb", LastName: "Nkinu", Address: "Johannesburg", …}
            // Collection: false
            // ConfirmedDate: "0001-01-01T00:00:00Z"
            // Delivery: false
            // EndDate: "2019-12-29T22:00:00Z"
            // Id: "5dd998d45dab1c617ce91307"
            // Lender: {Id: "5db01151d143786f5819d406", Role: null, FirstName: "Sfeb", LastName: "Nkinu", Address: "Johannesburg", …}
            // Listing: {Id: "5dd98efd5dab1c617ce91306", Owner: {…}, PricePerDay: "231", Title: "tets", Description: "da", …}
            // Price: null
            // RequestDate: "0001-01-01T00:00:00Z"
            // StartDate: "0001-01-01T00:00:00Z"
            // Status: 3
            // isActive: false
            // isPaid: false
            bookingId: booking.id,
            //profilePicUrl: booking.profilePicUrl,
            title: booking.listing.title,
            timePeriod: `${booking.friendlyStartDate} until ${booking.friendlyEndDate}`,
            price: booking.price,
            username: booking.borrower.firstName,
            email: booking.borrower.email,
            phone: booking.borrower.cellphone,
            paid: booking.isPaid,
            status: mapBookingStatus(booking.status),
            confirmedDate: booking.friendlyConfirmedDate,
            requestDate: booking.friendlyRequestDate,
            listing: booking.listing,
            profilePicUrl: booking.borrower.profilePicUrl,
            listingImageUrl: booking.listing.mainImage,
            lastModifiedDate: booking.friendlyDateLastModified

        }

    });

    const listings = response.listings.map(listing => {
        return {
            title: listing.title,
            id: listing.Id,

        }
    })

    return {
        bookings: bookings,
        listings: listings
    }
}

function mapStatus(status) {
    switch (status) {

        case "0": return ListingStatusEnum.ENABLED;
        case "1": return ListingStatusEnum.DISABLED;
        case "2": return ListingStatusEnum.PENDING_DELETION;
        case "3": return ListingStatusEnum.DELETED;
    }
}

function mapBookingStatus(status) {
    switch (status) {

        case 0: return BookingStatusEnum.REJECTED;
        case 1: return BookingStatusEnum.APPROVED;
        case 2: return BookingStatusEnum.CANCELLED;
        case 3: return BookingStatusEnum.PENDING;
        case 4: return BookingStatusEnum.ONGOING;
        case 5: return BookingStatusEnum.COMPLETE;

    }
}



export function MapAddBookingFromState(state, userId, listingId) {
    const requestModel = {
        ListingId: listingId,
        BorrowerId: userId,
        StartDate: state.startDate,
        EndDate: state.endDate,
        Delivery: state.delivery,
        Collection: state.collection
    }
    return requestModel;
}