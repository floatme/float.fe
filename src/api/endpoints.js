
const base = "https://api.justfloat.app/api";
// const base = "http://localhost:11718/api"

//HOME
export const GET_Home = `${base}/home`;

//DETAIL

//LISTINGS
export const GET_all_listings = `${base}/listing/`;
export const GET_search_listings = (term, location, subCategory) => { return `${base}/listing/SearchListing/${term}/${location}/${subCategory}` };
export const GET_filter_listings = (filterType, filterTerm, orderType, desc, searchTerm, location, subCategory) => { return `${base}/listing/filterlisting/${filterType}/${filterTerm}/${orderType}/${desc}/${searchTerm}/${location}/${subCategory}` };
export const GET_listings_by_category = `${base}/listing/home/category/`;
export const GET_top_listings = `${base}/listing/top/`;
export const GET_detail = `${base}/listing/`;
export const GET_user_listings = `${base}/listing/getUserListings/`;
export const GET_imported_listings = `${base}/listing/admin/imports/`;
export const GET_admin = `${base}/listing/admin/home/summary/`;


export const GET_ENABLE_listing = (userId, listingId) => { return `${base}/listing/enable/${userId}/${listingId}` };
export const GET_DISABLE_listing = (userId, listingId) => { return `${base}/listing/disable/${userId}/${listingId}` };
export const GET_DELETE_listing = (userId, listingId) => { return `${base}/listing/delete/${userId}/${listingId}` };

export const POST_add_listing = `${base}/listing`;
export const POST_add_image = `${base}/listing/upload/images/`;
export const GET_TOP_category = `${base}/category/top/`;
export const GET_category = `${base}/category/`;
export const GET_FEATURED_category = `${base}/category/featured`;
export const POST_check_availability = (listingId, startDate, endDate) => { return `${base}/listing/availability/${listingId}/${startDate}/${endDate}` };

//PROFILE
export const GET_profile = `${base}/user/`;
export const GET_profile_summary = `${base}/user/profile/`;
export const GET_client_profile_summary = `${base}/user/profile/client/`;
export const POST_update_profile = `${base}/user/profile/update`;
export const POST_update_profilepic = `${base}/user/update/profilepic/`;
export const GET_add_bookmark = (userId, listingId) => { return `${base}/user/bookmark/add/${userId}/${listingId}` };
export const GET_remove_bookmark = (userId, listingId) => { return `${base}/user/bookmark/remove/${userId}/${listingId}` };
export const GET_bookmarks = `${base}/user/bookmarks/`;


//NOTIFICATIONS
export const POST_dismiss_booking_notification = (userId, bookingId) => { return `${base}/user/booking/notification/dismiss/${userId}/${bookingId}` };


//BOOKINGS
export const GET_booking = `${base}/booking/`;
export const GET_borrower_bookings = `${base}/booking/borrower/`;
export const GET_lender_bookings = `${base}/booking/lender/`;
export const GET_cancel_booking = (bookingId, userId) => { return `${base}/booking/cancel/${bookingId}/${userId}/` };
export const GET_approve_booking = (bookingId, userId) => { return `${base}/booking/approve/${bookingId}/${userId}/` };
export const GET_reject_booking = (bookingId, userId) => { return `${base}/booking/reject/${bookingId}/${userId}/` };
export const POST_add_booking = `${base}/booking/`;

//USER
export const POST_login = `${base}/user/authenticate/`;
export const POST_register = `${base}/user/`;
export const POST_complete_register = `${base}/user/complete/`;
export const POST_verify_otp = `${base}/user/otp`;

//REVIEWS
export const POST_add_review = `${base}/listing/review/add/`;
export const POST_add_review_image = (listingId, reviewId) => `${base}/listing/review/upload/images/${listingId}/${reviewId}`;
export const GET_upvote_review = (listingId, reviewId, userId) => `${base}/listing/review/addupcount/${listingId}/${reviewId}/${userId}`;
export const GET_reviews = `${base}/listing/review/getreview/`;