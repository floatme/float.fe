import Cookie from "js-cookie"
import axios from "axios"

const loginCookieName = "float-session-token";
const userIdCookieName = "float-user-id";
const HEADER = {
    'Authorization': `Bearer ${GetJwToken()}`,
    'Access-Control-Allow-Origin': '*',
};

export function GET(endPoint, params) {
    axios.get(endPoint, {headers: HEADER, params:params})
        .then(result => {
            return result
        })
        .catch(error => {
            return error;
        })
}

export function POST(endPoint, params) {
    axios.post(endPoint, {headers: HEADER, params: params})
        .then(result => {
            return result
        })
        .catch(error => {
            return error;
        })
}

export function DELETE(endPoint, params) {

    axios.delete(endPoint, {headers: HEADER, params: params})
        .then(result => {
            return result
        })
        .catch(error => {
            return error;
        })
}

export function PUT(endPoint, params) {
    let HEADER = {
        'Authorization': `Bearer ${GetJwToken()}`,
        'Access-Control-Allow-Origin': '*',
    };
    axios.put(endPoint, {headers: HEADER, params: params})
        .then(result => {
            return result
        })
        .catch(error => {
            return error;
        })
}


function GetUserId() {
    const userId = Cookies.get(userIdCookieName);
    if (userId !== null && userId !== undefined) return userId;
    return undefined
}

function GetJwToken() {
    const jwToken = Cookies.get(loginCookieName);
    if (jwToken !== null && jwToken !== undefined) return jwToken;
    return undefined
}

function SetUserId(value) {
    Cookies.set(userIdCookieName, value);
}

function SetJwToken(value) {
    Cookies.set(loginCookieName, value);
}

function RemoveUserId() {
    Cookies.remove(userIdCookieName);
}

export function RemoveJwToken() {
    Cookies.remove(loginCookieName);
}





